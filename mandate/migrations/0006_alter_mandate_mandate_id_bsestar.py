# Generated by Django 4.0.2 on 2022-08-04 07:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mandate', '0005_rename_mandate_id_mandate_mandate_id_digio_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mandate',
            name='mandate_id_bsestar',
            field=models.CharField(blank=True, max_length=15, null=True),
        ),
    ]
