from datetime import datetime, timedelta
from django.utils import timezone


def str_to_datetime(str_date, str_format="%Y-%m-%d %H:%M:%S"):
    return datetime.strptime(str_date, str_format).replace(tzinfo=timezone.get_current_timezone())


def str_to_date(str_date, str_format="%d-%m-%Y"):
    return datetime.strptime(str_date, str_format).date()


def date_range(start_date, n=10):
    """ Returns [(day, date)]
    """
    dates = []
    for i in range(n):
        dt = start_date + timedelta(days=i+1)
        dates.append((dt.day, dt))
    return dates
