import boto3
import logging

from django.conf import settings

logger = logging.getLogger(__name__)


class SMS:

    def __init__(self):
        self.client = boto3.client("sns",
                                   aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                                   aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                                   region_name='ap-south-1')

    def send(self, phone, message):
        try:
            self.client.publish(PhoneNumber='+91'+phone, Message=message)
            return True
        except Exception as e:
            logger.error(e)
            return False


def send_otp(phone, message):
    sms = SMS()
    return sms.send(phone, message)
