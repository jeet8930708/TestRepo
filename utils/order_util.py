import hashlib
import hmac
import secrets

from django.conf import settings
from django.utils import timezone


def generate_random_token(nbytes=3):
    return secrets.token_hex(nbytes)


def generate_bsestar_unique_ref_no(max_length=19):
    return f"{settings.BSESTAR_MEMBER_CODE}{str(timezone.localtime().timestamp()).replace('.', '')[:(max_length-5)]}"


def generate_billdesk_unique_ref_no(max_length=16):
    return str(timezone.localtime().timestamp()).replace('.', '')


def generate_checksum_sha256(key, msg, is_upper=True):
    checksum = hmac.new(key.encode(), msg.encode(), hashlib.sha256).hexdigest()
    if is_upper:
        checksum = checksum.upper()
    return checksum


def validate_checksum_sha256(key, msg, checksum):
    exp_checksum = generate_checksum_sha256(key, msg)
    return checksum == exp_checksum
