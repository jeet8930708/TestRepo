import base64
import os
import pdf2image
import tifftools
import requests
import logging

from uuid import uuid4
from django.core.files.base import ContentFile
from django.conf import settings

logger = logging.getLogger(__name__)


def base64_to_image(base64_string):
    if ';base64,' in base64_string:
        imgfmt, imgstr = base64_string.split(';base64,')
        ext = imgfmt.split('/')[-1]
    else:
        imgstr, ext = base64_string, 'jpg'
    return ContentFile(base64.b64decode(imgstr), name=uuid4().hex + "." + ext)


def convert_pdf_to_base64(pdf_path, tiff_path):
    save_dir = os.path.dirname(tiff_path)

    print("Converting pdf to tiff")
    pdf2image.convert_from_path(pdf_path, dpi=50, output_file=tiff_path, fmt="tiff")

    print("Merging tiff files")
    tiff_files_li = []
    for ti in os.listdir(save_dir):
        if '.tif' in ti:
            tiff_files_li.append(os.path.join(save_dir, ti))
    tifftools.tiff_concat(tiff_files_li, tiff_path, overwrite=True)

    print("Converting tiff to base64")
    data = open(tiff_path, 'rb').read()
    bytes_base64 = base64.b64encode(data)

    # import sys
    # print(sys.getsizeof(bytes_base64))
     
    return bytes_base64.decode()


def upload_file(data, ext, byte_file=False):
    if not byte_file:
        return ContentFile(base64.b64decode(data), name=uuid4().hex + "." + ext)
    return ContentFile(data, name=uuid4().hex + "." + ext)

def convert_image_to_base64(img_url):
    return base64.b64encode(requests.get(img_url).content)


def convert_pdf_url_to_base64(pdf_url):
    filename = pdf_url.rsplit('/', 1)[1].lower().replace('.pdf', '')
    tiff_path = settings.MEDIA_ROOT / f"temp/{filename}.tiff"
    pdf_path = settings.MEDIA_ROOT / f"temp/{filename}.pdf"
    with open(pdf_path, 'wb') as f:
        f.write(requests.get(pdf_url).content)
    result = convert_pdf_to_base64(pdf_path, tiff_path)
    for ti in os.listdir(settings.MEDIA_ROOT / "temp"):
        if ti.startswith(filename):
            os.remove(settings.MEDIA_ROOT / f"temp/{ti}")

    return result


def convert_obj_url_to_base64(obj_url):
    return convert_pdf_url_to_base64(obj_url) if obj_url.lower().endswith('pdf') else convert_image_to_base64(obj_url).decode()
