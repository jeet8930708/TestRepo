from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string

import logging
logger = logging.getLogger(__name__)


def send_email(subject, sender, receivers, message=None, html_message=None):
    send_mail(subject, message, sender, receivers, html_message=html_message, fail_silently=False)


def send_welcome_email(receiver):
    subject = "Welcome to FIKAA Community"
    html_message = render_to_string("email/community_welcome.html")
    logger.info("Sending welcome email")
    send_email(subject, settings.EMAIL_HOST_USER, [receiver], html_message=html_message)
