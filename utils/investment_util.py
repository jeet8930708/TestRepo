import logging
import os.path

import numpy as np
import pandas as pd

from django.db.models import F
from django.conf import settings
from django.core.cache import cache
from django.utils import timezone
from order.models import Order, OrderStatusEnum
from investment.models import Instrument
from api.rest import nav_history, nav_price, growth_10k

logger = logging.getLogger(__name__)

def _calc_totals(df):
    df['total_units'] = round(df['units'] - df['units_sell'], 4)
    df['investment'] = round(df['amount'] - df['amount_sell'], 0)
    df['current_value'] = round(df['total_units'] * df['current_nav'], 0)

    df['investment'] = df['investment'].astype(float)
    df['current_value'] = df['current_value'].astype(float)
    df['gain'] = round(df['current_value'] - df['investment'], 2)
    df['return'] = round(df['gain'] * 100 / df['investment'], 2)


def _get_user_sip_investments_df(user):
    order_det = Order.objects.filter(user=user, status=OrderStatusEnum.Completed.value, sip__isnull=False) \
                             .values('buy_sell',
                                     'amount',
                                     'units',
                                     'modified_on',
                                     inv_id=F('sip_id'),
                                     inv_basket_id=F('sip__sip_mandate_id'),
                                     fund_id=F('instrument_id'),
                                     fund_no=F('instrument__code'),
                                     fund_name=F('instrument__name'),
                                     basket_name=F('sip__sip_mandate__basket__name'),
                                     current_nav=F('instrument__current_nav_value'))
    if order_det:
        df = pd.DataFrame(list(order_det))

        # calculate average nav for each fund / basket
        # for sip, there can be multiple buy / sell transactions
        # hence it is neccesary to calculate avg nav for all buy / sell transactions
        df = df.groupby(['inv_basket_id', 'inv_id', 'basket_name', 'fund_id', 'fund_no', 'fund_name', 'buy_sell']) \
            .agg({'amount': np.sum, 'units': np.sum, 'current_nav': np.max, 'modified_on': np.max}) \
            .reset_index()
        df['nav'] = round(df['amount'] / df['units'], 4)

        df_sell = df[df['buy_sell'] == 2]
        if not df_sell.empty:
            df_sell = df_sell.rename(columns={'amount': 'amount_sell', 'units': 'units_sell', 'nav': 'nav_sell'})
            df_sell = df_sell.drop(['buy_sell', 'current_nav', 'modified_on'], axis=1)
            df = pd.merge(df[df['buy_sell'] == 1], df_sell,
                          on=['inv_basket_id', 'inv_id', 'basket_name', 'fund_id', 'fund_no', 'fund_name'],
                          how='left')
            df = df.fillna({
                'amount_sell': 0.0,
                'units_sell': 0.0,
                'nav_sell': 0.0})
        else:
            df = df.assign(amount_sell=0.0,
                           units_sell=0.0,
                           nav_sell=0.0)
        _calc_totals(df)
        df['order_type'] = 'sip'
        return df
    return  pd.DataFrame()


def _get_user_lumpsum_investments_df(user):
    order_det = Order.objects.filter(user=user, status=OrderStatusEnum.Completed.value,
                                     order_lumpsum__isnull=False) \
                             .values('buy_sell',
                                     'amount',
                                     'units',
                                     'modified_on',
                                     inv_id=F('id'),
                                     inv_basket_id=F('order_lumpsum_id'),
                                     fund_no=F('instrument__code'),
                                     fund_id=F('instrument_id'),
                                     fund_name=F('instrument__name'),
                                     basket_name=F('order_lumpsum__recommendation__basket__name'),
                                     current_nav=F('instrument__current_nav_value'))
    if order_det:
        df = pd.DataFrame(list(order_det))

        # check sell orders
        df_sell = df[df['buy_sell'] == 2]
        if not df_sell.empty:
            # get average sell details
            df_sell = df_sell.groupby(['inv_basket_id', 'fund_id', 'fund_no', 'fund_name', 'basket_name']) \
                             .agg({'amount': np.sum, 'units': np.sum}) \
                             .reset_index()
            df_sell['nav'] = round(df_sell['amount'] / df_sell['units'], 4)

            df_sell = df_sell.rename(columns={'amount': 'amount_sell', 'units': 'units_sell', 'nav': 'nav_sell'})

            df = pd.merge(df[df['buy_sell'] == 1], df_sell,
                          on=['inv_basket_id', 'fund_id', 'fund_no', 'fund_name', 'basket_name'],
                          how='left')
            df = df.fillna({
                'amount_sell': 0.0,
                'units_sell': 0.0,
                'nav_sell': 0.0})
        else:
            df = df.assign(amount_sell=0.0,
                           units_sell=0.0,
                           nav_sell=0.0)

        _calc_totals(df)
        df['order_type'] = 'lumpsum'
        return df
    return pd.DataFrame()


def get_user_investments(user, summary=False):
    df_sip = _get_user_sip_investments_df(user)
    df_lumpsum = _get_user_lumpsum_investments_df(user)

    if df_sip.empty and df_lumpsum.empty:
        data = {
            'investment_total': 0.0,
            'current_value_total': 0.0,
            'gain_total': 0.0,
            'return_total': 0.0
        }
        if not summary:
            data['baskets'] = []
        return data

    data = {}

    # convert to dataframe
    df = pd.concat([df_sip, df_lumpsum], ignore_index=True)
    df.fillna(0.0, inplace=True)
    df.sort_values('modified_on', inplace=True, ascending=False)
    # removing total_units less than 0.0
    df = df.loc[df['total_units'] > 0.0]

    if summary:
        _calc_totals(df)
        df['investment'] = round(df['amount'] - df['amount_sell'], 0)
        df['current_value'] = round(df['total_units'] * df['current_nav'], 0)
        investment_summary = int(df['investment'].sum())
        gain_summary = int(df['gain'].sum())
        current_summary = int(df['current_value'].sum()) 
        return {
            'investment_total': investment_summary,
            'current_value_total': current_summary,
            'gain_total': gain_summary,
            'return_total': round(gain_summary * 100 / investment_summary, 2) if investment_summary else 0.0 
        }

    # fetch fund logos
    df['fund_logo'] = df['fund_id'].map(get_fund_logos)

    data['baskets'] = []
    investment_total, current_value_total = 0.0, 0.0
    for group_name, df_basket in df.groupby(['inv_basket_id', 'basket_name'], sort=False):
        _calc_totals(df_basket)

        df_basket = df_basket.drop(['inv_basket_id', 'basket_name', 'buy_sell', 'amount', 'units',
                                    'current_nav', 'amount_sell', 'units_sell', 'nav_sell'], axis=1)
        df_basket['sip_id'] = df_basket['inv_id']   # for backward compatibility

        basket_investment = int(df_basket['investment'].sum())
        basket_current_value = int(df_basket['current_value'].sum())
        basket_gain = basket_current_value - basket_investment
        basket_return = round(basket_gain * 100 / basket_investment, 2)

        investment_total += basket_investment
        current_value_total += basket_current_value

        data['baskets'].append({
            'basket_name': group_name[1],
            'basket_investment': basket_investment,
            'basket_current_value': basket_current_value,
            'basket_gain': basket_gain,
            'basket_return': basket_return,
            'basket_funds': df_basket.to_dict('records')
        })

    data['investment_total'] = int(investment_total)
    data['current_value_total'] = int(current_value_total)
    data['gain_total'] = int(current_value_total) - int(investment_total)
    data['return_total'] = 0.0 if investment_total == 0.0 \
                            else round(data['gain_total'] * 100 / data['investment_total'], 2)
    return data


def get_fund_logos(fund_id=None):
    key = 'fund_logo'
    fund_logos = cache.get(key)
    if fund_logos:
        if fund_id:
            return fund_logos.get(fund_id)
        return fund_logos
    fund_logos = {inst.id: inst.logo.url if inst.logo else None for inst in Instrument.objects.filter(is_active=True)}
    cache.set(key, fund_logos, None)
    return fund_logos


def fetch_index_nav_from_morning_star(refetch_all=False):
    index_nav_path = settings.DATA_DIR / 'investment/nav_index_fund.csv'
    fund_mstar_id = 'F00000W42G'
    df = pd.DataFrame(columns=['nav_date', 'nav_value'])
    if not refetch_all and os.path.exists(index_nav_path):
        logger.info("Fund nav history exists. Fetching current nav")
        df = pd.read_csv(index_nav_path)
        status, data = nav_price(fund_mstar_id)
        if status == 'Success':
            logger.info(f"Updating current nav: {data}")
            new_df = pd.DataFrame({'nav_date': [data['DP-DayEndDate']], 'nav_value': [float(data['DP-DayEndNAV'])]})
            df = pd.concat([df, new_df], ignore_index=True)
            df.drop_duplicates('nav_date', inplace=True)
            df.sort_values('nav_date', inplace=True, ascending=False)
            df.to_csv(index_nav_path, index=False)
    else:
        logger.info("Fetching fund historical nav from morning star")
        status, data = nav_history(fund_mstar_id, '2017-03-01', timezone.localdate().strftime('%Y-%m-%d'))
        if status == 'Success':
            logger.info("Saving historical nav...")
            nav_data = []
            for prices in data:
                nav_data.append([prices['d'], float(prices['v'])])
            if nav_data:
                df = pd.DataFrame(nav_data, columns=['nav_date', 'nav_value'])
                df.sort_values('nav_date', inplace=True, ascending=False)
                df.to_csv(index_nav_path, index=False)
    return df.to_dict('records')


def fetch_index_nav_history():
    index_nav_path = settings.DATA_DIR / 'investment/nav_index_fund.csv'
    if os.path.exists(index_nav_path):
        logger.info('Index nav history exists')
        df = pd.read_csv(index_nav_path)
        return df.to_dict('records')
    else:
        logger.info('Index nav history does not exists. Fetching from morning star.')
        return fetch_index_nav_from_morning_star()


def fetch_growth_10k(mstarid, start_date='2017-03-01', refetch=False):
    fund_growth10k_path = settings.DATA_DIR / f'investment/growth_10k_{mstarid}.csv'
    if refetch or not os.path.exists(fund_growth10k_path):
        logger.info("Fetching growth 10k history")
        status, data = growth_10k(mstarid, start_date, timezone.localdate().strftime('%Y-%m-%d'))
        if status == 'Success':
            logger.info("Saving historical growth 10k...")
            nav_data = []
            for prices in data:
                nav_data.append([prices['d'], float(prices['v'].replace(',', ''))])
            if nav_data:
                df = pd.DataFrame(nav_data, columns=['date', 'value'])
                df.sort_values('date', inplace=True, ascending=False)
                df.to_csv(fund_growth10k_path, index=False)
    else:
        logger.info('fetch growth 10k details')
        df = pd.read_csv(fund_growth10k_path)
    return df.to_dict('records')[0]
