
from rest_framework.serializers import (
    ModelSerializer,
    CharField,
    SerializerMethodField,
)

from .models import (
    Event,
    EventRegistration
)


class EventRegistrationSerializer(ModelSerializer):

    user_name  = CharField(source="user.get_full_name", read_only=True)
    user_image = CharField(source="user.image_url", read_only=True)

    class Meta:
        model = EventRegistration
        fields = ["id", 'event', 'user', 'user_name', 'user_image', 'is_active']
        read_only_fields = ("id", 'user')

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return super().create(validated_data)


class EventSerializer(ModelSerializer):
    category_name = CharField(source='category.name', read_only=True)
    is_registered = SerializerMethodField()
    registrations = EventRegistrationSerializer(many=True, required=False)

    class Meta:
        model = Event
        fields = ['id', 'name', 'description', 'author_name', 'location', 'zoom_link', 'banner_link', 'event_time',
                  'created_by', 'category_name', 'is_paid', 'base_amount', 'category', 'is_registered',
                  'registration_count', 'registrations']
        read_only_fields = ('id', 'created_by', 'registration_count', 'registrations')

    def get_is_registered(self, obj):
        request = self.context["request"]
        return EventRegistration.objects.filter(event=obj, user=request.user).exists()

    def create(self, validated_data):
        request = self.context['request']
        validated_data['created_by'] = request.user
        return super().create(validated_data)



