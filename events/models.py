from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MinValueValidator
from master.models import Category
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from rest_framework.serializers import ValidationError

User = get_user_model()


class Event(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    author_name = models.CharField(max_length=100, blank=True, null=True)
    location = models.CharField(max_length=100, blank=True, null=True)
    zoom_link = models.CharField(max_length=200, blank=True, null=True)
    banner_link = models.CharField(max_length=200, blank=True, null=True)
    event_time = models.DateTimeField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE,  related_name="events")
    is_paid = models.BooleanField(default=False)
    base_amount = models.DecimalField(default=0.0, max_digits=6, decimal_places=2, validators=[MinValueValidator(0.0)])
    tax_percentage = models.FloatField(default=0.0)
    commission_percentage = models.FloatField(default=0.0)
    registration_count = models.PositiveSmallIntegerField(default=0)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def update_registration_count(self):
        self.registration_count = self.registrations.filter(is_active=True).count()
        self.save(update_fields=['registration_count'])


@receiver(pre_save, sender=Event)
def validate_base_amount(sender, instance, update_fields=None, **kwargs):
    if update_fields is None and instance.is_paid and not instance.base_amount:
        raise ValidationError("base_amount should be greater than 0.0 for paid events")


class EventRegistration(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='events_registered')
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='registrations')
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("user", "event")


@receiver(post_delete, sender=EventRegistration)
@receiver(post_save, sender=EventRegistration)
def validate_base_amount(sender, instance, **kwargs):
    instance.event.update_registration_count()
