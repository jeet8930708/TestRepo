from django.utils import timezone
from rest_framework.generics import ListAPIView, CreateAPIView, ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.status import HTTP_200_OK
from utils.response import response_200

from .models import Event, EventRegistration
from .serializers import (
    EventSerializer,
    EventRegistrationSerializer
)
import datetime

# Create your views here.


class EventCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Event created successfully", response.data)


class EventListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = EventSerializer
    queryset = Event.objects.all().order_by('event_time')

    def list(self, request, *args, **kwargs):
        res = super().list(request, *args, **kwargs)
        results = res.data
        results_len = results["count"]
        response = {
            'status_code': HTTP_200_OK,
            'status_message': '{} record(s) found'.format(results_len) if results_len > 0 else 'No record found',
            'count': results_len,
            'previous': results['previous'],
            'next': results['next'],
            'results': results['results'],
        }
        return response_200("Event list fetched successfully", response)


class EventUpcomingListView(EventListAPIView):
    queryset = Event.objects.filter(event_time__gte=timezone.now()).order_by('event_time')


class EventPastListView(EventListAPIView):
    queryset = Event.objects.filter(event_time__lt=timezone.now()).order_by('-event_time')


# ========================================================================
class EventRetrieveUpdateView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        response.data["is_registered"] = False
        event = response.data
        registered = EventRegistration.objects.filter(event=event["id"], user=request.user).first()
        if registered is not None:
            response.data["is_registered"] = True
        return response_200("Event details retrieved successfully", response.data)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        return response_200("Event updated successfully", response.data)


class EventRegistrationListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = EventRegistrationSerializer
    queryset = EventRegistration.objects.all()

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("EventRegistration created successfully", response.data)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("EventRegistration list fetched successfully", response.data)


# ========================================================================
class EventRegistrationRetrieveUpdateView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = EventRegistrationSerializer
    queryset = EventRegistration.objects.all()

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("EventRegistration details retrieved successfully", response.data)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        return response_200("EventRegistration updated successfully", response.data)
