from django.urls import path

from .views import (
    EventCreateView,
    EventUpcomingListView,
    EventPastListView,
    EventRetrieveUpdateView,
    EventRegistrationListCreateView,
    EventRegistrationRetrieveUpdateView
)

urlpatterns = [
    path('create/', EventCreateView.as_view(), name='event-create'),
    path('list/upcoming/', EventUpcomingListView.as_view(), name='event-list-upcoming'),
    path('list/past/', EventPastListView.as_view(), name='event-list-past'),
    path('retrieve/<int:pk>/', EventRetrieveUpdateView.as_view(), name='event-retrieve'),
    path('update/<int:pk>/', EventRetrieveUpdateView.as_view(), name='event-update'),

    path('registration/create/', EventRegistrationListCreateView.as_view(), name='event-registration-create'),
    path('registration/list/', EventRegistrationListCreateView.as_view(), name='event-registration-list'),
    path('registration/retrieve/<int:pk>/', EventRegistrationRetrieveUpdateView.as_view(),
         name='event-registration-retrieve'),
    path('registration/update/<int:pk>/', EventRegistrationRetrieveUpdateView.as_view(),
         name='event-registration-update'),
]
