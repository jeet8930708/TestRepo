from django.contrib import admin
from .models import Event, EventRegistration

# Register your models here.


class EventRegistrationInline(admin.TabularInline):
    model = EventRegistration
    raw_id_fields = ('user',)
    extra = 0


class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'author_name', 'category', 'location', 'event_time', 'is_paid', 'base_amount')
    list_filter = ('category', 'event_time')
    search_fields = ('name', 'category__name', 'location', 'author_name')
    inlines = [EventRegistrationInline]
admin.site.register(Event, EventAdmin)
