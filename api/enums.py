from core.enums import ChoiceEnum


class ApiParamTypeEnum(ChoiceEnum):
    String = 1
    Integer = 2
    File = 3


class ApiRequestMethodEnum(ChoiceEnum):
    GET = 'GET'
    POST = 'POST'


class ApiRequestTypeEnum(ChoiceEnum):
    REST = 1
    SOAP = 2
