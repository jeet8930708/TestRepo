import importlib


def derive_class_instance(path):
    mod_path, cls_name = path.rsplit('.', 1)
    mod = importlib.import_module(mod_path)
    return getattr(mod, cls_name)()

