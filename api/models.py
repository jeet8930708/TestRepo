from django.contrib.auth import get_user_model
from django.db import models
# Create your models here.

User = get_user_model()


class ApiLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='third_party_api_requests',
                             blank=True, null=True)
    api = models.CharField(max_length=1024, help_text='API URL')
    headers = models.TextField(blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    method = models.CharField(max_length=10, db_index=True)
    response = models.TextField()
    status_code = models.PositiveSmallIntegerField(help_text='Response status code', db_index=True)
    request_datetime = models.DateTimeField()
    execution_time = models.DecimalField(decimal_places=5, max_digits=8,
                                         help_text='Server execution time in seconds (Not complete response time.)')

    def __str__(self):
        return self.api

    class Meta:
        ordering = ('-request_datetime',)
        verbose_name = 'API Log'
        verbose_name_plural = 'API Logs'
