import requests
import xmltodict
import logging

from requests.structures import CaseInsensitiveDict
from django.conf import settings

from api.provider.bsestar import BSESTAR
from api.enums import ApiRequestTypeEnum
from .base import EndpointBase

logger = logging.getLogger(__name__)


class UCCRegistrationEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/StarMFCommonAPI/ClientMaster/Registration"

    @property
    def request_headers(self):
        return {
            'content-type': 'application/json'
        }

    @property
    def request_data(self):
        return [
            {'key': 'UserId',
             'dtype': str,
             'mandatory': True
            },
            {'key': 'MemberCode',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'Password',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'RegnType',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'Param',
             'dtype': str,
             'mandatory': True
             },
        ]


class MFAPIEndpoint(EndpointBase):
    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        if settings.ENV == "PRD":
            return "/StarMFWebService/StarMFWebService.svc/Secure"
        return "/MFUploadService/MFUploadService.svc/Secure"

    @property
    def request_type(self):
        """
        REST / SOAP
        """
        return ApiRequestTypeEnum.SOAP.value

    @property
    def request_headers(self):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/soap+xml"
        return headers

    @property
    def request_data(self):
        return []

    @property
    def service_name(self):
        return ''


class GetPasswordMFAPIEndpoint(MFAPIEndpoint):

    @property
    def service_name(self):
        return 'getPassword'


class FATCAUploadEndpoint(MFAPIEndpoint):
    @property
    def service_name(self):
        return 'MFAPI'


class MandateRegistrationEndpoint(MFAPIEndpoint):
    @property
    def service_name(self):
        return 'MFAPI'

    def build_soap_response(self, res, content):
        response = {}
        content = xmltodict.parse(res.content)
        status_code = res.status_code
        if status_code != 200:
            response['status_message'] = content['s:Envelope']['s:Body']['s:Fault']['s:Reason']
        else:
            result = content['s:Envelope']['s:Body'][f'{self.service_name}Response'][f'{self.service_name}Result']
            res = result.split('|')
            status = res[0]
            message = res[1]
            response['status_message'] = message
            if status != "100":
                status_code = 400
            else:
                response['data'] = {"mandate_id": res[2]}
        return response, status_code


class OrderPaymentStatusMFAPIEndpoint(MFAPIEndpoint):

    @property
    def service_name(self):
        return 'MFAPI'


class EMandateAuthURLEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/StarMFwebService/StarMFWebService.svc/EMandateAuthURL"

    @property
    def request_headers(self):
        return {
            'content-type': 'application/json'
        }

    @property
    def request_data(self):
        return [
            {'key': 'UserId',
             'dtype': str,
             'mandatory': True
            },
            {'key': 'MemberCode',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'Password',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'ClientCode',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'MandateID',
             'dtype': str,
             'mandatory': True
             },
        ]


class FileUploadServiceEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/StarMFFileUploadService/StarMFFileUploadService.svc/Secure"

    @property
    def request_type(self):
        """
        REST / SOAP
        """
        return ApiRequestTypeEnum.SOAP.value

    @property
    def request_headers(self):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/soap+xml"
        return headers

    @property
    def request_data(self):
        return []

    @property
    def service_name(self):
        return ''

    def build_soap_response(self, res, content):
        response = {}
        content = xmltodict.parse(res.content)
        status_code = res.status_code
        if status_code != 200:
            response['status_message'] = content['s:Envelope']['s:Body']['s:Fault']['s:Reason']
        else:
            result = content['s:Envelope']['s:Body'][f'{self.service_name}Response'][f'{self.service_name}Result']
            status = result['b:Status']
            data = result['b:ResponseString']
            response['status_message'] = data
            if status != "100":
                status_code = 400
        return response, status_code


class GetPasswordFileUploadEndpoint(FileUploadServiceEndpoint):

    @property
    def service_name(self):
        return 'GetPassword'


class AOFImageUploadEndpoint(FileUploadServiceEndpoint):

    @property
    def service_name(self):
        return 'UploadFile'


class MFOrderEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/MFOrderEntry/MFOrder.svc/Secure"

    @property
    def request_type(self):
        """
        REST / SOAP
        """
        return ApiRequestTypeEnum.SOAP.value

    @property
    def request_headers(self):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/soap+xml"
        return headers

    @property
    def request_data(self):
        return []

    @property
    def service_name(self):
        return ''


class GetPasswordMFOrderEndpoint(MFOrderEndpoint):

    @property
    def service_name(self):
        return 'getPassword'


class XSIPOrderEntryEndpoint(MFOrderEndpoint):

    @property
    def service_name(self):
        return 'xsipOrderEntryParam'

    def build_soap_response(self, res, content):
        response = {}
        content = xmltodict.parse(res.content)
        status_code = res.status_code
        if status_code != 200:
            response['status_message'] = content['s:Envelope']['s:Body']['s:Fault']['s:Reason']
        else:
            # Success: NEW|202208092913201022|29132|9833854753|2913201|476298|X-SIP HAS BEEN REGISTERED, REG NO IS : 476298|0|7763609
            # Failure: NEW|202208092913201022|29132|9833854753|2913201|0|FAILED: DUPLICAT UNIQUE REF NO.|1|0
            result = content['s:Envelope']['s:Body'][f'{self.service_name}Response'][f'{self.service_name}Result']
            res = result.split('|')
            success_flag = res[-2]
            if success_flag != "0":
                status_code = 400
                response['status_message'] = res[6]
            else:
                response['status_message'] = res[5]
        return response, status_code


class OrderEntryEndpoint(MFOrderEndpoint):

    @property
    def service_name(self):
        return 'orderEntryParam'

    def build_soap_response(self, res, content):
        response = {}
        content = xmltodict.parse(res.content)
        status_code = res.status_code
        if status_code != 200:
            response['status_message'] = content['s:Envelope']['s:Body']['s:Fault']['s:Reason']
        else:
            # Success: NEW|29132011308202212|7532017| loginid | membercode | ucc |ORD CONF:
            # Your Request for FRESH REDEMPTION 100.000 UNITS in SCHEME: 02-DP THRO : PHYSICAL is confirmed for
            # CLIENT : clientname (Code: ucc) CONFIRMATION TIME: Jun 2 2022 12:30PM ENTRY BY: ORDER NO:
            # 7532017|0
            # Failure: NEW|29132011308202212|0|2913201|29132|9833854753|FAILED : MENTION BANK ACCOUNT NOT VERIFIED|1
            result = content['s:Envelope']['s:Body'][f'{self.service_name}Response'][f'{self.service_name}Result']
            res = result.split('|')
            success_flag = res[-1]
            if success_flag != "0":
                status_code = 400
                response['status_message'] = res[6]
            else:
                response['status_message'] = res[2]
        return response, status_code


class OrderStatusEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/StarMFWebService/StarMFWebService.svc/Secure"

    @property
    def request_type(self):
        """
        REST / SOAP
        """
        return ApiRequestTypeEnum.SOAP.value

    @property
    def request_headers(self):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/soap+xml"
        return headers

    @property
    def request_data(self):
        return []

    @property
    def service_name(self):
        return 'OrderStatus'

    def build_soap_response(self, res, content):
        response = {}
        content = xmltodict.parse(res.content)
        status_code = res.status_code
        if status_code != 200:
            response['status_message'] = content['s:Envelope']['s:Body']['s:Fault']['s:Reason']
        else:
            result = content['s:Envelope']['s:Body'][f'{self.service_name}Response'][f'{self.service_name}Result']
            status = result['b:Status']
            if status != "100":
                status_code = 400
                response['data'] = None
            else:
                response['data'] = result['b:OrderDetails']['b:OrderDetails']
            response['status_message'] = result['b:Message']
        return response, status_code


class PaymentGatewayEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/StarMFSinglePaymentAPI/Single/Payment"

    @property
    def request_headers(self):
        return {
            'content-type': 'application/json'
        }

    @property
    def request_data(self):
        return []

    def build_rest_response(self, res, content):
        response = {}
        if res.status_code == 200:
            if content["statuscode"] == "100":
                response['status_message'] = 'Success'
                response['data'] = content
            else:
                response['status_message'] = content["responsestring"]
                return response, 400
        elif res.status_code >= 500:
            response['status_message'] = content
        else:
            response['status_message'] = 'Error'
            response['error'] = content.get('message')
        return response, res.status_code


class GetPasswordChildOrderEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/StarMFWebService/StarMFWebService.svc/GetPasswordForChildOrder"

    @property
    def request_headers(self):
        return {
            'content-type': 'application/json'
        }

    @property
    def request_data(self):
        return [
            {'key': 'UserId',
             'dtype': str,
             'mandatory': True
            },
            {'key': 'MemberId',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'Password',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'PassKey',
             'dtype': str,
             'mandatory': True
             },
        ]


class ChildOrderDetailsEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/StarMFWebService/StarMFWebService.svc/ChildOrderDetails"

    @property
    def request_headers(self):
        return {
            'content-type': 'application/json'
        }

    @property
    def request_data(self):
        return [
            {'key': 'Date',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'MemberCode',
             'dtype': str,
             'mandatory': True
            },
            {'key': 'ClientCode',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'SystematicPlanType',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'RegnNo',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'EncryptedPassword',
             'dtype': str,
             'mandatory': True
             },
        ]


class AllotmentStatementEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/StarMFWebService/StarMFWebService.svc/Secure"

    @property
    def request_type(self):
        """
        REST / SOAP
        """
        return ApiRequestTypeEnum.SOAP.value

    @property
    def request_headers(self):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/soap+xml"
        return headers

    @property
    def request_data(self):
        return []

    @property
    def service_name(self):
        return 'AllotmentStatement'

    def build_soap_response(self, res, content):
        response = {}
        content = xmltodict.parse(res.content)
        status_code = res.status_code
        if status_code != 200:
            response['status_message'] = content['s:Envelope']['s:Body']['s:Fault']['s:Reason']
        else:
            result = content['s:Envelope']['s:Body'][f'{self.service_name}Response'][f'{self.service_name}Result']
            status = result['b:Status']
            if status != "100":
                status_code = 400
                response['data'] = None
            else:
                response['data'] = result['b:AllotmentDetails']['b:AllotmentDetails']
            response['status_message'] = result['b:Message']
        return response, status_code


class RedemptionStatementEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/StarMFWebService/StarMFWebService.svc/Secure"

    @property
    def request_type(self):
        """
        REST / SOAP
        """
        return ApiRequestTypeEnum.SOAP.value

    @property
    def request_headers(self):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/soap+xml"
        return headers

    @property
    def request_data(self):
        return []

    @property
    def service_name(self):
        return 'RedemptionStatement'

    def build_soap_response(self, res, content):
        response = {}
        content = xmltodict.parse(res.content)
        status_code = res.status_code
        if status_code != 200:
            response['status_message'] = content['s:Envelope']['s:Body']['s:Fault']['s:Reason']
        else:
            result = content['s:Envelope']['s:Body'][f'{self.service_name}Response'][f'{self.service_name}Result']
            status = result['b:Status']
            if status != "100":
                status_code = 400
                response['data'] = None
            else:
                response['data'] = result['b:RedemptionDetails']['b:RedemptionDetails']
            response['status_message'] = result['b:Message']
        return response, status_code


class Redeem2FAEndpoint(EndpointBase):

    @property
    def provider(self):
        return BSESTAR

    @property
    def api_endpoint(self):
        return "/BSEMFWEBAPI/api/2FAAuthController/_2FAAuthunetication/w"

    @property
    def request_headers(self):
        return {
            'content-type': 'application/json'
        }

    @property
    def request_data(self):
        return [
            {'key': 'LoginID',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'Password',
             'dtype': str,
             'mandatory': True
            },
            {'key': 'MemberCode',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'ClientCode',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'LoopbackReturnUrl',
             'dtype': str,
             'mandatory': True
             },
            {'key': 'AllowLoopBack',
             'dtype': str,
             'mandatory': True
             },
        ]
