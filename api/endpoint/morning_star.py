from api.provider.morningstar import MORNINGSTAR
from api.enums import ApiRequestMethodEnum
from .base import EndpointBase


class MFDetailEndpoint(EndpointBase):

    ISIN = ""

    @property
    def provider(self):
        return MORNINGSTAR

    @property
    def api_endpoint(self):
        return f'/v2/service/mf/sgsbcs9k99thlrnt/isin/{self.ISIN}?accesscode={self.provider.client_secret}&format=json'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.GET.value

    @property
    def request_data(self):
        return []


class NAVHistoryEndpoint(EndpointBase):

    MSTARID = ""
    START_DATE = ""
    END_DATE = ""

    @property
    def provider(self):
        return MORNINGSTAR

    @property
    def api_endpoint(self):
        return f'/service/mf/Price/mstarid/{self.MSTARID}?accesscode={self.provider.client_secret}' \
               f'&startdate={self.START_DATE}&enddate={self.END_DATE}&format=json'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.GET.value

    @property
    def request_data(self):
        return []


class NAVPriceEndpoint(EndpointBase):

    MSTARID = ""

    @property
    def provider(self):
        return MORNINGSTAR

    @property
    def api_endpoint(self):
        return f'/v2/service/mf/f6rur72ppnetw8i2/mstarid/{self.MSTARID}?accesscode={self.provider.client_secret}' \
               f'&format=json'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.GET.value

    @property
    def request_data(self):
        return []


class GrowthOf10KEndpoint(EndpointBase):

    MSTARID = ""
    START_DATE = ""
    END_DATE = ""

    @property
    def provider(self):
        return MORNINGSTAR

    @property
    def api_endpoint(self):
        return f'/service/mf/GrowthOf10K/mstarid/{self.MSTARID}?accesscode={self.provider.client_secret}' \
               f'&startdate={self.START_DATE}&enddate={self.END_DATE}&format=json'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.GET.value

    @property
    def request_data(self):
        return []
