import base64, requests, json
import xmltodict
import logging
import traceback

from time import time
from abc import ABC, abstractmethod
from django.utils import timezone
from api.models import ApiLog
from api.enums import ApiRequestTypeEnum, ApiRequestMethodEnum

logger = logging.getLogger('api')


class EndpointBase(ABC):

    @property
    @abstractmethod
    def provider(self):
        """
        API provider
        Return: str
        """
        pass

    @property
    @abstractmethod
    def api_endpoint(self):
        """
        API endpoint
        Return: str
        """
        pass

    @property
    def request_type(self):
        """
        REST / SOAP
        """
        return ApiRequestTypeEnum.REST.value

    @property
    def request_method(self):
        """
        API request method
        Return: GET / POST
        """
        return ApiRequestMethodEnum.POST.value

    @property
    def request_params(self):
        """
        API request params
        """
        return None

    @property
    def request_data(self):
        """
        API request data
        Return: (
            (param_key, param_datatype, is_mandatory),
            (param_key, param_datatype, is_mandatory),
        )
        """
        return ()

    @property
    def request_headers(self):
        usr_pass = f"{self.provider.client_id}:{self.provider.client_secret}"
        b64_val = base64.b64encode(usr_pass.encode()).decode()
        return {
            'authorization': f'Basic  {b64_val}',
            'content-type': 'application/json'
        }

    @property
    def request_url(self):
        return f"{self.provider.base_url}{self.api_endpoint}"

    @property
    def service_name(self):
        """ used for soap request to fetch response """
        return ''

    def _validate_request_data(self, data):
        mandatory_missing = []
        datatype_mismatch = []
        # self.request_data = [
        #             {'key': 'id_no',
        #              'dtype': str,
        #              'mandatory': True
        #             }
        #         ]
        for d in self.request_data:
            if d['key'] not in data or (d['key'] in data and d['mandatory'] and data[d['key']] in (None, "")):
                mandatory_missing.append(d['key'])
            elif not isinstance(data[d['key']], d['dtype']):
                datatype_mismatch.append(d['key'])
        errors = []
        if mandatory_missing:
            errors.append(f"Mandatory params missing: {', '.join(mandatory_missing)}")
        if datatype_mismatch:
            errors.append(f"Incorrect datatype: {', '.join(datatype_mismatch)}")
        return errors

    def build_rest_response(self, res, content):
        response = {}
        if res.status_code == 200:
            response['status_message'] = 'Success'
            response['data'] = content
        elif res.status_code >= 500:
            response['status_message'] = content
        else:
            response['status_message'] = 'Error'
            response['error'] = content.get('message')
        return response, res.status_code

    def build_soap_response(self, res, content):
        response = {}
        content = xmltodict.parse(res.content)
        status_code = res.status_code
        if status_code != 200:
            logger.error(res.content)
            response['status_message'] = content['s:Envelope']['s:Body']['s:Fault']['s:Reason']
        else:
            result = content['s:Envelope']['s:Body'][f'{self.service_name}Response'][f'{self.service_name}Result']
            status, data = result.split('|')
            response['status_message'] = data
            if status != "100":
                status_code = 400
        return response, status_code

    def build_response(self, res, content):
        if self.request_type == ApiRequestTypeEnum.SOAP.value:
            return self.build_soap_response(res, content)
        return self.build_rest_response(res, content)

    def _send_rest_request(self, data):
        res = requests.request(method=self.request_method,
                               url=self.request_url,
                               headers=self.request_headers,
                               data=json.dumps(data) if data else None
                               )
        content = (res.content if ('/client/kyc/v2/media/' in self.request_url) else res.content if '/v2/client/document/download' in self.request_url else json.loads(res.content.decode())) \
                  if res.status_code < 500 else res.reason
        return res, content

    def _send_soap_request(self, data):
        res = requests.post(self.request_url, headers=self.request_headers, data=data)
        return res, res.content

    def _request(self, data):
        if self.request_type == ApiRequestTypeEnum.SOAP.value:
            return self._send_soap_request(data)
        return self._send_rest_request(data)

    def send_request(self, user, data=None):
        """
        Send third party api request
        """
        errors = None
        # validate request data
        if self.request_type == ApiRequestTypeEnum.REST.value:
            errors = self._validate_request_data(data)
            if errors:
                return {
                    'status_code': 500,
                    'status_message': 'Error',
                    'error': " | ".join(errors)
                }

        response = {}

        # log request
        request_datetime = timezone.now()
        try:
            api_log = ApiLog(user=user,
                             api=self.request_url,
                             headers=self.request_headers,
                             body=data,
                             method=self.request_method,
                             request_datetime=request_datetime
                             )
        except Exception as e:
            traceback.print_exc()
            return {
                'status_code': 500,
                'status_message': 'Error',
                'error': e
            }

        # send request
        try:
            logger.info(f"Request: {self.request_url}")
            t1 = time()
            res, content = self._request(data)
            t2 = time()
            logger.info(f"Request: {self.request_url} response received in {(t2 - t1):.4f}s")
            response, status_code = self.build_response(res, content)
            api_log.response = content
            api_log.status_code = status_code
        except Exception as e:
            traceback.format_exc()
            api_log.status_code = 500
            api_log.response = e
            response['status_message'] = 'Error'
            response['error'] = api_log.response
            t2 = time()
            logger.error(f"Request: {self.request_url} failed in {(t2 - t1):.4f}s")
        response['status_code'] = api_log.status_code

        api_log.execution_time = (timezone.now() - request_datetime).total_seconds()
        api_log.save()
        return response
