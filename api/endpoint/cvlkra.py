import logging
import xmltodict

from requests.structures import CaseInsensitiveDict

from api.provider.cvlkra import CVLKRA
from api.enums import ApiRequestTypeEnum
from .base import EndpointBase

logger = logging.getLogger(__name__)


class CVLKRABase(EndpointBase):

    @property
    def provider(self):
        return CVLKRA

    @property
    def request_type(self):
        """
        REST / SOAP
        """
        return ApiRequestTypeEnum.REST.value

    @property
    def api_endpoint(self):
        return ""

    @property
    def service_name(self):
        return ''

    @property
    def request_headers(self):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = 'text/xml;'
        return headers

    @property
    def request_data(self):
        return []

    def build_soap_response(self, res, content):
        response = {}
        content = xmltodict.parse(res.content)
        status_code = res.status_code
        if status_code != 200:
            response['status_message'] = content['s:Envelope']['s:Body']['s:Fault']['faultstring']
        else:
            result = content['s:Envelope']['s:Body'][f'{self.service_name}Response'][f'{self.service_name}Result']
            status, data = result.split('|')
            response['status_message'] = data
            if status != "100":
                status_code = 400
        return response, status_code


class GetPasswordEndpoint(CVLKRABase):

    @property
    def request_headers(self):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = 'text/xml'
        headers["SOAPAction"] = 'https://krapancheck.cvlindia.com/ICVLPanInquiry/GetPassword'
        return headers

    @property
    def api_endpoint(self):
        return "/CVLPanInquiry.svc"

    @property
    def service_name(self):
        return 'GetPassword'


class InsertUpdateKYCRecordEndpoint(EndpointBase):

    @property
    def provider(self):
        return CVLKRA

    @property
    def api_endpoint(self):
        return "/CVLPanInquiry.svc"

    @property
    def request_type(self):
        """
        REST / SOAP
        """
        return ApiRequestTypeEnum.SOAP.value

    @property
    def request_headers(self):
        headers = CaseInsensitiveDict()
        headers["Content-Type"] = "application/soap+xml"
        return headers

    @property
    def request_data(self):
        return []

    @property
    def service_name(self):
        return 'InsertUpdateKYCRecord'
