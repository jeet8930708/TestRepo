from api.provider.digio import DIGIO
from api.enums import ApiRequestMethodEnum
from .base import EndpointBase


class CKYCStatusCheckEndpoint(EndpointBase):

    @property
    def provider(self):
        return DIGIO

    @property
    def api_endpoint(self):
        return '/v3/client/kyc/ckyc/search'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.POST.value

    @property
    def request_data(self):
        return [
            {'key': 'id_no',
             'dtype': str,
             'mandatory': True
            }
        ]


class KRACheckPanStatusEndpoint(EndpointBase):

    @property
    def provider(self):
        return DIGIO

    @property
    def api_endpoint(self):
        return '/v3/client/kyc/kra/get_pan_details'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.POST.value

    @property
    def request_data(self):
        return [
            {
                'key': 'pan_no',
                'dtype': str,
                'mandatory': True
            }
        ]


class KYCRequestCreateEndpoint(EndpointBase):

    @property
    def provider(self):
        return DIGIO

    @property
    def api_endpoint(self):
        return '/client/kyc/v2/request/with_template'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.POST.value

    @property
    def request_data(self):
        return [
            {
                'key': 'customer_identifier',
                'dtype': str,
                'mandatory': True
            },
            {
                'key': 'template_name',
                'dtype': str,
                'mandatory': True
            },
            {
                'key': 'notify_customer',
                'dtype': bool,
                'mandatory': True
            },
            {
                'key': 'request_details',
                'dtype': dict,
                'mandatory': True
            },
            {
                'key': 'generate_access_token',
                'dtype': bool,
                'mandatory': True
            }
        ]


class KYCRequestDetailEndpoint(EndpointBase):

    REQUEST_ID = ""

    @property
    def provider(self):
        return DIGIO

    @property
    def api_endpoint(self):
        return '/client/kyc/v2/{request_id}/response'.format(request_id=self.REQUEST_ID)

    @property
    def request_method(self):
        return ApiRequestMethodEnum.POST.value

    @property
    def request_data(self):
        return []


class KYCRequestMediaEndpoint(EndpointBase):

    FILE_ID = ""
    DOC_TYPE = ""

    @property
    def provider(self):
        return DIGIO

    @property
    def api_endpoint(self):
        endpoint = '/client/kyc/v2/media/{file_id}'.format(file_id=self.FILE_ID)
        if self.DOC_TYPE:
            endpoint += "?doc_type={doc_type}".format(doc_type=self.DOC_TYPE)
        return endpoint

    @property
    def request_method(self):
        return ApiRequestMethodEnum.GET.value

    @property
    def request_data(self):
        return []


class CKYCDataDownloadEndpoint(EndpointBase):

    @property
    def provider(self):
        return DIGIO

    @property
    def api_endpoint(self):
        return '/v3/client/kyc/ckyc/download'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.POST.value

    @property
    def request_data(self):
        return [
            {
                'key': 'ckyc_no',
                'dtype': str,
                'mandatory': True
            },
            {
                'key': 'date_of_birth',
                'dtype': str,
                'mandatory': True
            }
        ]


class BankVerificationEndpoint(EndpointBase):

    @property
    def provider(self):
        return DIGIO

    @property
    def api_endpoint(self):
        return '/client/verify/bank_account'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.POST.value

    @property
    def request_data(self):
        return [
            {
                'key': 'beneficiary_account_no',
                'dtype': str,
                'mandatory': True
            },
            {
                'key': 'beneficiary_ifsc',
                'dtype': str,
                'mandatory': True
            }
        ]


class EsignRequestEndpoint(EndpointBase):

    @property
    def provider(self):
        return DIGIO

    @property
    def api_endpoint(self):
        return '/v2/client/template/multi_templates/create_sign_request'

    @property
    def request_method(self):
        return ApiRequestMethodEnum.POST.value

    @property
    def request_data(self):
        return []


class EsignTemplateDownloadEndpoint(EndpointBase):

    DOCUMENT_ID = ""

    @property
    def provider(self):
        return DIGIO

    @property
    def api_endpoint(self):
        return '/v2/client/document/download?document_id={}'.format(self.DOCUMENT_ID)

    @property
    def request_method(self):
        return ApiRequestMethodEnum.GET.value

    @property
    def request_data(self):
        return []
