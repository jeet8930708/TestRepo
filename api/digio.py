import logging

from core.decorators import timeit
from django.conf import settings
from django.utils import timezone

from .register import api_register
from .util import derive_class_instance
from .api_params import esign_request_params

logger = logging.getLogger(__name__)


@timeit
def ckyc_status_check(user, data):
    """
    Rest service to check CKYC status
    """
    instance = derive_class_instance(api_register.CKYC_STATUS_CHECK)
    response = instance.send_request(user, data)
    if response['status_code'] == 200:
        if response['data']['success']:
            return True, response['data']['search_response']
        else:
            return False, response['data']['error_message']
    return None, response['error']


@timeit
def kra_check_pan_status(user, data):
    """ KRA check PAN status
        If verified, retrieve CKYC details
    """
    instance = derive_class_instance(api_register.KRA_CHECK_PAN_STATUS)
    response = instance.send_request(user, {"pan_no": data["id_no"]})
    if response['status_code'] == 200:
        if response['data']['status'] == "KRA Verified":
            status, data = ckyc_status_check(user, data)
            if status:
                data['kra_status'] = response['data']['status']
                return status, data
            return status, data
        else:
            return False, response['data']['status']
    elif response['status_code'] >= 500:
        return None, response["status_message"]
    return None, response['error']


@timeit
def ckyc_data_download(user, data):
    """
    Rest service to download CKYC data
    """
    instance = derive_class_instance(api_register.CKYC_DATA_DOWNLOAD)
    response = instance.send_request(user, data)
    if response['status_code'] == 200:
        if response['data']['success']:
            return True, response['data']['download_response']
        else:
            return False, response['data']['error_message']
    return False, response['error']


@timeit
def bank_verify(user, data):
    """
    Rest service to verify PAN
    """
    instance = derive_class_instance(api_register.BANK_VERIFICATION)
    return instance.send_request(user, data)


@timeit
def kyc_request_create(user, data):
    """
    Rest service to create KYC request
    """
    instance = derive_class_instance(api_register.KYC_REQUEST_CREATE)
    data['customer_identifier'] = data.pop("username")
    data['template_name'] = settings.DIGIO_KYC_WORKFLOW
    data['notify_customer'] = False
    data['request_details'] = {}
    data['generate_access_token'] = True
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200 and resp["status_message"] == "Success":
        return True, resp['data']
    return False, resp.get('error', '')


@timeit
def kyc_request_detail(user, kid):
    """
    Rest service to get KYC request detail
    """
    instance = derive_class_instance(api_register.KYC_REQUEST_DETAIL)
    instance.REQUEST_ID = kid
    resp = instance.send_request(user, None)
    if resp['status_code'] == 200 and resp["status_message"] == "Success":
        return True, resp['data']
    return False, resp.get('error', '')


@timeit
def kyc_request_media(user, fid, doc_type=None):
    """
    Rest service to get KYC request detail
    """
    instance = derive_class_instance(api_register.KYC_REQUEST_MEDIA)
    instance.FILE_ID = fid
    instance.DOC_TYPE = doc_type
    resp = instance.send_request(user, None)
    if resp['status_code'] == 200 and resp["status_message"] == "Success":
        return True, resp['data']
    return False, resp.get('error', '')

@timeit
def esign_request(user):
    params = esign_request_params(user)
    instance = derive_class_instance(api_register.ESIGN_REQUEST)
    resp = instance.send_request(user, params)
    if resp['status_code'] == 200 and resp["status_message"] == "Success":
        return True, resp['data']
    return False, resp.get('error', '')

@timeit
def esign_download(user, doc_id):
    instance = derive_class_instance(api_register.ESIGN_DOWNLOAD)
    instance.DOCUMENT_ID = doc_id
    resp = instance.send_request(user)
    if resp['status_code'] == 200 and resp["status_message"] == "Success":
        return True, resp['data']
    return False, resp.get('error', '')
