import os
import shutil
import logging

from django.utils import timezone
from django.conf import settings
from reportlab.lib import colors
from reportlab.platypus import TableStyle, Image
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import A4
from reportlab.platypus import SimpleDocTemplate, Table

from core.decorators import timeit
from master.models import Country, State
from utils.image_util import convert_pdf_to_base64, convert_image_to_base64, convert_obj_url_to_base64

logger = logging.getLogger('api')


def get_state_code(state_name, cvlkra=False):
    try:
        state = State.objects.get(name__iexact=state_name, country__name="India")
        return state.code if not cvlkra else state.code_cvlkra
    except State.DoesNotExist:
        return "XX" if not cvlkra else '000'


def get_country_code(country_name):
    try:
        country = Country.objects.get(name__iexact=country_name)
        return country.code_text
    except State.DoesNotExist:
        return "101"


@timeit
def get_bsestar_ucc_registration_params(user):
    user_personal_det = user.personal_details
    user_professional_det = user.professional_details
    user_bank_details = user.user_bank_details
    user_address_details = user.address_details
    user_family_details = user.family_details
    user_kyc_details = user.user_kyc_details
    param = {
        "Client Code (UCC)": user.bsestar_client_code,
        "Primary Holder First Name": user_personal_det.get("first_name", ""),
        "Primary Holder Middle Name": user_personal_det.get("middle_name", ""),
        "Primary Holder Last Name": user_personal_det.get("last_name", ""),
        "Tax Status": "01",  # 01: Individual
        "Gender": user_personal_det.get("gender", "O"),  # M/F/O
        "Primary Holder DOB": user_personal_det.get("dob", "").replace("-", "/"),
        "Occupation Code": user_professional_det.get("occ_code", "08"),  # 08 - Others
        "Holding Nature": "SI",  # SI: Single, JO: Joint, AS:Anyone or Survivor
        "Second Holder First Name": "",
        "Second Holder Middle Name": "",
        "Second Holder Last Name": "",
        "Third Holder First Name": "",
        "Third Holder Middle Name": "",
        "Third Holder Last Name": "",
        "Second Holder DOB": "",
        "Third Holder DOB": "",
        "Guardian First Name": "",
        "Guardian Middle Name": "",
        "Guardian Last Name": "",
        "Guardian DOB": "",
        "Primary Holder PAN Exempt": "N",
        "Second Holder PAN Exempt": "",
        "Third Holder PAN Exempt": "",
        "Guardian PAN Exempt": "",
        "Primary Holder PAN": user.pan_no,
        "Second Holder PAN": "",
        "Third Holder PAN": "",
        "Guardian PAN": "",
        "Primary Holder Exempt Category": "",
        "Second Holder Exempt Category": "",
        "Third Holder Exempt Category": "",
        "Guardian Exempt Category": "",
        "Client Type": "P",  # P - Physical, D - Demat
        "PMS": "",
        "Default DP": "",
        "CDSL DPID": "",
        "CDSLCLTID": "",
        "CMBP Id": "",
        "NSDLDPID": "",
        "NSDLCLTID": "",
    }
    # there is placeholder for 5 bank details
    bank_count = 0
    for bank_det in user_bank_details:
        bank_count += 1
        param[f"Account Type {bank_count}"] = bank_det["account_type"]
        param[f"Account No {bank_count}"] = bank_det["account_number"]
        param[f"MICR No {bank_count}"] = ""
        param[f"IFSC Code {bank_count}"] = bank_det["ifsc_code"]
        param[f"Default Bank Flag {bank_count}"] = "Y" if (bank_det["is_primary_bank"] or len(user_bank_details) == 1) else "N"
        if bank_count == 5:
            break
    else:
        # fill blank values for remaining banks if user has less than 5 banks
        for i in range(bank_count+1, 6):
            param[f"Account Type {i}"] = ""
            param[f"Account No {i}"] = ""
            param[f"MICR No {i}"] = ""
            param[f"IFSC Code {i}"] = ""
            param[f"Default Bank Flag {i}"] = ""
    param["Cheque name"] = ""
    param["Div pay mode"] = "03"    # 01: Cheque, 02: Direct Credit, 03: ECS, 04: NEFT, 05: RTGS
    param["Address 1"] = user_address_details["address_1"][:40]
    param["Address 2"] = user_address_details["address_2"][:40]
    param["Address 3"] = user_address_details["address_3"][:40]
    param["City"] = user_address_details["address_city"]
    param["State"] = get_state_code(user_address_details["address_state"]) if user_address_details["address_state"] else ""
    param["Pincode"] = user_address_details["address_pin"]
    param["Country"] = user_address_details["address_country"]
    param["Resi. Phone"] = user_address_details["res_tele_number"]
    param["Resi. Fax"] = ""
    param["Office Phone"] = user_address_details["off_tele_number"]
    param["Office Fax"] = ""
    param["Email"] = user.email
    param["Communication Mode"] = "M"   # P: Physical, E: Email, M: Mobile
    param["Foreign Address 1"] = ""
    param["Foreign Address 2"] = ""
    param["Foreign Address 3"] = ""
    param["Foreign Address City"] = ""
    param["Foreign Address Pincode"] = ""
    param["Foreign Address State"] = ""
    param["Foreign Address Country"] = ""
    param["Foreign Address Resi Phone"] = ""
    param["Foreign Address Fax"] = ""
    param["Foreign Address Off Phone"] = ""
    param["Foreign Address Off Fax"] = ""
    param["Indian Mobile No"] = user.phone
    param["Nominee 1 Name"] = user_family_details["nominee_name"]
    param["Nominee 1 Relationship"] = user_family_details["nominee_relation"]
    param["Nominee 1 Applicable(%)"] = "100" if user_family_details["nominee_name"] else ""
    param["Nominee 1 Minor Flag"] = "N" if user_family_details["nominee_name"] else ""
    param["Nominee 1 DOB"] = ""
    param["Nominee 1 Guardian"] = ""
    for i in range(2, 4):
        param[f"Nominee {i} Name"] = ""
        param[f"Nominee {i} Relationship"] = ""
        param[f"Nominee {i} Applicable(%)"] = ""
        param[f"Nominee {i} Minor Flag"] = ""
        param[f"Nominee {i} DOB"] = ""
        param[f"Nominee {i} Guardian"] = ""

    param["Primary Holder KYC Type"] = "C"  # K: KRA Compliant, C: CKYC Compliant, B: BIOMETRIC KYC, E: Aadhaar Ekyc PAN
    param["Primary Holder CKYC Number"] = user_kyc_details["ckyc_number"]
    param["Second Holder KYC Type"] = ""
    param["Second Holder CKYC Number"] = ""
    param["Third Holder KYC Type"] = ""
    param["Third Holder CKYC Number"] = ""
    param["Guardian KYC Type"] = ""
    param["Guardian CKYC Number"] = ""
    param["Primary Holder KRA Exempt Ref. No."] = ""
    param["Second Holder KRA Exempt Ref. No."] = ""
    param["Third Holder KRA Exempt Ref. No."] = ""
    param["Guardian KRA Exempt Ref. No."] = ""
    param["Aadhaar Updated"] = "Y"
    param["Mapin Id."] = ""
    param["Paperless_flag"] = "Z"   # Investor onboarding P: Paper, Z: paperless
    param["LEI No"] = ""
    param["LEI Validity"] = ""
    param["Filler 1 ( Mobile Declaration Flag )"] = "SE"
    param["Filler 2 (Email Declaration Flag )"] = "SE"
    param["Filler 3"] = ""
    result = "|".join(param.values())
    return result


@timeit
def get_bsestar_fatca_upload_params(user):
    user_personal_det = user.personal_details
    user_professional_det = user.professional_details
    user_address_details = user.address_details

    pan_no = user.pan_no
    state_code = get_state_code(user_address_details["address_state"]) if user_address_details["address_state"] else ""
    country_code = get_country_code(user_address_details["address_country"]) if user_address_details["address_country"] else ""
    dob = user_personal_det["dob"].split('-')
    param = {
        "PAN_RP": user.pan_no,
        "PEKRN": "",
        "INV_NAME": user_personal_det["full_name"][:70],
        "DOB": f"{dob[1]}/{dob[0]}/{dob[2]}",
        "FR_NAME": "",
        "SP_NAME": "",
        "TAX_STATUS": "01",     # Individual
        "DATA_SRC": "E",        # E: Electronically, P: Physical
        "ADDR_TYPE": "2",       # 1 - Residential or Business; 2 - Residential; 3 - Business; 4 - Registered Office; 5 - Unspecified
        "PO_BIR_INC": state_code,
        "CO_BIR_INC": country_code,
        "TAX_RES1": country_code,
        "TPIN1": pan_no,
        "ID1_TYPE": "C",
    }
    for i in range(2, 5):
        param[f"TAX_RES{i}"] = ""
        param[f"TPIN{i}"] = ""
        param[f"ID{i}_TYPE"] = ""
    param["SRCE_WEALT"] = "08"  # Others
    param["CORP_SERVS"] = ""
    param["INC_SLAB"] = user_professional_det["inc_code"]
    param["NETWORTH"] = ""
    param["NW_DATE"] = ""
    param["PEP_FLAG"] = "Y" if user_professional_det["politically_exposed"] else "N"
    param["OCC_CODE"] = user_professional_det["occ_code"]
    param["OCC_TYPE"] = user_professional_det["occ_type"]
    for col in ["EXEMP_CODE", "FFI_DRNFE", "GIIN_NO", "SPR_ENTITY", "GIIN_NA", "GIIN_EXEMC", "NFFE_CATG", "ACT_NFE_SC",
                "NATURE_BUS", "REL_LISTED"]:
        param[col] = ""
    param["EXCH_NAME"] = "B"    # B - BSE; N - NSE; O - Others
    param["UBO_APPL"] = "N"
    for col in ["UBO_COUNT", "UBO_NAME", "UBO_PAN", "UBO_NATION", "UBO_ADD1", "UBO_ADD2", "UBO_ADD3", "UBO_CITY",
                "UBO_PIN", "UBO_STATE", "UBO_CNTRY", "UBO_ADD_TY", "UBO_CTR", "UBO_TIN", "UBO_ID_TY", "UBO_COB",
                "UBO_DOB", "UBO_GENDER", "UBO_FR_NAM", "UBO_OCC", "UBO_OCC_TY", "UBO_TEL", "UBO_MOBILE", "UBO_CODE",
                "UBO_HOL_PC", "SDF_FLAG"]:
        param[col] = ""
    param["UBO_DF"] = "N"
    param["AADHAAR_RP"] = ""
    param["NEW_CHANGE"] = "N" if not user.bsestar_fatca_uploaded else "C"
    param["LOG_NAME"] = timezone.now().strftime("%d%m%Y%H%M%S%f") + user.bsestar_client_code
    param["FILLER1"] = ""
    param["FILLER2"] = ""
    result = "|".join(param.values())
    return result


@timeit
def get_bsestar_mandate_params(user, mandate_detail):
    param = {
        "Client Code": user.bsestar_client_code,
        "Amount": str(mandate_detail.maximum_amount),
        "Mandate Type": "N",        # X: XSIP, I: ISIP, N: Net Banking
        "Account No": mandate_detail.customer_account_number,
        "Account Type": mandate_detail.customer_account_type,
        "IFSC Code": mandate_detail.bank_ifsc,
        "MICR Code": "",
        "Start Date": mandate_detail.first_collection_date.strftime("%d/%m/%Y"),
        "End Date": mandate_detail.first_collection_date.strftime(f"%d/%m/{mandate_detail.first_collection_date.year+100}"),
    }
    result = "|".join(param.values())
    return result


@timeit
def _get_aof_image_data_and_style(user):
    user_personal_det = user.personal_details
    user_professional_det = user.professional_details
    user_bank_details = user.user_bank_details
    user_address_details = user.address_details
    user_family_details = user.family_details
    user_kyc_details = user.user_kyc_details

    data = [[Image(str(settings.DATA_DIR / 'bsestar/logo.png'), height=1*inch, width=1*inch), '', '', ''],
            [f'Broker/Agent Code ARN: {settings.BSESTAR_ARN_CODE}', '', 'SUB-BROKER', 'EUIN'],
            ['Unit Folder Information', '', '', ''],
            [f'Name of the First Applicant:', user_personal_det["full_name"], '', ''],
            [f'PAN Number: {user.pan_no}', '', 'KYC', f'Date of Birth: {user_personal_det["dob"]}'],
            [f'Father Name: {user_personal_det["father_full_name"]}', '', f'Mother Name: {user_personal_det["mother_full_name"]}', ''],
            [f'Name if Guardian:', '', 'PAN:', ''],
            [f'Contact Address: {user_address_details["address_1"]} {user_address_details["address_2"]} {user_address_details["address_3"]}', '', '', ''],
            [f'City: {user_address_details["address_city"]}', f'Pincode: {user_address_details["address_pin"]}', f'State: {user_address_details["address_state"]}', f'Country: {user_address_details["address_country"]}'],
            [f'Tel. (Off): {user_address_details["off_tele_number"]}', f'Tel. (Res): {user_address_details["res_tele_number"]}', f'Email: {user.email}', ''],
            [f'Fax. (Res):', 'Fax (Res):', f'Mobile: {user_address_details["mobile_no"]}', ''],
            [f'Income Tax Slab/Networth: {user_professional_det["inc_name"]}', '', f'Occupation Details: {user_professional_det["occ_name"]}', ''],
            [f'Place of Birth:', '', 'Country of Tax Residence:', ''],
            [f'Tax Id No:', '', '', ''],
            [f'Politically exposed person/Related to Politically exposed person etc.? {"Yes" if user_professional_det["politically_exposed"] else "No"}', '', '', ''],
            [f'Mode of Holding: SINGLE', '', f'Occupation: {user_professional_det["occ_type_display"]}', ''],
            ['Name of the Second Applicant:', '', '', ''],
            ['PAN Number:', '', 'KYC', 'Date of Birth:'],
            ['Income Tax Slab/Networth:', '', 'Occupation Details:', ''],
            ['Place of Birth:', '', 'Country of Tax Residence:', ''],
            ['Tax Id No:', '', '', ''],
            ['Politically exposed person/Related to Politically exposed person etc.? Yes / No', '', '', ''],
            ['Name of the Third Applicant:', '', '', ''],
            ['PAN Number:', '', 'KYC', 'Date of Birth:'],
            ['Income Tax Slab/Networth:', '', 'Occupation Details:', ''],
            ['Place of Birth:', '', 'Country of Tax Residence:', ''],
            ['Tax Id No:', '', '', ''],
            ['Politically exposed person/Related to Politically exposed person etc.? Yes / No', '', '', ''],
            ['Other Details of Sole / 1st Applicant', '', '', ''],
            ['Overseas Address(In case of NRI Investor):', '', '', ''],
            ['City:', 'Pincode', '', 'Country'],
            ]
    for idx, bank_det in enumerate(user_bank_details):
        data.extend([
            [f'Bank Mandate {idx+1} Details', '', '', ''],
            [f'Name of Bank: {bank_det["bank_name"]}', '', f'Branch: {bank_det["bank_branch"]}', ''],
            [f'A/C No: {bank_det["account_number"]}', '', f'A/C Type: {bank_det["account_type_display"]}', f'IFSC Code: {bank_det["ifsc_code"]}'],
            [f'Bank Address: {bank_det["bank_address"]}', '', '', ''],
            [f'City: {bank_det["bank_city"]}', 'Pincode:', f'State: {bank_det["bank_state"]}', 'Country: India']
        ])
    bank_total = len(user_bank_details)
    for idx in range(5-bank_total):
        data.extend([
            [f'Bank Mandate {bank_total + idx + 1} Details', '', '', ''],
            ['Name of Bank:', '', f'Branch:', ''],
            ['A/C No:', '', 'A/C Type:', 'IFSC Code:'],
            ['Bank Address:', '', '', ''],
            ['City:', 'Pincode:', 'State:', 'Country:']
        ])

    data.extend([
        ['Nomination Details', '', '', ''],
        [f'Nominee Name: {user_family_details["nominee_name"]}', '', f'Relationship: {user_family_details["nominee_relation"]}'],
        ['Guardian Name(If Nominee is Minor):', '', '', ''],
        ['Nominee Address:', '', '', ''],
        ['City:', 'Pincode:', '', 'State:']
    ])

    data.extend([
        ['Declaration and Signature\nI/We confirm that details provided by me/us are true and correct. \n'
         'The ARN holder has disclosed to me/us all the commission (In the form of trail commission or any other mode),\n'
         'payable to him for the different competing Schemes of various Mutual Fund From amongst which the scheme\n'
         'is being recommended to me/us.', '', '', ''],
        ['Date:', '', 'Place:', ''],
        ['1st applicant Signature:', '', '2nd applicant Signature:', '3rd applicant Signature:'],
    ])

    style = [
        ('GRID', (0, 0), (-1, -1), 1, colors.black),
        ('SPAN', (0, 0), (-1, 0)), ('ALIGN', (0, 0), (-1, 0), 'CENTER'),
        ('SPAN', (0, 1), (1, 1)),
        ('SPAN', (0, 2), (-1, 2)), ('FONTNAME', (0, 2), (-1, 2), 'Helvetica-Bold'),

        # First Applicant
        ('SPAN', (1, 3), (-1, 3)), ('FONTNAME', (0, 3), (0, 3), 'Helvetica-Bold'),
        ('SPAN', (0, 4), (1, 4)),
        ('SPAN', (0, 5), (1, 5)), ('SPAN', (2, 5), (-1, 5)),
        ('SPAN', (0, 6), (1, 6)), ('SPAN', (2, 6), (-1, 6)),
        ('SPAN', (0, 7), (-1, 7)),
        ('SPAN', (2, 9), (-1, 9)),
        ('SPAN', (2, 10), (-1, 10)),
        ('SPAN', (0, 11), (1, 11)), ('SPAN', (2, 11), (-1, 11)),
        ('SPAN', (0, 12), (1, 12)), ('SPAN', (2, 12), (-1, 12)),
        ('SPAN', (0, 13), (-1, 13)),
        ('SPAN', (0, 14), (-1, 14)),
        ('SPAN', (0, 15), (1, 15)), ('SPAN', (2, 15), (-1, 15)),

        # Second Applicant
        ('SPAN', (1, 16), (-1, 16)), ('FONTNAME', (0, 16), (0, 16), 'Helvetica-Bold'),
        ('SPAN', (0, 17), (1, 17)),
        ('SPAN', (0, 18), (1, 18)), ('SPAN', (2, 18), (-1, 18)),
        ('SPAN', (0, 19), (1, 19)), ('SPAN', (2, 19), (-1, 19)),
        ('SPAN', (0, 20), (-1, 20)),
        ('SPAN', (0, 21), (-1, 21)),

        # Third Applicant
        ('SPAN', (1, 22), (-1, 22)), ('FONTNAME', (0, 22), (0, 22), 'Helvetica-Bold'),
        ('SPAN', (0, 23), (1, 23)),
        ('SPAN', (0, 24), (1, 24)), ('SPAN', (2, 24), (-1, 24)),
        ('SPAN', (0, 25), (1, 25)), ('SPAN', (2, 25), (-1, 25)),
        ('SPAN', (0, 26), (-1, 26)),
        ('SPAN', (0, 27), (-1, 27)),

        # other details
        ('SPAN', (0, 28), (-1, 28)),
        ('SPAN', (0, 29), (-1, 29)),
        ('SPAN', (1, 30), (2, 30)),
    ]

    # bank details
    for i in range(5):
        style.extend([
            ('SPAN', (0, 31+i*5), (-1, 31+i*5)), ('FONTNAME', (0, 31+i*5), (-1, 31+i*5), 'Helvetica-Bold'),
            ('SPAN', (0, 32+i*5), (1, 32+i*5)), ('SPAN', (2, 32+i*5), (-1, 32+i*5)),
            ('SPAN', (2, 33+i*5), (-1, 33+i*5)),
            ('SPAN', (0, 34+i*5), (-1, 34+i*5)), ('FONTSIZE', (0, 34+i*5), (-1, 34+i*5), 9)
        ])

    # nominee details
    style.extend([
        ('SPAN', (0, 56), (-1, 56)), ('FONTNAME', (0, 56), (-1, 56), 'Helvetica-Bold'),
        ('SPAN', (0, 57), (1, 57)), ('SPAN', (2, 57), (-1, 57)),
        ('SPAN', (0, 58), (-1, 58)),
        ('SPAN', (0, 59), (-1, 59)),
        ('SPAN', (1, 60), (2, 60)),
    ])

    # declaration
    style.extend([
        ('SPAN', (0, 61), (-1, 61)),
        ('SPAN', (0, 62), (1, 62)), ('SPAN', (2, 62), (3, 62)),
        ('SPAN', (0, 63), (1, 63)),
    ])

    return data, TableStyle(style)


@timeit
def get_bsestar_aof_image_base64(user):
    filename = f"AOF_{settings.BSESTAR_MEMBER_CODE}_{user.bsestar_client_code}"
    pdf_path = str(settings.DATA_DIR / f"bsestar/aof_upload/{filename}/{filename}.pdf")
    tiff_path = str(settings.DATA_DIR / f"bsestar/aof_upload/{filename}/{filename}.tiff")
    save_dir = os.path.dirname(pdf_path)
    if os.path.exists(save_dir):
        shutil.rmtree(save_dir)
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)
    doc = SimpleDocTemplate(pdf_path,
                            pagesize=A4,
                            leftMargin=0.1,
                            rightMargin=0.1,
                            topMargin=0.1,
                            bottomMargin=0.1)
    # container for the 'Flowable' objects
    elements = []

    data, style = _get_aof_image_data_and_style(user)

    t = Table(data)
    t.setStyle(style)
    elements.append(t)
    # write the document to disk
    doc.build(elements)

    bytes_base64 = convert_pdf_to_base64(pdf_path, tiff_path)
    return bytes_base64, f"{filename}.tiff"


@timeit
def get_cvlkra_input_params(user):
    kyc_det = user.get_kyc_detail()
    family_det = user.family_details
    return f"""
    <![CDATA[ 
            <ROOT>
            <HEADER>
                <COMPANY_CODE>{settings.CVLKRA_POSCODE}</COMPANY_CODE>
                <BATCH_DATE>{timezone.localdate().strftime('%d/%m/%Y')}</BATCH_DATE>
            </HEADER>
            <KYCDATA>
                <APP_UPDTFLG>01</APP_UPDTFLG>
                <APP_POS_CODE>{settings.CVLKRA_POSCODE}</APP_POS_CODE>
                <APP_TYPE>I</APP_TYPE>
                <APP_NO/>
                <APP_DATE>{timezone.localdate().strftime('%d/%m/%Y')}</APP_DATE>
                <APP_PAN_NO>{kyc_det.pan_number}</APP_PAN_NO>
                <APP_PAN_COPY>N</APP_PAN_COPY>
                <APP_EXMT>N</APP_EXMT>
                <APP_EXMT_CAT/>
                <APP_EXMT_ID_PROOF>01</APP_EXMT_ID_PROOF>
                <APP_IPV_FLAG>N</APP_IPV_FLAG>
                <APP_IPV_DATE/>
                <APP_GEN>{kyc_det.gender}</APP_GEN>
                <APP_NAME>{kyc_det.pan_name}</APP_NAME>
                <APP_F_NAME>{family_det.father_name}</APP_F_NAME>
                <APP_REGNO/>
                <APP_DOB_INCORP>{kyc_det.dob.strftime('%d/%m/%Y')}</APP_DOB_INCORP>
                <APP_COMMENCE_DT/>
                <APP_NATIONALITY>01</APP_NATIONALITY>
                <APP_OTH_NATIONALITY/>
                <APP_COMP_STATUS/>
                <APP_OTH_COMP_STATUS/>
                <APP_RES_STATUS>R</APP_RES_STATUS>
                <APP_RES_STATUS_PROOF/>
                <APP_UID_NO>Y</APP_UID_NO>
                <APP_COR_ADD1>{kyc_det.current_address}</APP_COR_ADD1>
                <APP_COR_ADD2/>
                <APP_COR_ADD3/>
                <APP_COR_CITY>{kyc_det.current_district_or_city}</APP_COR_CITY>
                <APP_COR_PINCD>{kyc_det.current_pincode}</APP_COR_PINCD>
                <APP_COR_STATE>{get_state_code(kyc_det.current_state, cvlkra=True)}</APP_COR_STATE>
                <APP_COR_CTRY>101</APP_COR_CTRY>
                <APP_OFF_ISD/>
                <APP_OFF_STD/>
                <APP_OFF_NO/>
                <APP_RES_ISD/>
                <APP_RES_STD/>
                <APP_RES_NO/>
                <APP_MOB_ISD/>
                <APP_MOB_NO/>
                <APP_FAX_ISD/>
                <APP_FAX_STD/>
                <APP_FAX_NO/>
                <APP_EMAIL/>
                <APP_COR_ADD_PROOF>31</APP_COR_ADD_PROOF>
                <APP_COR_ADD_REF/>
                <APP_COR_ADD_DT/>
                <APP_PER_ADD_FLAG>N</APP_PER_ADD_FLAG>
                <APP_PER_ADD1>{kyc_det.permanent_address}</APP_PER_ADD1>
                <APP_PER_ADD2/>
                <APP_PER_ADD3/>
                <APP_PER_CITY>{kyc_det.permanent_district_or_city}</APP_PER_CITY>
                <APP_PER_PINCD>{kyc_det.permanent_pincode}</APP_PER_PINCD>
                <APP_PER_STATE>{get_state_code(kyc_det.permanent_state, cvlkra=True)}</APP_PER_STATE>
                <APP_PER_CTRY>101</APP_PER_CTRY>
                <APP_PER_ADD_PROOF>31</APP_PER_ADD_PROOF>
                <APP_PER_ADD_REF/>
                <APP_PER_ADD_DT/>
                <APP_INCOME/>
                <APP_OCC/>
                <APP_OTH_OCC/>
                <APP_POL_CONN/>
                <APP_DOC_PROOF>E</APP_DOC_PROOF>
                <APP_INTERNAL_REF/>
                <APP_BRANCH_CODE/>
                <APP_MAR_STATUS>{'01' if family_det.marital_status==1 else '02'}</APP_MAR_STATUS>
                <APP_NETWRTH/>
                <APP_NETWORTH_DT/>
                <APP_INCORP_PLC/>
                <APP_OTHERINFO/>
                <APP_ACC_OPENDT/>
                <APP_ACC_ACTIVEDT/>
                <APP_ACC_UPDTDT/>
                <APP_FILLER1/>
                <APP_FILLER2/>
                <APP_FILLER3/>
                <APP_STATUS/>
                <APP_STATUSDT/>
                <APP_ERROR_DESC/>
                <APP_DUMP_TYPE>S</APP_DUMP_TYPE>
                <APP_DNLDDT/>
                <APP_REMARKS/>
                <APP_KYC_MODE>5</APP_KYC_MODE>
                <APP_UID_TOKEN/>
                <APP_VER_NO>V30</APP_VER_NO>
                <APP_KRA_INFO>CVLKRA</APP_KRA_INFO>
                <APP_SIGNATURE>{convert_image_to_base64(kyc_det.signature.url) if kyc_det.signature else ''}</APP_SIGNATURE>
                <APP_IOP_FLG>II</APP_IOP_FLG>
            </KYCDATA>
            <FOOTER>
                <NO_OF_KYC_RECORDS>1</NO_OF_KYC_RECORDS>
                <NO_OF_ADDLDATA_RECORDS>0</NO_OF_ADDLDATA_RECORDS>
            </FOOTER>
        </ROOT>
        ]]>
    """


@timeit
def esign_request_params(user):
    user_personal_det = user.personal_details
    user_family_details = user.family_details
    user_kyc_details = user.kyc_detail
    user_citizen_det = user.citizenship_detail
    
    logger.info(user_kyc_details)

    data = {
        "signers": [{
            "identifier": user.phone,
            "name": user_kyc_details.name,
            "sign_type": "aadhaar"
        }],
        "expire_in_days": 10,
        "send_sign_link": False,
        "notify_signers": False,
        "display_on_page": "custom",
        "sign_coordinates": {
            user.phone: {
                "1": [
                    {
                        "llx": 391.00028174257426,
                        "lly": 37.00669965010503,
                        "urx": 529.9942396633663,
                        "ury": 97.0004960111967
                    },
                    {
                        "llx": 450.00003029702975,
                        "lly": 416.0083438488453,
                        "urx": 552.9929690346535,
                        "ury": 462.00021228831343
                    }
                ],
                "2": [
                    {
                        "llx": 291.0000785346535,
                        "lly": 234.0061847445766,
                        "urx": 397.99927036633665,
                        "ury": 372.0002015395381
                    }
                ],
                "5": [
                    {
                        "llx": 414.0002636435644,
                        "lly": 42.00725668299513,
                        "urx": 546.9986565891089,
                        "ury": 76.99826787963613
                    }
                ],
                "6": [
                    {
                        "llx": 414.0002636435644,
                        "lly": 42.00725668299513,
                        "urx": 546.9986565891089,
                        "ury": 76.99826787963613
                    }
                ],
                "7": [
                    {
                        "llx": 414.0002636435644,
                        "lly": 44.002692512246306,
                        "urx": 547.993312529703,
                        "ury": 79.99712918124561
                    }
                ]
            } 
        },
        "templates": [{
            "template_key": settings.DIGIO_ESIGN_TEMPLATE_KEY,
            "template_values": {
                "application_type": "new_kyc",
                "kyc_mode": "digilocker",
                "pan_number": user_kyc_details.pan_number,
                "user_salutation": "Mr" if user_kyc_details.gender == "M" else "Mrs",
                "user_name": user_kyc_details.name,
                "maiden_salutation": "",
                "maiden_first_name": "",
                "maiden_middle_name": "",
                "maiden_last_name": "",
                "father_salutation": "Mr",
                "father_name": user_family_details['father_name'],
                "date_of_birth": user_kyc_details.dob.strftime("%Y-%m-%d"),
                "gender": user_kyc_details.gender,
                "marital_status": "married" if user_family_details['marital_status'] == 1 else "single",
                "nationality": "indian" if user_citizen_det.country_of_birth.name=="India" else "other",
                "nationality_other_input": user_citizen_det.country_of_birth.name if user_citizen_det.country_of_birth.name!="India" else "",
                "residential_status": "resident_individual" if user_citizen_det.resident_status==1 else "non_resident_indian",
                "proof_of_identity": "aadhaar_card",
                "proof_of_identity_aadhaar_last_digits": user_kyc_details.aadhaar_number[-4:],
                "correspondence_address": user_kyc_details.current_address,
                "correspondence_city": user_kyc_details.current_district_or_city,
                "correspondence_district": user_kyc_details.current_district_or_city,
                "correspondence_pincode": user_kyc_details.current_pincode,
                "correspondence_state": user_kyc_details.current_state,
                "correspondence_country": "India",
                "correspondence_address_type": "residential",
                "permanent_address": user_kyc_details.permanent_address,
                "permanent_city": user_kyc_details.permanent_district_or_city,
                "permanent_district": user_kyc_details.permanent_district_or_city,
                "permanent_pincode": user_kyc_details.permanent_pincode,
                "permanent_state": user_kyc_details.permanent_state,
                "permanent_country": "India",
                "permanent_address_type": "residential",
                "proof_of_address": "aadhaar_card",
                "proof_of_address_aadhaar_last_digits": user_kyc_details.aadhaar_number[-4:],
                "contact_details_email": user.email.upper(),
                "contact_details_country_code": "91",
                "contact_details_mobile_number": user.phone,
                "contact_details_office_telephone_code": "",
                "contact_details_office_telephone_number": "",
                "contact_details_residential_telephone_code": "",
                "contact_details_residential_telephone_number": "",
                "applicant_declaration_date": timezone.localdate().strftime("%Y-%m-%d"),
                "applicant_declaration_place": user_kyc_details.current_district_or_city
            },
            "images": {
                "applicant_photo": {
                    "content": convert_obj_url_to_base64(user_kyc_details.photo.url) if user_kyc_details.photo else "",
                    "type": "JPEG"
                },
                "applicant_wet_signature": {
                    "content": convert_obj_url_to_base64(user_kyc_details.signature.url) if user_kyc_details.signature else "",
                    "type": "JPEG"
                },
                "digilocker_wet_signature": {
                    "content": convert_obj_url_to_base64(user_kyc_details.signature.url) if user_kyc_details.signature else "",
                    "type": "JPEG"
                },
                "digilocker_pan_card": {
                    "content": convert_obj_url_to_base64(user_kyc_details.pan_image.url) if user_kyc_details.pan_image else "",
                    "type": "JPEG"
                },
                "digilocker_aadhaar_card": {
                    "content": convert_obj_url_to_base64(user_kyc_details.aadhaar_image.url) if user_kyc_details.aadhaar_image else "",
                    "type": "JPEG"

                }
            },
        }]
    }
    return data
