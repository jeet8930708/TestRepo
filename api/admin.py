from django.contrib import admin

from .models import (
    ApiLog,
)


# Register your models here.
class ApiLogAdmin(admin.ModelAdmin):
    list_display = ['api', 'user', 'method', 'status_code', 'request_datetime', 'execution_time']
    search_fields = ['api']
    list_filter = ['status_code', 'method']
admin.site.register(ApiLog, ApiLogAdmin)
