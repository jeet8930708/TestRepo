import time
import logging

from django.core.cache import cache
from django.conf import settings
from django.utils import timezone
from core.decorators import timeit
from order.enums import BuySellEnum, PaymentModeEnum
from .register import api_register
from .util import derive_class_instance
from .api_params import (
    get_bsestar_ucc_registration_params,
    get_bsestar_fatca_upload_params,
    get_bsestar_aof_image_base64,
    get_bsestar_mandate_params,
)

logger = logging.getLogger('api')


@timeit
def ucc_registration(user):
    instance = derive_class_instance(api_register.UCC_REGISTRATION)
    data = {
        "UserId": settings.BSESTAR_USER_ID,
        "MemberCode": settings.BSESTAR_MEMBER_CODE,
        "Password": settings.BSESTAR_PASSWORD,
        "RegnType": "NEW" if not user.bsestar_ucc_registered else "MOD",
        "Param": get_bsestar_ucc_registration_params(user),
    }
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        if resp['data']['Status'] == '1':
            if resp['data']['Remarks'] == "FAILED : CLIENT MODIFICATION NOT ALLOWED IF REGN TYPE IS NEW":
                logger.info(f"User already registered. BSEStar response: {resp['data']['Remarks']}")
                user.update_ucc_registration_status()
                return True, "User already registered"
            return False, resp['data']['Remarks']
        if not user.bsestar_ucc_registered:
            logger.info("Updating UCC registration status of user...")
            user.update_ucc_registration_status()
        return True, resp['status_message']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def _get_password_mfapi(user):
    instance = derive_class_instance(api_register.GET_PASSWORD_MFAPI)
    data = f"""  <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="{settings.BSESTAR_API_ACTION_BASE_URL}/2016/01/">
                            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                                <wsa:Action>{settings.BSESTAR_API_ACTION_BASE_URL}/2016/01/{'IStarMFWebService' if settings.ENV=='PRD' else 'IMFUploadService'}/getPassword</wsa:Action>
                                <wsa:To>{instance.request_url}</wsa:To>
                            </soap:Header>
                            <soap:Body>
                                <ns:getPassword>
                                    <!--Optional:-->
                                     <ns:UserId>{settings.BSESTAR_USER_ID}</ns:UserId>
                                     <!--Optional:-->
                                     <ns:MemberId>{settings.BSESTAR_MEMBER_CODE}</ns:MemberId>
                                     <!--Optional:-->
                                     <ns:Password>{settings.BSESTAR_PASSWORD}</ns:Password>
                                     <!--Optional:-->
                                     <ns:PassKey>{settings.BSESTAR_PASSKEY}</ns:PassKey>
                                </ns:getPassword>
                            </soap:Body>
                        </soap:Envelope>
                    """
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        return True, resp['status_message']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def fatca_upload(user):
    success, password = _get_password_mfapi(user)
    if not success:
        logger.error(password)
        return False, password
    instance = derive_class_instance(api_register.FATCA_UPLOAD)
    param = get_bsestar_fatca_upload_params(user)
    data = f"""  <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:ns="{settings.BSESTAR_API_ACTION_BASE_URL}/2016/01/">
                            <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                                <wsa:Action>{settings.BSESTAR_API_ACTION_BASE_URL}/2016/01/{'IStarMFWebService' if settings.ENV=='PRD' else 'IMFUploadService'}/MFAPI</wsa:Action>
                                <wsa:To>{instance.request_url}</wsa:To>
                            </soap:Header>
                            <soap:Body>
                                <ns:MFAPI>
                                 <!--Optional:-->
                                 <ns:Flag>01</ns:Flag>
                                 <!--Optional:-->
                                 <ns:UserId>{settings.BSESTAR_USER_ID}</ns:UserId>
                                 <!--Optional:-->
                                 <ns:EncryptedPassword>{password}</ns:EncryptedPassword>
                                 <!--Optional:-->
                                 <ns:param>{param}</ns:param>
                              </ns:MFAPI>
                           </soap:Body>
                        </soap:Envelope>
                """
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        if not user.bsestar_fatca_uploaded:
            user.update_fatca_upload_status()
        return True, resp['status_message']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def mandate_register(user, mandate_detail):
    if settings.ENV == "DEV":
        return True, timezone.now().strftime("%d%m%Y%H%M%S")[:15]
    success, password = _get_password_mfapi(user)
    if not success:
        logger.error(password)
        return False, password
    instance = derive_class_instance(api_register.MANDATE_REGISTER_BSESTAR)
    param = get_bsestar_mandate_params(user, mandate_detail)
    data = f""" <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
                               xmlns:ns="{settings.BSESTAR_API_ACTION_BASE_URL}/2016/01/">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>{settings.BSESTAR_API_ACTION_BASE_URL}/2016/01/{'IStarMFWebService' if settings.ENV=='PRD' else 'IMFUploadService'}/MFAPI</wsa:Action>
                        <wsa:To>{instance.request_url}</wsa:To>
                    </soap:Header>
                    <soap:Body>
                        <ns:MFAPI>
                            <!--Optional:-->
                            <ns:Flag>06</ns:Flag>
                            <!--Optional:-->
                            <ns:UserId>{settings.BSESTAR_USER_ID}</ns:UserId>
                            <!--Optional:-->
                            <ns:EncryptedPassword>{password}</ns:EncryptedPassword>
                            <!--Optional:-->
                            <ns:param>{param}</ns:param>
                        </ns:MFAPI>
                    </soap:Body>
                </soap:Envelope>
            """
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        return True, resp['data']['mandate_id']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def _get_password_file_upload(user):
    instance = derive_class_instance(api_register.GET_PASSWORD_FILE_UPLOAD)
    data = f""" <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
                               xmlns:tem="http://tempuri.org/" 
                               xmlns:star="http://schemas.datacontract.org/2004/07/StarMFFileUploadService">
                <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                    <wsa:Action>http://tempuri.org/IStarMFFileUploadService/GetPassword</wsa:Action>
                    <wsa:To>{instance.request_url}</wsa:To>
                </soap:Header>
                <soap:Body>
                    <tem:GetPassword>
                        <!--Optional:-->
                        <tem:Param>
                            <!--Optional:-->
                            <star:MemberId>{settings.BSESTAR_MEMBER_CODE}</star:MemberId>
                            <!--Optional:-->
                            <star:Password>{settings.BSESTAR_PASSWORD}</star:Password>
                            <!--Optional:-->
                            <star:UserId>{settings.BSESTAR_USER_ID}</star:UserId>
                        </tem:Param>
                    </tem:GetPassword>
                </soap:Body>
            </soap:Envelope>
    """
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        return True, resp['status_message']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def aof_image_upload(user):
    success, password = _get_password_file_upload(user)
    if not success:
        logger.error(password)
        return False, password
    instance = derive_class_instance(api_register.AOF_IMAGE_UPLOAD)
    aof_image_base64, filename = get_bsestar_aof_image_base64(user)
    data = f"""<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
                              xmlns:tem="http://tempuri.org/" 
                              xmlns:star="http://schemas.datacontract.org/2004/07/StarMFFileUploadService">
                <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                    <wsa:Action>http://tempuri.org/IStarMFFileUploadService/UploadFile</wsa:Action>
                    <wsa:To>{instance.request_url}</wsa:To>
                </soap:Header>
                <soap:Body>
                    <tem:UploadFile>
                        <!--Optional:-->
                        <tem:data>
                            <!--Optional:-->
                            <star:ClientCode>{user.bsestar_client_code}</star:ClientCode>
                            <!--Optional:-->
                            <star:DocumentType>Nrm</star:DocumentType>
                            <!--Optional:-->
                            <star:EncryptedPassword>{password}</star:EncryptedPassword>
                            <!--Optional:-->
                            <star:FileName>{filename}</star:FileName>
                            <!--Optional:-->
                            <star:Filler1></star:Filler1>
                            <!--Optional:-->
                            <star:Filler2></star:Filler2>
                            <!--Optional:-->
                            <star:Flag>UCC</star:Flag>
                            <!--Optional:-->
                            <star:MemberCode>{settings.BSESTAR_MEMBER_CODE}</star:MemberCode>
                            <!--Optional:-->
                            <star:UserId>{settings.BSESTAR_USER_ID}</star:UserId>
                            <!--Optional:-->
                            <star:pFileBytes>{aof_image_base64}</star:pFileBytes>
                        </tem:data>
                    </tem:UploadFile>
                </soap:Body>
            </soap:Envelope>
    """
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        if not user.bsestar_aof_image_uploaded:
            user.update_aof_image_upload_status()
        return True, resp['status_message']
    elif resp['status_message'] in ("FAILED: IMAGE IS ALREADY AVAILABLE AND IMAGE STATUS IS PENDING",
                                  "FAILED: PAN NO ALREADY APPROVED"):
        logger.info(f"AOF image already uploaded, BSEStar response: {resp['status_message']}")
        user.update_aof_image_upload_status()
        return True, resp['status_message'][8:]
    logger.error(resp)
    return False, resp['status_message']


def bsestar_registration(user):
    if settings.ENV == "DEV":
        return True, "User registered on bsestar"

    if not user.bsestar_ucc_registered:
        status, message = ucc_registration(user)
        if not status:
            return status, message

    if not user.bsestar_fatca_uploaded:
        status, message = fatca_upload(user)
        if not status:
            return status, message

    if not user.bsestar_aof_image_uploaded:
        status, message = aof_image_upload(user)
        if not status:
            return status, message

    return True, "User registered on bsestar"


@timeit
def _get_password_mforder(user):
    cache_key = f"password-mforder-{user.username}"
    if cache.get(cache_key):
        logger.info("fetching from cache...")
        return True, cache.get(cache_key)
    instance = derive_class_instance(api_register.GET_PASSWORD_MFORDER)
    data = f""" <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://bsestarmf.in/MFOrderEntry/getPassword</wsa:Action>
                        <wsa:To>{instance.request_url}</wsa:To>
                    </soap:Header>
                    <soap:Body>
                        <bses:getPassword>
                            <!--Optional:-->
                            <bses:UserId>{settings.BSESTAR_USER_ID}</bses:UserId>
                            <!--Optional:-->
                            <bses:Password>{settings.BSESTAR_PASSWORD}</bses:Password>
                            <!--Optional:-->
                            <bses:PassKey>{settings.BSESTAR_PASSKEY}</bses:PassKey>
                      </bses:getPassword>
                    </soap:Body>
                </soap:Envelope>
    """
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        cache.set(cache_key, resp['status_message'])
        return True, resp['status_message']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def xsip_order_entry(user, sip, cancel=False):
    success, password = _get_password_mforder(user)
    if not success:
        logger.error(password)
        return False, password
    instance = derive_class_instance(api_register.XSIP_ORDER_ENTRY)
    data = f""" <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://bsestarmf.in/MFOrderEntry/xsipOrderEntryParam</wsa:Action>
                        <wsa:To>{instance.request_url}</wsa:To>
                    </soap:Header>
                    <soap:Body>
                        <bses:xsipOrderEntryParam>
                            <!--Optional:-->
                            <bses:TransactionCode>{'CXL' if cancel else 'NEW'}</bses:TransactionCode>
                            <!--Optional:-->
                            <bses:UniqueRefNo>{sip.bsestar_unique_ref_no_cancel if cancel else sip.bsestar_unique_ref_no}</bses:UniqueRefNo>
                            <!--Optional:-->
                            <bses:SchemeCode>{sip.instrument.bsestar_scheme_code}</bses:SchemeCode>
                            <!--Optional:-->
                            <bses:MemberCode>{settings.BSESTAR_MEMBER_CODE}</bses:MemberCode>
                            <!--Optional:-->
                            <bses:ClientCode>{user.bsestar_client_code}</bses:ClientCode>
                            <!--Optional:-->
                            <bses:UserId>{settings.BSESTAR_USER_ID}</bses:UserId>
                            <!--Optional:-->
                            <bses:InternalRefNo/>
                            <!--Optional:-->
                            <bses:TransMode>P</bses:TransMode>
                            <!--Optional:-->
                            <bses:DpTxnMode>P</bses:DpTxnMode>
                            <!--Optional:-->
                            <bses:StartDate>{sip.first_sip_date.strftime('%d/%m/%Y')}</bses:StartDate>
                            <!--Optional:-->
                            <bses:FrequencyType>{sip.get_inv_interval_display().upper()}</bses:FrequencyType>
                            <!--Optional:-->
                            <bses:FrequencyAllowed>1</bses:FrequencyAllowed>
                            <!--Optional:-->
                            <bses:InstallmentAmount>{sip.sip_amount}</bses:InstallmentAmount>
                            <!--Optional:-->
                            <bses:NoOfInstallment>{sip.installments_total}</bses:NoOfInstallment>
                            <!--Optional:-->
                            <bses:Remarks/>
                            <!--Optional:-->
                            <bses:FolioNo>{user.bsestar_folio_no if user.bsestar_folio_no else ''}</bses:FolioNo>
                            <!--Optional:-->
                            <bses:FirstOrderFlag>Y</bses:FirstOrderFlag>
                            <!--Optional:-->
                            <bses:Brokerage/>
                            <!--Optional:-->
                            <bses:MandateID>{sip.sip_mandate.mandate.mandate_id_bsestar}</bses:MandateID>
                            <!--Optional:-->
                            <bses:SubberCode/>
                            <!--Optional:-->
                            <bses:Euin/>
                            <!--Optional:-->
                            <bses:EuinVal>N</bses:EuinVal>
                            <!--Optional:-->
                            <bses:DPC/>
                            <!--Optional:-->
                            <bses:XsipRegID>{sip.bsestar_xsip_reg_id if cancel else ""}</bses:XsipRegID>
                            <!--Optional:-->
                            <bses:IPAdd/>
                            <!--Optional:-->
                            <bses:Password>{password}</bses:Password>
                            <!--Optional:-->
                            <bses:PassKey>{settings.BSESTAR_PASSKEY}</bses:PassKey>
                            <!--Optional:-->
                            <bses:Param1/>
                            <!--Optional:-->
                            <bses:Param2/>
                            <!--Optional:-->
                            <bses:Param3/>
                            <!--Optional:-->
                            <bses:Filler1/>
                            <!--Optional:-->
                            <bses:Filler2/>
                            <!--Optional:-->
                            <bses:Filler3/>
                            <!--Optional:-->
                            <bses:Filler4/>
                            <!--Optional:-->
                            <bses:Filler5/>
                            <!--Optional:-->
                            <bses:Filler6/>
                        </bses:xsipOrderEntryParam>
                    </soap:Body>
                </soap:Envelope>
    """
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        return True, resp['status_message']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def order_buy_sell(user, order):
    if settings.ENV == "DEV":
        time.sleep(1)
        return True, str(timezone.now().timestamp()).replace('.', '')[:10]
    success, password = _get_password_mforder(user)
    if not success:
        logger.error(password)
        return False, password
    instance = derive_class_instance(api_register.ORDER_ENTRY)
    is_sell_order = order.buy_sell == BuySellEnum.Sell.value
    data = f"""
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:bses="http://bsestarmf.in/">
                <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                    <wsa:Action>http://bsestarmf.in/MFOrderEntry/orderEntryParam</wsa:Action>
                    <wsa:To>{instance.request_url}</wsa:To>
                </soap:Header>
                <soap:Body>
                    <bses:orderEntryParam>
                        <bses:TransCode>NEW</bses:TransCode>
                        <bses:TransNo>{order.bsestar_unique_ref_no}</bses:TransNo>
                        <bses:OrderId/>
                        <bses:UserID>{settings.BSESTAR_USER_ID}</bses:UserID>
                        <bses:MemberId>{settings.BSESTAR_MEMBER_CODE}</bses:MemberId>
                        <bses:ClientCode>{user.bsestar_client_code}</bses:ClientCode>
                        <bses:SchemeCd>{order.instrument.bsestar_scheme_code}</bses:SchemeCd>
                        <bses:BuySell>{'R' if is_sell_order else 'P'}</bses:BuySell>
                        <bses:BuySellType>FRESH</bses:BuySellType>
                        <bses:DPTxn>P</bses:DPTxn>
                        <bses:OrderVal>{'' if order.redeem_all else int(order.amount)}</bses:OrderVal>
                        <bses:Qty/>
                        <bses:AllRedeem>{'Y' if order.redeem_all else 'N'}</bses:AllRedeem>
                        <bses:FolioNo>{user.bsestar_folio_no if user.bsestar_folio_no else ''}</bses:FolioNo>
                        <bses:Remarks/>
                        <bses:KYCStatus>Y</bses:KYCStatus>
                        <bses:RefNo/>
                        <bses:SubBrCode/>
                        <bses:EUIN/>
                        <bses:EUINVal>N</bses:EUINVal>
                        <bses:MinRedeem>N</bses:MinRedeem>
                        <bses:DPC>Y</bses:DPC>
                        <bses:IPAdd/>
                        <bses:Password>{password}</bses:Password>
                        <bses:PassKey>{settings.BSESTAR_PASSKEY}</bses:PassKey>
                        <bses:Param1/>
                        <bses:Param2/>
                        <bses:Param3>{order.bank_account_number if is_sell_order else ''}</bses:Param3>
                        <bses:MobileNo>{user.phone if is_sell_order else ""}</bses:MobileNo>
                        <bses:EmailID>{user.email if is_sell_order else ""}</bses:EmailID>
                        <bses:MandateID/>
                        <bses:Filler1/>
                        <bses:Filler2/>
                        <bses:Filler3/>
                        <bses:Filler4/>
                        <bses:Filler5/>
                        <bses:Filler6/>
                    </bses:orderEntryParam>
                </soap:Body>
            </soap:Envelope>
    """
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        return True, resp['status_message']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def order_log(user=None, from_date=None, to_date=None, order_no=None):
    """ Fetch order log
    Params:
    user: User instance
    from_date = date instance
    to_date = date instance
    """
    from_date = from_date.strftime("%d/%m/%Y") if from_date else timezone.now().strftime("%d/%m/%Y")
    to_date = to_date.strftime("%d/%m/%Y") if to_date else timezone.now().strftime("%d/%m/%Y")
    client_code = user.bsestar_client_code if user else ""
    order_no = order_no if order_no else ""
    instance = derive_class_instance(api_register.ORDER_STATUS)
    data = f""" <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
                               xmlns:ns="http://www.bsestarmf.in/2016/01/" 
                               xmlns:star="http://schemas.datacontract.org/2004/07/StarMFWebService">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/OrderStatus</wsa:Action>
                        <wsa:To>{instance.request_url}</wsa:To>
                    </soap:Header>
                    <soap:Body>
                        <ns:OrderStatus>
                            <!--Optional:-->
                            <ns:Param>
                                <!--Optional:-->
                                <star:ClientCode>{client_code}</star:ClientCode>
                                <!--Optional:-->
                                <star:Filler1/>
                                <!--Optional:-->
                                <star:Filler2/>
                                <!--Optional:-->
                                <star:Filler3/>
                                <!--Optional:-->
                                <star:FromDate>{from_date}</star:FromDate>
                                <!--Optional:-->
                                <star:MemberCode>{settings.BSESTAR_MEMBER_CODE}</star:MemberCode>
                                <!--Optional:-->
                                <star:OrderNo>{order_no}</star:OrderNo>
                                <!--Optional:-->
                                <star:OrderStatus>All</star:OrderStatus>
                                <!--Optional:-->
                                <star:OrderType>All</star:OrderType>
                                <!--Optional:-->
                                <star:Password>{settings.BSESTAR_PASSWORD}</star:Password>
                                <!--Optional:-->
                                <star:SettType>All</star:SettType>
                                <!--Optional:-->
                                <star:SubOrderType>All</star:SubOrderType>
                                <!--Optional:-->
                                <star:ToDate>{to_date}</star:ToDate>
                                <!--Optional:-->
                                <star:TransType>ALL</star:TransType>
                                <!--Optional:-->
                                <star:UserId>{settings.BSESTAR_USER_ID}</star:UserId>
                            </ns:Param>
                        </ns:OrderStatus>
                    </soap:Body>
                </soap:Envelope> """
    resp = instance.send_request(user if user else None, data)
    if resp['status_code'] == 200:
        return True, resp['data']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def emandate_auth_url(user, mandateid):
    instance = derive_class_instance(api_register.EMANDATE_AUTH_URL)
    data = {
        "UserId": settings.BSESTAR_USER_ID,
        "MemberCode": settings.BSESTAR_MEMBER_CODE,
        "Password": settings.BSESTAR_PASSWORD,
        "MandateID": mandateid,
        "ClientCode": user.bsestar_client_code,
    }
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        if resp['data']['Status'] == '100':
            return True, resp['data']['ResponseString']
        logger.error(resp['data']['ResponseString'])
        return False, resp['data']['ResponseString']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def payment_gateway(user, order_det):
    instance = derive_class_instance(api_register.PAYMENT_GATEWAY)
    is_upi_payment = order_det["payment_mode"] == PaymentModeEnum.UPI.value
    data = {
        "LoginId": settings.BSESTAR_USER_ID,
        "Password": settings.BSESTAR_PASSWORD,
        "membercode": settings.BSESTAR_MEMBER_CODE,
        "clientcode": user.bsestar_client_code,
        "modeofpayment": "UPI" if is_upi_payment else "DIRECT",
        "bankid": order_det["bank_code"],
        "accountnumber": order_det["bank_account_number"],
        "ifsc": order_det["bank_ifsc"],
        "ordernumber": order_det["order_numbers"],
        "totalamount": order_det["amount"],
        "internalrefno": "",
        "NEFTreference": "",
        "mandateid": "",
        "vpaid": order_det["vpa_id"] if is_upi_payment else "",
        "loopbackURL": settings.BSESTAR_PG_RETURN_URL,
        "allowloopBack": "Y",
        "filler1": "",
        "filler2": "",
        "filler3": "",
        "filler4": "",
        "filler5": ""
    }
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        if resp['data']['statuscode'] == '100':
            return True, resp['data']['responsestring']
        logger.error(resp['data']['responsestring'])
        return False, resp['data']['responsestring']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def order_payment_status(user, order_number):
    success, password = _get_password_mfapi(user)
    if not success:
        logger.error(password)
        return False, password
    instance = derive_class_instance(api_register.ORDER_PAYMENT_STATUS)
    data = f""" <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
                               xmlns:ns="{settings.BSESTAR_API_ACTION_BASE_URL}/2016/01/">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>{settings.BSESTAR_API_ACTION_BASE_URL}/2016/01/{'IStarMFWebService' if settings.ENV=='PRD' else 'IMFUploadService'}/MFAPI</wsa:Action>
                        <wsa:To>{instance.request_url}</wsa:To>
                    </soap:Header>
                    <soap:Body>
                        <ns:MFAPI>
                            <!--Optional:-->
                            <ns:Flag>11</ns:Flag>
                            <!--Optional:-->
                            <ns:UserId>{settings.BSESTAR_USER_ID}</ns:UserId>
                            <!--Optional:-->
                            <ns:EncryptedPassword>{password}</ns:EncryptedPassword>
                            <!--Optional:-->
                            <ns:param>{user.bsestar_client_code}|{order_number}|BSEMF</ns:param>
                        </ns:MFAPI>
                    </soap:Body>
                </soap:Envelope>
            """
    resp = instance.send_request(user, data)
    logger.info(resp)
    if resp['status_code'] == 200:
        return True, resp['status_message']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def get_password_child_order(user):
    instance = derive_class_instance(api_register.GET_PASSWORD_CHILD_ORDER)
    data = {
        "UserId": settings.BSESTAR_USER_ID,
        "MemberId": settings.BSESTAR_MEMBER_CODE,
        "Password": settings.BSESTAR_PASSWORD,
        "PassKey": settings.BSESTAR_PASSKEY
    }
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        return True if resp['data']['Status'] == '100' else False, resp['data']['ResponseString']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def get_child_order_details(user, sip_det):
    status, password = get_password_child_order(user)
    if not status:
        return False, password
    instance = derive_class_instance(api_register.CHILD_ORDER_DETAILS)
    data = {
        "Date": sip_det['date'],    # dd/mm/YYYY format. sip order placement date
        "MemberCode": settings.BSESTAR_MEMBER_CODE,
        "ClientCode": user.bsestar_client_code,
        "SystematicPlanType": "XSIP",
        "RegnNo": sip_det["xsip_regno"],
        "EncryptedPassword": password
    }
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        if resp['data']['Status'] == '100':
            return True, resp['data']['ChildOrderDetails']
        return False, resp['data']['Message']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def allotment_statement(from_date=None, to_date=None, user=None, order_id=None, client_code=None):
    """ Fetch order log
    Params:
    user: User instance
    from_date = date instance
    to_date = date instance
    """
    from_date = from_date.strftime("%d/%m/%Y") if from_date else timezone.now().strftime("%d/%m/%Y")
    to_date = to_date.strftime("%d/%m/%Y") if to_date else timezone.now().strftime("%d/%m/%Y")
    client_code = client_code if client_code else ""
    order_id = order_id if order_id else ""
    instance = derive_class_instance(api_register.ALLOTMENT_STATEMENT)
    data = f""" <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
                               xmlns:ns="http://www.bsestarmf.in/2016/01/" 
                               xmlns:star="http://schemas.datacontract.org/2004/07/StarMFWebService">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/AllotmentStatement</wsa:Action>
                        <wsa:To>{instance.request_url}</wsa:To>
                    </soap:Header>
                    <soap:Body>
                        <ns:AllotmentStatement>
                            <!--Optional:-->
                            <ns:Param>
                                <!--Optional:-->
                                <star:ClientCode>{client_code}</star:ClientCode>
                                <!--Optional:-->
                                <star:Filler1/>
                                <!--Optional:-->
                                <star:Filler2/>
                                <!--Optional:-->
                                <star:Filler3/>
                                <!--Optional:-->
                                <star:FromDate>{from_date}</star:FromDate>
                                <!--Optional:-->
                                <star:MemberCode>{settings.BSESTAR_MEMBER_CODE}</star:MemberCode>
                                <!--Optional:-->
                                <star:OrderNo>{order_id}</star:OrderNo>
                                <!--Optional:-->
                                <star:OrderStatus>All</star:OrderStatus>
                                <!--Optional:-->
                                <star:OrderType>All</star:OrderType>
                                <!--Optional:-->
                                <star:Password>{settings.BSESTAR_PASSWORD}</star:Password>
                                <!--Optional:-->
                                <star:SettType>All</star:SettType>
                                <!--Optional:-->
                                <star:SubOrderType>All</star:SubOrderType>
                                <!--Optional:-->
                                <star:ToDate>{to_date}</star:ToDate>
                                <!--Optional:-->
                                <star:UserId>{settings.BSESTAR_USER_ID}</star:UserId>
                            </ns:Param>
                        </ns:AllotmentStatement>
                    </soap:Body>
                </soap:Envelope> """
    resp = instance.send_request(user if user else None, data)
    if resp['status_code'] == 200:
        return True, resp['data']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def redemption_statement(from_date=None, to_date=None, user=None, order_id=None, client_code=None):
    """ Fetch order log
    Params:
    user: User instance
    from_date = date instance
    to_date = date instance
    """
    from_date = from_date.strftime("%d/%m/%Y") if from_date else timezone.now().strftime("%d/%m/%Y")
    to_date = to_date.strftime("%d/%m/%Y") if to_date else timezone.now().strftime("%d/%m/%Y")
    client_code = client_code if client_code else ""
    order_id = order_id if order_id else ""
    instance = derive_class_instance(api_register.REDEMPTION_STATEMENT)
    data = f""" <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" 
                               xmlns:ns="http://www.bsestarmf.in/2016/01/" 
                               xmlns:star="http://schemas.datacontract.org/2004/07/StarMFWebService">
                    <soap:Header xmlns:wsa="http://www.w3.org/2005/08/addressing">
                        <wsa:Action>http://www.bsestarmf.in/2016/01/IStarMFWebService/RedemptionStatement</wsa:Action>
                        <wsa:To>{instance.request_url}</wsa:To>
                    </soap:Header>
                    <soap:Body>
                        <ns:RedemptionStatement>
                            <!--Optional:-->
                            <ns:Param>
                                <!--Optional:-->
                                <star:ClientCode>{client_code}</star:ClientCode>
                                <!--Optional:-->
                                <star:Filler1/>
                                <!--Optional:-->
                                <star:Filler2/>
                                <!--Optional:-->
                                <star:Filler3/>
                                <!--Optional:-->
                                <star:FromDate>{from_date}</star:FromDate>
                                <!--Optional:-->
                                <star:MemberCode>{settings.BSESTAR_MEMBER_CODE}</star:MemberCode>
                                <!--Optional:-->
                                <star:OrderNo>{order_id}</star:OrderNo>
                                <!--Optional:-->
                                <star:OrderStatus>All</star:OrderStatus>
                                <!--Optional:-->
                                <star:OrderType>All</star:OrderType>
                                <!--Optional:-->
                                <star:Password>{settings.BSESTAR_PASSWORD}</star:Password>
                                <!--Optional:-->
                                <star:SettType>All</star:SettType>
                                <!--Optional:-->
                                <star:SubOrderType>All</star:SubOrderType>
                                <!--Optional:-->
                                <star:ToDate>{to_date}</star:ToDate>
                                <!--Optional:-->
                                <star:UserId>{settings.BSESTAR_USER_ID}</star:UserId>
                            </ns:Param>
                        </ns:RedemptionStatement>
                    </soap:Body>
                </soap:Envelope> """
    resp = instance.send_request(user if user else None, data)
    if resp['status_code'] == 200:
        return True, resp['data']
    logger.error(resp)
    return False, resp['status_message']


@timeit
def redeem_2fa(user):
    instance = derive_class_instance(api_register.REDEEM_2FA)
    data = {
        "LoginID": settings.BSESTAR_USER_ID,
        "Password": settings.BSESTAR_PASSWORD,
        "MemberCode": settings.BSESTAR_MEMBER_CODE,
        "ClientCode": user.bsestar_client_code,
        "LoopbackReturnUrl": settings.BSESTAR_PG_REDIRECT_URL,
        "AllowLoopBack": "Y"
    }
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        return True, resp['data']
    logger.error(resp)
    return False, resp['status_message']
