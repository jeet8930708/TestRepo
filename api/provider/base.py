from abc import ABC, abstractmethod


class ProviderBase(ABC):

    @property
    @abstractmethod
    def base_url(self):
        pass

    @property
    @abstractmethod
    def client_id(self):
        pass

    @property
    @abstractmethod
    def client_secret(self):
        pass
