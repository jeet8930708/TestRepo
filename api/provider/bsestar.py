from django.conf import settings
from .base import ProviderBase


class ProviderBSEStar(ProviderBase):

    @property
    def base_url(self):
        return settings.BSESTAR_BASE_URL

    @property
    def client_id(self):
        return None

    @property
    def client_secret(self):
        return None

    @property
    def user_id(self):
        return settings.BSESTAR_USER_ID

    @property
    def member_code(self):
        return settings.BSESTAR_MEMBER_CODE

    @property
    def password(self):
        return settings.BSESTAR_PASSWORD


BSESTAR = ProviderBSEStar()
