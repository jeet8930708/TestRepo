from django.conf import settings
from .base import ProviderBase


class ProviderMorningStar(ProviderBase):

    @property
    def base_url(self):
        return settings.MORNINGSTAR_BASE_URL

    @property
    def client_id(self):
        return None

    @property
    def client_secret(self):
        return settings.MORNINGSTAR_ACCESS_CODE


MORNINGSTAR = ProviderMorningStar()