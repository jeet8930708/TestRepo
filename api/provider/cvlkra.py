from django.conf import settings
from .base import ProviderBase


class ProviderCVLKRA(ProviderBase):

    @property
    def base_url(self):
        return settings.CVLKRA_BASE_URL

    @property
    def client_id(self):
        return settings.CVLKRA_POSCODE

    @property
    def client_secret(self):
        return settings.CVLKRA_PASSWORD


CVLKRA = ProviderCVLKRA()
