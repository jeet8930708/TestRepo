from django.conf import settings
from .base import ProviderBase


class ProviderDigio(ProviderBase):

    @property
    def base_url(self):
        return settings.DIGIO_BASE_URL

    @property
    def client_id(self):
        return settings.DIGIO_CLIENT_ID

    @property
    def client_secret(self):
        return settings.DIGIO_CLIENT_SECRET


DIGIO = ProviderDigio()