class APIRegister:

    CKYC_STATUS_CHECK = 'api.endpoint.digio.CKYCStatusCheckEndpoint'
    KRA_CHECK_PAN_STATUS = 'api.endpoint.digio.KRACheckPanStatusEndpoint'
    CKYC_DATA_DOWNLOAD = 'api.endpoint.digio.CKYCDataDownloadEndpoint'
    KYC_REQUEST_CREATE = 'api.endpoint.digio.KYCRequestCreateEndpoint'
    KYC_REQUEST_DETAIL = 'api.endpoint.digio.KYCRequestDetailEndpoint'
    KYC_REQUEST_MEDIA = 'api.endpoint.digio.KYCRequestMediaEndpoint'
    BANK_VERIFICATION = 'api.endpoint.digio.BankVerificationEndpoint'
    ESIGN_REQUEST = 'api.endpoint.digio.EsignRequestEndpoint'
    ESIGN_DOWNLOAD = 'api.endpoint.digio.EsignTemplateDownloadEndpoint'

    MUTUAL_FUND_DETAIL = 'api.endpoint.morning_star.MFDetailEndpoint'
    NAV_HISTORY = 'api.endpoint.morning_star.NAVHistoryEndpoint'
    NAV_PRICE = 'api.endpoint.morning_star.NAVPriceEndpoint'
    GROWTH_10K = 'api.endpoint.morning_star.GrowthOf10KEndpoint'

    UCC_REGISTRATION = 'api.endpoint.bse_star.UCCRegistrationEndpoint'
    GET_PASSWORD_MFAPI = 'api.endpoint.bse_star.GetPasswordMFAPIEndpoint'
    FATCA_UPLOAD = 'api.endpoint.bse_star.FATCAUploadEndpoint'
    MANDATE_REGISTER_BSESTAR = 'api.endpoint.bse_star.MandateRegistrationEndpoint'
    EMANDATE_AUTH_URL = 'api.endpoint.bse_star.EMandateAuthURLEndpoint'
    GET_PASSWORD_FILE_UPLOAD = 'api.endpoint.bse_star.GetPasswordFileUploadEndpoint'
    AOF_IMAGE_UPLOAD = 'api.endpoint.bse_star.AOFImageUploadEndpoint'
    GET_PASSWORD_MFORDER = 'api.endpoint.bse_star.GetPasswordMFOrderEndpoint'
    XSIP_ORDER_ENTRY = 'api.endpoint.bse_star.XSIPOrderEntryEndpoint'
    ORDER_ENTRY = 'api.endpoint.bse_star.OrderEntryEndpoint'
    ORDER_STATUS = 'api.endpoint.bse_star.OrderStatusEndpoint'
    PAYMENT_GATEWAY = 'api.endpoint.bse_star.PaymentGatewayEndpoint'
    ORDER_PAYMENT_STATUS = 'api.endpoint.bse_star.OrderPaymentStatusMFAPIEndpoint'
    GET_PASSWORD_CHILD_ORDER = 'api.endpoint.bse_star.GetPasswordChildOrderEndpoint'
    CHILD_ORDER_DETAILS = 'api.endpoint.bse_star.ChildOrderDetailsEndpoint'
    ALLOTMENT_STATEMENT = 'api.endpoint.bse_star.AllotmentStatementEndpoint'
    REDEMPTION_STATEMENT = 'api.endpoint.bse_star.RedemptionStatementEndpoint'
    REDEEM_2FA = 'api.endpoint.bse_star.Redeem2FAEndpoint'

    GET_PASSWORD_CVLKRA = 'api.endpoint.cvlkra.GetPasswordEndpoint'
    INSERT_UPDATE_KYC_RECORD = 'api.endpoint.cvlkra.InsertUpdateKYCRecordEndpoint'


api_register = APIRegister()
