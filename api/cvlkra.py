import time
import logging

from django.core.cache import cache
from django.conf import settings
from django.utils import timezone
from core.decorators import timeit
from order.enums import BuySellEnum
from .register import api_register
from .util import derive_class_instance
from .api_params import (
    get_cvlkra_input_params
)

logger = logging.getLogger('api')


@timeit
def _get_password(user):
    instance = derive_class_instance(api_register.GET_PASSWORD_CVLKRA)
    data = f""" <soap:Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
                    <soap:Body>
                        <GetPassword xmlns="https://krapancheck.cvlindia.com">
                            <webApi>
                                <userName>{settings.CVLKRA_USERNAME}</userName>
                                <posCode>{settings.CVLKRA_POSCODE}</posCode>
                                <password>{settings.CVLKRA_PASSWORD}</password>
                                <passKey>{settings.CVLKRA_PASSKEY}</passKey>
                            </webApi>
                        </GetPassword>
                    </soap:Body>
                </soap:Envelope>
            """
    resp = instance.send_request(user, data)
    if resp['status_code'] == 200:
        return True, resp['status_message']
    logger.error(resp)
    return False, resp['status_message']