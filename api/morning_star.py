from core.decorators import timeit

from .register import api_register
from .util import derive_class_instance


@timeit
def mutual_fund_detail(isin):
    instance = derive_class_instance(api_register.MUTUAL_FUND_DETAIL)
    instance.ISIN = isin
    resp = instance.send_request(None)
    if resp['status_code'] == 200 and resp["status_message"] == "Success":
        if int(resp['data']['status']['code']) == 0:
            return resp['status_message'], resp['data']['data'][0]['api']
        else:
            return 'Error', resp['data']['status']['message']
    return resp['status_message'], resp.get('error', '')


@timeit
def nav_history(mstarid, start_date, end_date):
    instance = derive_class_instance(api_register.NAV_HISTORY)
    instance.MSTARID = mstarid
    instance.START_DATE = start_date
    instance.END_DATE = end_date
    resp = instance.send_request(None)
    if resp['status_code'] == 200 and resp["status_message"] == "Success":
        if int(resp['data']['status']['code']) == 0:
            return resp['status_message'], resp['data']['data']['Prices']
        else:
            return 'Error', resp['data']['status']['message']
    return resp['status_message'], resp.get('error', '')


@timeit
def nav_price(mstarid):
    instance = derive_class_instance(api_register.NAV_PRICE)
    instance.MSTARID = mstarid
    resp = instance.send_request(None)
    if resp['status_code'] == 200 and resp["status_message"] == "Success":
        if int(resp['data']['status']['code']) == 0:
            return resp['status_message'], resp['data']['data']['api']
        else:
            return 'Error', resp['data']['status']['message']
    return resp['status_message'], resp.get('error', '')


@timeit
def growth_10k(mstarid, start_date, end_date):
    instance = derive_class_instance(api_register.GROWTH_10K)
    instance.MSTARID = mstarid
    instance.START_DATE = start_date
    instance.END_DATE = end_date
    resp = instance.send_request(None)
    if resp['status_code'] == 200 and resp["status_message"] == "Success":
        if int(resp['data']['status']['code']) == 0:
            return resp['status_message'], resp['data']['data']['CalendarReturn']
        else:
            return 'Error', resp['data']['status']['message']
    return resp['status_message'], resp.get('error', '')

