from rest_framework.serializers import (
    ModelSerializer,
    CharField,
    SerializerMethodField
)

from .models import (
    Video,
    VideoBookmark,
    VideoView
)


class VideoSerializer(ModelSerializer):
    queryset = Video.objects.all()
    owner_name = CharField(source='owner.get_full_name', read_only=True)
    category_name = CharField(source='category.name', read_only=True)
    is_saved = SerializerMethodField()

    class Meta:
        model = Video
        fields = ['id', 'owner', 'link', 'name', 'banner_link', 'video_length', 'description',
                  'share_count', 'category', 'owner_name', 'category_name', 'views', 'is_saved',
                  'created_on']
        read_only_fields = ('id', 'owner')

    def get_is_saved(self, obj):
        request = self.context['request']
        if request.user.is_authenticated:
            return VideoBookmark.objects.filter(
                video=obj, user=request.user).exists()
        return False

    def create(self, validated_data):
        request = self.context['request']
        validated_data['owner'] = request.user
        return super().create(validated_data)


class VideoBookmarkSerializer(ModelSerializer):
    queryset = VideoBookmark.objects.all()
    video_link = CharField(source='video.link', read_only=True)
    video_length = CharField(source='video.video_length', read_only=True)
    video_banner_link = CharField(source='video.banner_link', read_only=True)
    views = CharField(source='video.views', read_only=True)
    category = CharField(source='video.category.name', read_only=True)
    video_name = CharField(source='video.name', read_only=True)

    class Meta:
        model = VideoBookmark
        fields = ['id', 'video', 'user', 'video_link', 'video_length', 'views', 'category',
                  'video_banner_link', 'video_name']
        read_only_fields = ('id', 'user')

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return super().create(validated_data)

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(user=self.context['request'].user)


class VideoViewSerializer(ModelSerializer):
    queryset = VideoView.objects.all()
    video_link = CharField(source='video.link', read_only=True)
    video_name = CharField(source='video.name', read_only=True)

    class Meta:
        model = VideoView
        fields = ['id', 'video', 'user', 'video_link', 'video_name']
        read_only_fields = ('id', 'user')

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return super().create(validated_data)
