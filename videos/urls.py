from django.urls import path

from .views import (
    VideoCreateView,
    VideoListView,
    VideoRetrieveUpdateView,
    VideoBookmarkListCreateView,
    VideoBookmarkRetrieveUpdateView,
    VideoViewListCreateView,
    VideoSuggestionListView,
)

urlpatterns = [

    path('create/', VideoCreateView.as_view(), name='videos-create'),
    path('all/', VideoListView.as_view(), name='videos-list'),
    path('suggested/', VideoSuggestionListView.as_view(), name='videos-suggestion-list'),
    path('retrieve/<int:pk>/', VideoRetrieveUpdateView.as_view(), name='video-retrieve'),
    path('update/<int:pk>/', VideoRetrieveUpdateView.as_view(), name='video-update'),
    
    path('saved/', VideoBookmarkListCreateView.as_view(), name='videos-saved-create'),
    path('saved/', VideoBookmarkListCreateView.as_view(), name='videos-saved-list'),
    path('saved/retrieve/<int:pk>/', VideoBookmarkRetrieveUpdateView.as_view(), name='video-saved-retrieve'),
    path('saved/update/<int:pk>/', VideoBookmarkRetrieveUpdateView.as_view(), name='video-saved-update'),
    path('saved/delete/<int:pk>', VideoBookmarkRetrieveUpdateView.as_view(), name='video-saved-update'),
    
    path('viewed/', VideoViewListCreateView.as_view(), name='videos-view-create'),

]
