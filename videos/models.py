from django.db import models
from django.contrib.auth import get_user_model
from master.models import Category

from django.dispatch import receiver
from django.db.models.signals import post_save
User = get_user_model()


class Video(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, blank=False, null=False)
    link = models.CharField(max_length=200, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    share_count = models.IntegerField(default=0)
    views = models.IntegerField(default=0)
    video_length = models.CharField(max_length=200, blank=True, null=True)
    banner_link = models.CharField(max_length=200, blank=True, null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    suggested_video = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


class VideoBookmark(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    video = models.ForeignKey(Video, on_delete=models.CASCADE)
    is_bookmark = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('user', 'video')


class VideoView(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    video = models.ForeignKey(Video, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    # class Meta:
    #     unique_together = ('user', 'video')

@receiver(post_save, sender=VideoView)
def update_post_likes(sender, instance, created, *args, **kwargs):
    count = VideoView.objects.filter(video=instance.video).count()
    instance.video.views = count
    instance.video.save()
