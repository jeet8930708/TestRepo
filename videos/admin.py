from django.contrib import admin
from .models import (
    Video,
)


class VideoAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', 'category', 'share_count',
                    'views', 'video_length', 'suggested_video', 'created_on', 'updated_on')
    list_filter = ('category','suggested_video',)
    search_fields = ('category__name', 'name')
admin.site.register(Video, VideoAdmin)

