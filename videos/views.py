from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from utils.response import response_200
from rest_framework.status import HTTP_200_OK
from .models import Video, VideoBookmark,VideoView
from .serializers import (
    VideoSerializer,
    VideoBookmarkSerializer,
    VideoViewSerializer
)


class VideoCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = VideoSerializer
    queryset = Video.objects.all()

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Video created successfully", response.data)


class VideoListView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = VideoSerializer
    queryset = Video.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        if 'sort' not in self.request.query_params:
            return queryset
        sort_by = self.request.query_params['sort']
        if sort_by == "newest":
            queryset = queryset.order_by('-created_on')
        elif sort_by == "oldest":
            queryset = queryset.order_by('created_on')
        elif sort_by == "most_views":
            queryset = queryset.order_by('-views')
        return queryset

    def list(self, request, *args, **kwargs):
        res = super().list(request, *args, **kwargs)
        results = res.data
        results_len = results["count"]
        response = {
            'status_code': HTTP_200_OK,
            'status_message': '{} record(s) found'.format(results_len) if results_len > 0 else 'No record found',
            'count': results_len,
            'previous': results['previous'],
            'next': results['next'],
            'results': results['results'],
        }
        return response_200("Video list fetched successfully", response)


# ========================================================================
class VideoSuggestionListView(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = VideoSerializer
    queryset = Video.objects.filter(suggested_video=True)

    def list(self, request, *args, **kwargs):
        res = super().list(request, *args, **kwargs)
        results = res.data
        results_len = results["count"]
        response = {
            'status_code': HTTP_200_OK,
            'status_message': '{} record(s) found'.format(results_len) if results_len > 0 else 'No record found',
            'count': results_len,
            'previous': results['previous'],
            'next': results['next'],
            'results': results['results'],
        }
        return response_200("Suggested Video list fetched successfully", response)


# ========================================================================
class VideoRetrieveUpdateView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = VideoSerializer
    queryset = Video.objects.all()

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        video = response.data
        response.data["is_saved"] = False
        saved = VideoBookmark.objects.filter(video=video["id"], user=request.user).first()
        if saved is not None:
            response.data["is_saved"] = True
        return response_200("Video details retrieved successfully", response.data)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        return response_200("Video updated successfully", response.data)


# ========================================================================

class VideoBookmarkListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = VideoBookmarkSerializer
    queryset = VideoBookmark.objects.all()
    
    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("VideoBookmark created successfully", response.data)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("VideoBookmark list fetched successfully", response.data)


# ========================================================================
class VideoBookmarkRetrieveUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = VideoBookmarkSerializer
    queryset = VideoBookmark.objects.all()

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("VideoBookmark details retrieved successfully", response.data)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        return response_200("VideoBookmark updated successfully", response.data)

    def delete(self, request, *args, **kwargs):
        # response = super().delete(request, *args, **kwargs)
        print(kwargs.get('pk'))
        print(request.user)
        video = VideoBookmark.objects.get(video_id=kwargs.get('pk'), user= request.user)
        print(video)
        video.delete()
        return response_200("VideoBookmark unsaved successfully")

# ========================================================================


class VideoViewListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = VideoViewSerializer
    queryset = VideoView.objects.all()

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("VideoView created successfully", response.data)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("VideoBookmark list fetched successfully", response.data)
