# Generated by Django 4.0.3 on 2022-05-15 03:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0004_alter_videoview_unique_together'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='banner_link',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
