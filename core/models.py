import random
import secrets
import logging
from datetime import datetime

from django.conf import settings
from django.core.validators import MinLengthValidator
from django.db import models
from django.db.models import Q
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from rest_framework.serializers import ValidationError
from rest_framework.exceptions import APIException
from rest_framework_simplejwt.tokens import RefreshToken

from master.models import Income, Occupation, Country, BankBranch
from api.rest import bank_verify, ckyc_data_download, kra_check_pan_status, kyc_request_media, kyc_request_detail
from .enums import (
    BankAccountTypeEnum,
    GenderEnum,
    AgeRangeEnum,
    MaritalStatusEnum,
    KYCRequestStatusEnum,
    ResidentStatusEnum,
    NomineeRelationEnum,
    UserOTPTypeEnum,
)
from .storage import UserMediaStorage, KYCMediaStorage
from utils.order_util import generate_random_token
from utils.sms_util import send_otp
from utils.image_util import base64_to_image, upload_file
from utils import str_to_datetime, str_to_date

logger = logging.getLogger(__name__)

# Create your models here.
phone_regex = RegexValidator(regex=r'^\d{10}$', message=_("Enter a valid 10 digit phone number."))
pan_regex = RegexValidator(regex=r'^[A-Z]{5}\d{4}[A-Z]$', message=_("Enter a valid PAN number."))
ifsc_regex = RegexValidator(regex=r'^[A-Z]{4}0\d{6}$', message=_("Enter a valid IFSC code."))


class UserOtpManager(models.Manager):

    def validate_otp(self, phone, otp, otp_type=UserOTPTypeEnum.LOGIN.value):
        dt = timezone.now()
        latest_otp = self.filter(phone=phone,
                                 otp_type=otp_type,
                                 otp_validated_on__isnull=True,
                                 otp_expires_on__gte=dt).order_by('-otp_generated_on').first()
        if not latest_otp or not check_password(str(otp), latest_otp.otp):
            return False
        latest_otp.otp_validated_on = dt
        latest_otp.save()
        return True


class UserOtp(models.Model):
    phone = models.CharField(_('phone'), max_length=10, validators=[phone_regex])
    otp = models.CharField(max_length=128)
    otp_type = models.PositiveSmallIntegerField(choices=UserOTPTypeEnum.choices(),
                                                default=UserOTPTypeEnum.LOGIN.value)
    otp_generated_on = models.DateTimeField(blank=True, null=True)
    otp_expires_on = models.DateTimeField(blank=True, null=True)
    otp_validated_on = models.DateTimeField(blank=True, null=True)

    objects = UserOtpManager()

    def __str__(self):
        return f"{self.phone} - {self.otp}"


@receiver(pre_save, sender=UserOtp)
def user_generate_otp(sender, instance, **kwargs):
    if not instance.otp:
        otp = instance.phone[-4:] if instance.phone in settings.TEST_ACCOUNTS else random.randint(1000, 9999)
        sms_sent, message = False, ""
        if instance.otp_type == UserOTPTypeEnum.LOGIN.value:
            message = f"{otp} is your OTP to verify with FIKAA, India's leading Investment App for Women. " \
                      f"Please do not share OTP with anyone.\n\n-Team FIKAA\n\nLmuZMuU9eBn"
        elif instance.otp_type == UserOTPTypeEnum.NOMINEE_AUTHENTICATION.value:
            message = f"{otp} is your OTP to authenticate nominee with FIKAA, India's leading Investment App for Women. " \
                      f"Please do not share OTP with anyone.\n\n-Team FIKAA"
        if message:
            sms_sent = True if instance.phone in settings.TEST_ACCOUNTS else send_otp(instance.phone, message)
        if sms_sent:
            instance.otp = make_password(str(otp))
            instance.otp_generated_on = timezone.now()
            instance.otp_expires_on = instance.otp_generated_on + timezone.timedelta(seconds=settings.OTP_VALIDITY)
        else:
            raise APIException("Error in generating OTP. Please try after sometime.")


# ========================================================================
class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, phone, password, **extra_fields):
        """
        Create and save a user with the given phone, email, and password.
        """
        if not phone:
            raise ValueError('The given phone must be set')
        if not password:
            password = self.make_random_password()
        username = extra_fields.pop('username') if 'username' in extra_fields else phone
        user = self.model(username=username, phone=phone, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def get_or_create_user(self, phone, password=None, **extra_fields):
        try:
            user = self.get(username=phone)
            return user
        except User.DoesNotExist:
            extra_fields.setdefault('is_staff', False)
            extra_fields.setdefault('is_superuser', False)
            extra_fields.setdefault('is_active', True)
            return self._create_user(phone, password, **extra_fields)

    def create_superuser(self, phone, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(phone, password, **extra_fields)


class User(AbstractUser):
    email = models.EmailField(_('email address'), unique=True,
                              error_messages={
                                  'unique': _("A user with that email already exists."),
                              })
    phone = models.CharField(_('phone'), max_length=10, validators=[phone_regex], unique=True,
                             error_messages={
                                 'unique': _("A user with that phone already exists."),
                            })
    image = models.ImageField(storage=UserMediaStorage, blank=True, null=True)
    age_range = models.PositiveSmallIntegerField(choices=AgeRangeEnum.choices(), default=AgeRangeEnum.Age_25_to_35.value,
                                                 blank=True, null=True)
    referral_code = models.CharField(max_length=6, blank=True, null=True)
    get_whatsapp_comm = models.BooleanField(default=False)
    agree_terms = models.BooleanField(default=False)
    referrer_code = models.CharField(max_length=6, blank=True, null=True)
    referred_by = models.ForeignKey("self", on_delete=models.SET_NULL, blank=True, null=True)
    referral_count = models.PositiveSmallIntegerField(default=0)
    account_completed = models.BooleanField(default=False, help_text='Checked if basic details are updated like '
                                                                     'name, email, get_whatsapp_comm, agree_terms')

    pan_verified = models.BooleanField(default=False, help_text='Checked if Pan kra status == "KRA Verified"')
    pan_kra_status = models.CharField(max_length=50, blank=True, null=True,
                                      help_text='Status returned by digio /kyc/kra/get_pan_details api')
    kyc_first_time_submitted = models.BooleanField(default=False,
                                                   help_text='Checked for users submitting kyc for first time')
    kyc_first_time_approved = models.BooleanField(default=False,
                                                  help_text='Checked for users first time kyc approved by cvlkra')
    kyc_first_time_status = models.CharField(max_length=20, choices=KYCRequestStatusEnum.choices(),
                                             blank=True, null=True,
                                             help_text='Status of first time kyc request')
    ckyc_data_downloaded = models.BooleanField(default=False,
                                               help_text='Checked if Pan kra status == "KRA Verified" & '
                                                         'digio /kyc/ckyc/search api returns ckyc number & '
                                                         'digio /kyc/ckyc/download returns kyc data & '
                                                         'kyc data successfully saved in UserKYCDetail')

    pan_detail_submitted = models.BooleanField(default=False, help_text='Checked if Pan, dob, gender '
                                                                        'and name submitted by user')
    citizenship_detail_submitted = models.BooleanField(default=False,
                                                       help_text='Checked if resident_status, country_of_birth, '
                                                                 'city_of_birth updated by user.'
                                                                 'It is auto checked if Pan kra status is '
                                                                 '"KRA Verified" (this is true for users who are '
                                                                 'already kra verified & kyc updated)')
    family_detail_submitted = models.BooleanField(default=False,
                                                  help_text='Checked on submitting marital_status, father_name, '
                                                            'mother_name and nominee details')
    professional_detail_submitted = models.BooleanField(default=False,
                                                        help_text='Checked on submitting occupation, annual_income,'
                                                                  'politically exposed details')
    bank_detail_submitted = models.BooleanField(default=False,
                                                help_text='Checked on submitting bank detail')
    kyc_submitted = models.BooleanField(default=False,
                                        help_text='Checked if pan_details_submitted, citizenship_detail_submitted,'
                                                  'family_detail_submitted, professional_detail_submitted,'
                                                  'bank_detail_submitted')
    nominee_authentication = models.BooleanField(default=False,
                                                 help_text='Checked once user authenticates nominee via otp')
    nominee_authentication_datetime = models.DateTimeField(blank=True, null=True)

    # bse star flags
    bsestar_client_code = models.CharField(max_length=10, blank=True, null=True)
    bsestar_folio_no = models.CharField(max_length=20, blank=True, null=True)
    bsestar_ucc_registered = models.BooleanField(default=False)
    bsestar_fatca_uploaded = models.BooleanField(default=False)
    bsestar_aof_image_uploaded = models.BooleanField(default=False)
    bsestar_registered = models.BooleanField(default=False)

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    @property
    def age(self):
        kyc = self.get_kyc_detail()
        if kyc.dob:
            birthdate = kyc.dob
            today = timezone.localdate()
            age = today.year - birthdate.year - ((today.month, today.day) < (birthdate.month, birthdate.day))
            return age
        else:
            if self.age_range == AgeRangeEnum.Age_Below_25:
                return 24
            elif self.age_range == AgeRangeEnum.Age_25_to_35:
                return 25
            elif self.age_range == AgeRangeEnum.Age_35_to_55:
                return 35
            elif self.age_range == AgeRangeEnum.Age_55_Above:
                return 55
        return 0

    @property
    def image_url(self):
        return self.image.url if self.image else ""

    @property
    def personal_details(self):
        if hasattr(self, 'kyc_personal_details'):
            det = self.kyc_personal_details
            return {
                "prefix": det.prefix,
                "first_name": det.first_name,
                "middle_name": det.middle_name,
                "last_name": det.last_name,
                "full_name": det.full_name_wo_salutation,
                "maiden_prefix": det.maiden_prefix,
                "maiden_first_name": det.maiden_first_name,
                "maiden_middle_name": det.maiden_middle_name,
                "maiden_last_name": det.maiden_last_name,
                "father_prefix": det.father_prefix,
                "father_first_name": det.father_first_name,
                "father_middle_name": det.father_middle_name,
                "father_last_name": det.father_last_name,
                "dob": det.dob,
                "gender": det.gender,
                "corr_line1": det.corr_line1,
                "corr_line2": det.corr_line2,
                "corr_line3": det.corr_line3,
                "corr_city": det.corr_city,
                "corr_dist": det.corr_dist,
                "corr_pin": det.corr_pin,
                "corr_state": det.corr_state,
                "corr_country": det.corr_country,
                "perm_line1": det.perm_line1,
                "perm_line2": det.perm_line2,
                "perm_line3": det.perm_line3,
                "perm_city": det.perm_city,
                "perm_dist": det.perm_dist,
                "perm_pin": det.perm_pin,
                "perm_state": det.perm_state,
                "perm_country": det.perm_country,
                "off_std_code": det.off_std_code,
                "off_tele_number": det.off_tele_number,
                "res_std_code": det.res_std_code,
                "res_tele_number": det.res_tele_number,
                "father_full_name": det.father_full_name_wo_salutation,
                "mother_full_name": det.mother_full_name_wo_salutation,
                "gender": det.gender,
                "dob": det.dob,
            }
        return {}

    @property
    def address_details(self):
        if hasattr(self, 'kyc_personal_details'):
            det = self.kyc_personal_details
            return {
                "address_1": det.perm_line1,
                "address_2": det.perm_line2,
                "address_3": det.perm_line3,
                "address_city": det.perm_city,
                "address_dist": det.perm_dist,
                "address_state": det.perm_state,
                "address_country": det.perm_country,
                "address_pin": det.perm_pin,
                "res_std_code": det.res_std_code,
                "res_tele_number": det.res_tele_number,
                "off_std_code": det.off_std_code,
                "off_tele_number": det.off_tele_number,
                "mob_code": det.mob_code,
                "mobile_no": det.mobile_no or self.phone,
                "email": det.email or self.email
            }
        return {}

    @property
    def family_details(self):
        if hasattr(self, 'family_detail'):
            det = self.family_detail
            return {
                "marital_status": det.marital_status,
                "father_name": det.father_name,
                "mother_name": det.mother_name,
                "nominee_name": det.nominee_name or "",
                "nominee_relation": det.get_nominee_relation_display() if det.nominee_relation else ""
            }
        return {}

    @property
    def professional_details(self):
        if hasattr(self, 'professional_detail'):
            det = self.professional_detail
            return {
                "occ_code": det.occupation.code,
                "occ_name": det.occupation.name,
                "occ_type": det.occupation.type,
                "occ_type_display": det.occupation.get_type_display(),
                "inc_code": det.annual_income.code,
                "inc_name": det.annual_income.bucket,
                "politically_exposed": det.politically_exposed,
            }
        return {}

    @property
    def user_bank_details(self):
        banks = []
        for bank_det in self.bank_details.filter(verified=True).select_related('bank'):
            banks.append({
                'bank_name': bank_det.bank.bank.name,
                'ifsc_code': bank_det.bank.ifsc,
                'account_holder_name': bank_det.account_holder_name,
                'account_type': bank_det.account_type,
                'account_type_display': bank_det.get_account_type_display(),
                'account_number': bank_det.account_number,
                'beneficiary_name': bank_det.beneficiary_name,
                'is_primary_bank': bank_det.is_primary_bank,
                'bank_address': bank_det.bank.address,
                'bank_city': bank_det.bank.city,
                'bank_state': bank_det.bank.state,
                'bank_branch': bank_det.bank.branch,
            })
        return banks

    @property
    def user_kyc_details(self):
        if hasattr(self, 'kyc_detail'):
            det = self.kyc_detail
            return {
                "ckyc_number": det.ckyc_number,
                "name": det.name,
                "age": self.age,
                "pan_number": det.pan_number,
                "aadhaar_number": det.aadhaar_number,
                "photo": det.photo.url if det.photo else "",
                "sign": det.signature.url if det.signature else "",
                "aadhaar": det.aadhaar_image.url if det.aadhaar_image else "",
                "pan": det.pan_image.url if det.pan_image else ""
            }
        return {}

    @property
    def pan_no(self):
        kyc_det = self.get_kyc_detail()
        return kyc_det.pan_number

    def get_kyc_detail(self):
        return self.kyc_detail if hasattr(self, 'kyc_detail') else UserKYCDetail(user=self)

    def refresh_token(self):
        refresh = RefreshToken.for_user(self)
        return {
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        }

    def get_latest_kyc_request(self):
        return self.kyc_request.all().first()

    def update_referral_count(self):
        self.referral_count = User.objects.filter(referred_by=self).count()
        self.save(update_fields=['referral_count'])
        return self.referral_count

    def _has_submitted_pan_detail(self):
        kyc_det = self.get_kyc_detail()
        return True if kyc_det.pan_number else False

    def _has_submitted_citizenship_detail(self):
        return hasattr(self, 'citizenship_detail')

    def _has_submitted_family_detail(self):
        return hasattr(self, 'family_detail')

    def _has_submitted_professional_detail(self):
        return hasattr(self, 'professional_detail')

    def _has_submitted_bank_detail(self):
        return self.bank_details.all().exists()

    def _update_detail_submitted(self, det_type, status, save):
        setattr(self, det_type, status)
        if save:
            self.save()

    def reconcile_kyc_status(self):
        self.pan_detail_submitted = self._has_submitted_pan_detail()
        self.citizenship_detail_submitted = self._has_submitted_citizenship_detail()
        self.family_detail_submitted = self._has_submitted_family_detail()
        self.professional_detail_submitted = self._has_submitted_professional_detail()
        self.bank_detail_submitted = self._has_submitted_bank_detail()
        self.kyc_submitted = self.pan_verified \
                             and self.pan_detail_submitted \
                             and self.citizenship_detail_submitted \
                             and self.family_detail_submitted \
                             and self.professional_detail_submitted \
                             and self.bank_detail_submitted
        self.save()

    def update_kyc_first_time_status(self, status, save=True):
        self._update_detail_submitted('kyc_first_time_status', status, False)
        if status == KYCRequestStatusEnum.Approved.value:
            self._update_detail_submitted('kyc_first_time_submitted', True, False)
            self._update_detail_submitted('kyc_first_time_approved', True, save)
        elif status in (KYCRequestStatusEnum.Approval_Pending.value, KYCRequestStatusEnum.Rejected.value):
            self._update_detail_submitted('kyc_first_time_submitted', True, save)

    def update_pan_kra_status(self, status, save=True):
        self._update_detail_submitted('pan_kra_status', status, save=False)
        self.update_pan_verified(True if status == 'KRA Verified' else False, save)

    def update_ckyc_data_downloaded(self, status, save=True):
        self._update_detail_submitted('ckyc_data_downloaded', status, save)

    def update_pan_detail_submitted(self, status=True, save=True):
        self._update_detail_submitted('pan_detail_submitted', status, save)

    def update_citizenship_detail_submitted(self, status=True, save=True):
        self._update_detail_submitted('citizenship_detail_submitted', status, save)

    def update_family_detail_submitted(self, status=True, save=True):
        self._update_detail_submitted('family_detail_submitted', status, save)

    def update_professional_detail_submitted(self, status=True, save=True):
        self._update_detail_submitted('professional_detail_submitted', status, save)

    def update_bank_detail_submitted(self, status=True, save=True):
        self._update_detail_submitted('bank_detail_submitted', status, save)

    def update_pan_verified(self, status=True, save=True):
        self._update_detail_submitted('pan_verified', status, save)

    def update_ucc_registration_status(self, status=True, save=True):
        self._update_detail_submitted('bsestar_ucc_registered', status, save)

    def update_fatca_upload_status(self, status=True, save=True):
        self._update_detail_submitted('bsestar_fatca_uploaded', status, save)

    def update_aof_image_upload_status(self, status=True, save=True):
        self._update_detail_submitted('bsestar_aof_image_uploaded', status, save)

    def update_bsestar_ucc_registered(self):
        self.bsestar_ucc_registered = True
        self.save(update_fields=['bsestar_ucc_registered'])

    def update_nominee_authetication(self):
        self.nominee_authentication = True
        self.nominee_authentication_datetime = timezone.now()
        self.save(update_fields=['nominee_authentication', 'nominee_authentication_datetime'])


@receiver(pre_save, sender=User)
def update_bsestar_client_code(sender, instance, **kwargs):
    if not instance.bsestar_client_code:
        instance.bsestar_client_code = instance.username[:10]


@receiver(pre_save, sender=User)
def generate_referral_code(sender, instance, **kwargs):
    if not instance.referral_code:
        instance.referral_code = secrets.token_hex(3)


@receiver(pre_save, sender=User)
def update_referrer(sender, instance, **kwargs):
    if instance.referrer_code and not instance.referred_by:
        try:
            instance.referred_by = User.objects.get(Q(referral_code=instance.referrer_code) & ~(Q(id=instance.id)))
        except User.DoesNotExist:
            raise APIException("Invalid Referrer Code")


@receiver(pre_save, sender=User)
def update_account_completed(sender, instance, **kwargs):
    if not instance.account_completed and instance.email and instance.phone and instance.first_name \
            and instance.agree_terms:
        instance.account_completed = True


@receiver(pre_save, sender=User)
def update_kyc_status(sender, instance, **kwargs):
    if instance.pan_verified:
        instance.citizenship_detail_submitted = True
    instance.kyc_submitted = instance.pan_verified \
                         and instance.pan_detail_submitted \
                         and instance.citizenship_detail_submitted \
                         and instance.family_detail_submitted \
                         and instance.professional_detail_submitted \
                         and instance.bank_detail_submitted


@receiver(pre_save, sender=User)
def update_bsestar_status(sender, instance, **kwargs):
    instance.bsestar_registered = instance.bsestar_ucc_registered \
                         and instance.bsestar_fatca_uploaded \
                         and instance.bsestar_aof_image_uploaded


@receiver(post_save, sender=User)
def add_referral_count(sender, instance, created, **kwargs):
    if instance.referred_by:
        instance.referred_by.update_referral_count()


@receiver(post_delete, sender=User)
def remove_referral_count(sender, instance, **kwargs):
    if instance.referred_by:
        instance.referred_by.update_referral_count()


# ========================================================================
class UserKYCRequest(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='kyc_request')
    workflow_name = models.CharField(max_length=50)
    kid = models.CharField(max_length=50)
    rid = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    expire_in_days = models.PositiveSmallIntegerField()
    status = models.CharField(max_length=20, choices=KYCRequestStatusEnum.choices())
    reference_id = models.CharField(max_length=50)
    token_id = models.CharField(max_length=50)
    token_validity = models.DateTimeField()
    digio_kyc_url = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ('-created_at',)
        verbose_name_plural = "User KYC Requests"

    def _validate_kyc_status(self):
        if not self.pk:
            if self.user.kyc_submitted:
                raise ValidationError("KYC already submitted")
            elif self.user.kyc_first_time_status == KYCRequestStatusEnum.Approval_Pending.value:
                raise ValidationError("KYC request already submitted, Pending approval")
            elif self.user.kyc_first_time_status == KYCRequestStatusEnum.Approved.value:
                raise ValidationError("KYC request already approved")

    def _generate_digio_kyc_url(self):
        if not self.digio_kyc_url:
            self.digio_kyc_url = f"{settings.DIGIO_MANDATE_BASE_URL}{self.kid}/{generate_random_token()}/" \
                                 f"{self.user.username}?redirect_url={settings.DIGIO_KYC_RETURN_URL}"

    def add_or_update_kyc_detail(self):
        status, data = kyc_request_detail(self.user, self.kid)
        if status:
            if data['status'] in ('approval_pending', 'approved'):
                self.update_kyc_details(data)

    def download_kyc_media(self, doc_type, kyc_obj=None, save=True):
        """
        Returns media in image / pdf format
        doc_type: PAN / AADHAAR / DRIVING_LICENSE
        """
        if not self.rid:
            logger.error(f"Cannot download kyc media {doc_type} as RID is missing")
            return False, ""
        status, media_bytes = kyc_request_media(self.user, self.rid, doc_type)
        if status:
            filename = f'{self.user.username}_{doc_type.lower()}.pdf'
            upfile = upload_file(media_bytes, "pdf", byte_file=True)
            kyc_obj = kyc_obj or self.user.kyc_detail
            if doc_type == "PAN":
                kyc_obj.pan_image.save(filename, upfile, save)
            elif doc_type == "AADHAAR":
                kyc_obj.aadhaar_image.save(filename, upfile, save)
        return status, media_bytes


    def save(self, *args, **kwargs):
        self._validate_kyc_status()
        self._generate_digio_kyc_url()
        super().save(*args, **kwargs)

    def update_kyc_details(self, data):
        # update kyc request obj
        self.status = data['status']
        self.updated_at = str_to_datetime(data['updated_at'])
        self.rid = data['actions'][0]['execution_request_id']
        kyc_det = self.user.get_kyc_detail()

        aadhaar_pan_det = data['actions'][0]['details']

        if "aadhaar" in aadhaar_pan_det:
            # updating aadhaar details
            aadhaar_det = aadhaar_pan_det["aadhaar"]
            kyc_det.aadhaar_number = aadhaar_det['id_number']
            kyc_det.aadhaar_name = aadhaar_det['name']
            kyc_det.aadhaar_photo_image = base64_to_image(aadhaar_det['image'])
            kyc_det.gender = aadhaar_det['gender']
            kyc_det.dob = str_to_date(aadhaar_det['dob'], '%d/%m/%Y')
            kyc_det.current_address = aadhaar_det['current_address']
            kyc_det.current_locality_or_post_office = aadhaar_det['current_address_details']['locality_or_post_office']
            kyc_det.current_district_or_city = aadhaar_det['current_address_details']['district_or_city']
            kyc_det.current_state = aadhaar_det['current_address_details']['state']
            kyc_det.current_pincode = aadhaar_det['current_address_details']['pincode']
            kyc_det.permanent_address = aadhaar_det['permanent_address']
            kyc_det.permanent_locality_or_post_office = aadhaar_det['permanent_address_details']['locality_or_post_office']
            kyc_det.permanent_district_or_city = aadhaar_det['permanent_address_details']['district_or_city']
            kyc_det.permanent_state = aadhaar_det['permanent_address_details']['state']
            kyc_det.permanent_pincode = aadhaar_det['permanent_address_details']['pincode']

        # download aadhaar image
        self.download_kyc_media("AADHAAR", kyc_det, save=False)

        # updating pan details
        if "pan" in aadhaar_pan_det:
            pan_det = aadhaar_pan_det["pan"]
            kyc_det.pan_number = pan_det['id_number']
            kyc_det.pan_name = pan_det['name']
            if pan_det.get('image'):
                kyc_det.pan_photo_image = base64_to_image(pan_det['image'])

        # download pan image
        self.download_kyc_media("PAN", kyc_det, save=False)

        # signature
        sign_det = data['actions'][1]
        if 'file_id' in sign_det:
            status, media_bytes = kyc_request_media(self.user, sign_det['file_id'])
            if status:
                filename = f'{self.user.username}_signature.jpg'
                upfile = upload_file(media_bytes, "jpg", byte_file=True)
                kyc_det.signature.save(filename, upfile, False)

        # photo
        photo_det = data['actions'][2]
        if 'file_id' in photo_det:
            status, media_bytes = kyc_request_media(self.user, photo_det['file_id'])
            if status:
                filename = f'{self.user.username}_photo.jpg'
                upfile = upload_file(media_bytes, "jpg", byte_file=True)
                kyc_det.photo.save(filename, upfile, False)

        kyc_det.save()
        self.save()
        self.user.update_kyc_first_time_status(data['status'])


# ========================================================================
class UserKYCDetail(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="kyc_detail")
    ckyc_number = models.CharField(max_length=30)
    name = models.CharField(max_length=155, blank=True, null=True)
    father_name = models.CharField(max_length=155, blank=True, null=True)
    ckyc_image_type = models.CharField(max_length=10, blank=True, null=True)
    ckyc_photo = models.FileField(storage=UserMediaStorage, blank=True, null=True)
    ckyc_date = models.CharField(max_length=10)
    ckyc_updated_date = models.CharField(max_length=10)
    ckyc_remarks = models.CharField(max_length=100, blank=True, null=True)
    ckyc_prefix = models.CharField(max_length=50, blank=True, null=True)

    aadhaar_number = models.CharField(max_length=12, blank=True, null=True)
    aadhaar_name = models.CharField(max_length=200, blank=True, null=True)
    aadhaar_photo_image = models.FileField(storage=UserMediaStorage, blank=True, null=True)
    aadhaar_image = models.FileField(storage=UserMediaStorage, blank=True, null=True)

    pan_number = models.CharField(max_length=12, blank=True, null=True)
    pan_name = models.CharField(max_length=200, blank=True, null=True)
    pan_photo_image = models.FileField(storage=UserMediaStorage, blank=True, null=True)
    pan_image = models.FileField(storage=UserMediaStorage, blank=True, null=True)

    signature = models.FileField(storage=UserMediaStorage, blank=True, null=True)

    photo = models.FileField(storage=UserMediaStorage, blank=True, null=True)

    gender = models.CharField(choices=GenderEnum.choices(), max_length=1, blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    current_address = models.TextField(blank=True, null=True)
    current_locality_or_post_office = models.CharField(max_length=100, blank=True, null=True)
    current_district_or_city = models.CharField(max_length=100, blank=True, null=True)
    current_state = models.CharField(max_length=50, blank=True, null=True)
    current_pincode = models.CharField(max_length=6, blank=True, null=True)
    permanent_address = models.TextField(blank=True, null=True)
    permanent_locality_or_post_office = models.CharField(max_length=100, blank=True, null=True)
    permanent_district_or_city = models.CharField(max_length=100, blank=True, null=True)
    permanent_state = models.CharField(max_length=50, blank=True, null=True)
    permanent_pincode = models.CharField(max_length=6, blank=True, null=True)

    class Meta:
        verbose_name_plural = "User KYC Details"

    def _validate_unique_pan(self):
        if self.pan_number:
            if UserKYCDetail.objects.filter(pan_number=self.pan_number).exclude(user=self.user).exists():
                raise ValidationError(f"User already registered with PAN: {self.pan_number}.")

    def _update_ckyc_pan_detail(self, data):
        self.ckyc_number = data['ckyc_number']
        self.name = data['name']
        self.father_name = data['fathers_name']
        self.ckyc_image_type = data['image_type']
        filename = f'{self.user.username}_ckyc_photo.pdf'
        upfile = upload_file(data['photo'].encode(), "pdf")
        self.ckyc_photo.save(filename, upfile, False)
        self.ckyc_date = data['kyc_date']
        self.ckyc_updated_date = data['updated_date']
        self.ckyc_remarks = data['remarks']
        self.ckyc_prefix = data['ckyc_prefix']

    def _update_ckyc_download_detail(self, data):
        #logger.info(data)
        det = self.user.kyc_personal_details if hasattr(self.user, 'kyc_personal_details') \
                else UserKYCPersonalDetail(user=self.user)
        for key, value in data["personal_details"].items():
            setattr(det, key, value)
        det.save()

        # merge id_details & images
        id_data = {d['type']: {'id_no': d['id_no']} for d in data['id_details']} # {'PAN': {'id_no': 'abcde1234z'}}
        for i in data['images']:
            id_data.setdefault(i['image_type'], {}).update({'image_type': i['type'], 'data': i['data']})

        if 'PAN' in id_data:
            self.pan_number = id_data['PAN'].get('id_no', "")
            if 'data' in id_data['PAN']:
                filetype = id_data["PAN"]["image_type"].lower()
                filecontent = "application/pdf" if filetype == "pdf" else "image/jpeg"
                filename = f'{self.user.username}_pan.{filetype}'
                upfile = upload_file(id_data['PAN']['data'].encode(), filetype)
                self.pan_image.save(filename, upfile, False)
        if 'Proof of Possession of Aadhaar' in id_data:
            self.aadhaar_number = id_data['Proof of Possession of Aadhaar'].get('id_no', '')
            if 'data' in id_data['Proof of Possession of Aadhaar']:
                filetype = id_data["Proof of Possession of Aadhaar"]["image_type"].lower()
                filecontent = "application/pdf" if filetype == "pdf" else "image/jpeg"
                filename = f'{self.user.username}_aadhaar.{filetype}'
                upfile = upload_file(id_data['Proof of Possession of Aadhaar']['data'].encode(), filetype)
                self.aadhaar_image.save(filename, upfile, False)
        if 'Photograph' in id_data:
            filetype = id_data["Photograph"]["image_type"].lower()
            filecontent = "application/pdf" if filetype == "pdf" else "image/jpeg"
            filename = f'{self.user.username}_photo.{filetype}'
            upfile = upload_file(id_data['Photograph']['data'].encode(), filetype)
            self.photo.save(filename, upfile, False)
        if 'Signature' in id_data:
            filetype = id_data["Signature"]["image_type"].lower()
            filecontent = "application/pdf" if filetype == "pdf" else "image/jpeg"
            filename = f'{self.user.username}_photo.{filetype}'
            upfile = upload_file(id_data['Signature']['data'].encode(), filetype)
            self.signature.save(filename, upfile, False)

    def _download_kyc_data(self):
        if self.pan_number and not self.user.ckyc_data_downloaded:
            logger.info("Checking ckyc status")
            status, context = kra_check_pan_status(self.user, {"id_no": self.pan_number})
            logger.info("CKYC status: {}, context: {}".format(status, context))
            if status:
                if not self.name:
                    self.name = context.get("name", "")
                if context['kra_status'] == "KRA Verified":
                    self._update_ckyc_pan_detail(context)
                self.user.update_pan_kra_status(context['kra_status'])

                if context['kra_status'] == "KRA Verified":
                    logger.info("Downloading CKYC data")
                    status, response = ckyc_data_download(self.user, {"ckyc_no": context["ckyc_number"],
                                                                      "date_of_birth": self.dob.strftime("%d-%m-%Y")})
                    logger.info("CKYC download status: {}".format(status))
                    if status:
                        if response["personal_details"]["gender"] != self.gender:
                            raise ValidationError("Gender doesnt match with your KYC details")
                        self._update_ckyc_download_detail(response)
                        self.user.update_ckyc_data_downloaded(status=True)
                    else:
                        logger.error(response)
                        raise ValidationError(response)
            else:
                logger.error("User not KYC Verified")
                #raise APIException("User not KYC Verified")

    def save(self, *args, **kwargs):
        self._validate_unique_pan()
        self._download_kyc_data()
        super().save(*args, **kwargs)


@receiver(post_save, sender=UserKYCDetail)
def user_pan_submitted(sender, instance, created, **kwargs):
    if instance.pan_number and not instance.user.pan_detail_submitted:
        instance.user.update_pan_detail_submitted(status=True)
    elif not instance.pan_number and instance.user.pan_detail_submitted:
        instance.user.update_pan_detail_submitted(status=False)


@receiver(post_delete, sender=UserKYCDetail)
def user_pan_removed(sender, instance, **kwargs):
    instance.user.update_pan_detail_submitted(status=False, save=False)
    instance.user.update_ckyc_data_downloaded(status=False)


# ========================================================================
class UserKYCPersonalDetail(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="kyc_personal_details")
    type = models.CharField(max_length=50)
    kyc_type = models.CharField(max_length=50)
    ckyc_no = models.CharField(max_length=30)
    prefix = models.CharField(max_length=5, blank=True, null=True)
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    full_name = models.CharField(max_length=155)
    maiden_prefix = models.CharField(max_length=5, blank=True, null=True)
    maiden_first_name = models.CharField(max_length=50, blank=True, null=True)
    maiden_middle_name = models.CharField(max_length=50, blank=True, null=True)
    maiden_last_name = models.CharField(max_length=50, blank=True, null=True)
    maiden_full_name = models.CharField(max_length=155, blank=True, null=True)
    father_prefix = models.CharField(max_length=5, blank=True, null=True)
    father_first_name = models.CharField(max_length=50, blank=True, null=True)
    father_middle_name = models.CharField(max_length=50, blank=True, null=True)
    father_last_name = models.CharField(max_length=50, blank=True, null=True)
    father_full_name = models.CharField(max_length=155, blank=True, null=True)
    mother_prefix = models.CharField(max_length=5, blank=True, null=True)
    mother_first_name = models.CharField(max_length=50, blank=True, null=True)
    mother_middle_name = models.CharField(max_length=50, blank=True, null=True)
    mother_last_name = models.CharField(max_length=50, blank=True, null=True)
    mother_full_name = models.CharField(max_length=155, blank=True, null=True)
    gender = models.CharField(max_length=1)
    dob = models.CharField(max_length=10)
    perm_line1 = models.CharField(max_length=200)
    perm_line2 = models.CharField(max_length=200, blank=True, null=True)
    perm_line3 = models.CharField(max_length=200, blank=True, null=True)
    perm_city  = models.CharField(max_length=50)
    perm_dist  = models.CharField(max_length=50)
    perm_state = models.CharField(max_length=50)
    perm_country = models.CharField(max_length=50)
    perm_pin = models.CharField(max_length=6)
    perm_poa = models.CharField(max_length=100)
    perm_corr_same_flag = models.CharField(max_length=1)
    corr_line1 = models.CharField(max_length=200)
    corr_line2 = models.CharField(max_length=200, blank=True, null=True)
    corr_line3 = models.CharField(max_length=200, blank=True, null=True)
    corr_city  = models.CharField(max_length=50)
    corr_dist  = models.CharField(max_length=50)
    corr_state = models.CharField(max_length=50)
    corr_country = models.CharField(max_length=50)
    corr_pin = models.CharField(max_length=6)
    corr_poa = models.CharField(max_length=100)
    res_std_code = models.CharField(max_length=5, blank=True, null=True)
    res_tele_number = models.CharField(max_length=15, blank=True, null=True)
    off_std_code = models.CharField(max_length=5, blank=True, null=True)
    off_tele_number = models.CharField(max_length=15, blank=True, null=True)
    mob_code = models.CharField(max_length=5, blank=True, null=True)
    mobile_no = models.CharField(max_length=15, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    dec_date = models.CharField(max_length=10)
    dec_place = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = "User KYC Personal Details"

    @property
    def full_name_wo_salutation(self):
        name = self.first_name
        if self.middle_name:
            name += f" {self.middle_name}"
        if self.last_name:
            name += f" {self.last_name}"
        return name

    @property
    def father_full_name_wo_salutation(self):
        name = self.father_first_name
        if self.father_middle_name:
            name += f" {self.father_middle_name}"
        if self.father_last_name:
            name += f" {self.father_last_name}"
        return name

    @property
    def mother_full_name_wo_salutation(self):
        name = self.mother_first_name
        if self.mother_middle_name:
            name += f" {self.mother_middle_name}"
        if self.mother_last_name:
            name += f" {self.mother_last_name}"
        return name

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)


# ========================================================================
class UserCitizenshipDetail(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='citizenship_detail')
    resident_status = models.PositiveSmallIntegerField(choices=ResidentStatusEnum.choices())
    country_of_birth = models.ForeignKey(Country, on_delete=models.PROTECT, related_name='citizens')
    city_of_birth = models.CharField(max_length=100)


@receiver(post_save, sender=UserCitizenshipDetail)
def user_citizenship_submitted(sender, instance, created, **kwargs):
    if created or not instance.user.citizenship_detail_submitted:
        instance.user.update_citizenship_detail_submitted(status=True)


@receiver(post_delete, sender=UserCitizenshipDetail)
def user_citizenship_removed(sender, instance, **kwargs):
    instance.user.update_citizenship_detail_submitted(status=False)


# ========================================================================
class UserFamilyDetail(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='family_detail')
    marital_status = models.PositiveSmallIntegerField(choices=MaritalStatusEnum.choices())
    father_name = models.CharField(max_length=100)
    mother_name = models.CharField(max_length=100)
    nominee_name = models.CharField(max_length=100, blank=True, null=True)
    nominee_relation = models.PositiveSmallIntegerField(choices=NomineeRelationEnum.choices(), blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)


@receiver(post_save, sender=UserFamilyDetail)
def user_family_submitted(sender, instance, created, **kwargs):
    if created or not instance.user.family_detail_submitted:
        instance.user.update_family_detail_submitted(status=True)


@receiver(post_delete, sender=UserFamilyDetail)
def user_family_removed(sender, instance, **kwargs):
    instance.user.update_family_detail_submitted(status=False)


# ========================================================================
class UserProfessionalDetail(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='professional_detail')
    occupation = models.ForeignKey(Occupation, on_delete=models.PROTECT, related_name='users',
                                   limit_choices_to={'is_active': True})
    annual_income = models.ForeignKey(Income, on_delete=models.PROTECT, related_name='users',
                                      limit_choices_to={'is_active': True})
    politically_exposed = models.BooleanField(default=False)


@receiver(post_save, sender=UserProfessionalDetail)
def user_professional_submitted(sender, instance, created, **kwargs):
    if created or not instance.user.professional_detail_submitted:
        instance.user.update_professional_detail_submitted(status=True)


@receiver(post_delete, sender=UserProfessionalDetail)
def user_professional_removed(sender, instance, **kwargs):
    instance.user.update_professional_detail_submitted(status=False)


# ========================================================================
class UserBankDetail(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='bank_details')
    bank = models.ForeignKey(BankBranch, on_delete=models.CASCADE, related_name='users')
    account_holder_name = models.CharField(max_length=100, default='', blank=True)
    account_type = models.CharField(max_length=2, choices=BankAccountTypeEnum.choices(),
                                    default=BankAccountTypeEnum.Saving.value)
    account_number = models.CharField(max_length=18, validators=[MinLengthValidator(8)])
    verified = models.BooleanField(default=False)
    verified_at = models.DateTimeField(blank=True, null=True)
    beneficiary_name = models.CharField(max_length=128, blank=True, null=True)
    is_primary_bank = models.BooleanField(default=False)

    class Meta:
        unique_together = ('user', 'bank', 'account_number')


@receiver(pre_save, sender=UserBankDetail)
def validate_bank_details(sender, instance, update_fields=None, **kwargs):
    if UserBankDetail.objects.filter(user=instance.user, bank=instance.bank, account_number=instance.account_number) \
                             .exclude(id=instance.id).exists():
        raise ValidationError("Bank details already exists")
    if update_fields is None:
        request_data = {
            "beneficiary_account_no": instance.account_number,
            "beneficiary_ifsc": instance.bank.ifsc,
        }
        response = bank_verify(instance.user, request_data)
        if response['status_code'] != 200:
            raise ValidationError(response['error'])
        instance.verified = response['data']['verified']
        if not instance.verified:
            raise ValidationError(response['data']['error_msg'])
        if 'verified_at' in response['data']:
            instance.verified_at = timezone.make_aware(datetime.strptime(response['data']['verified_at'], "%Y-%m-%d %H:%M:%S"))
        if 'beneficiary_name_with_bank' in response['data']:
            instance.beneficiary_name = response['data']['beneficiary_name_with_bank']
    if not instance.pk:
        instance.is_primary_bank = instance.user.bank_details.all().count() == 0


@receiver(post_save, sender=UserBankDetail)
def user_bank_submitted(sender, instance, created, **kwargs):
    if created:
        instance.user.update_bank_detail_submitted(status=True)


@receiver(post_delete, sender=UserBankDetail)
def user_bank_removed(sender, instance, **kwargs):
    if instance.user.bank_details.all().count() == 0:
        instance.user.update_bank_detail_submitted(status=False)


@receiver(post_save, sender=UserBankDetail)
def remove_is_primary_from_other_banks(sender, instance, created, **kwargs):
    if instance.is_primary_bank:
        instance.user.bank_details.exclude(id=instance.id).update(is_primary_bank=False)


class UserEsign(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    request_id = models.CharField(max_length=50, unique=True)
    requested_on = models.DateTimeField(auto_now_add=True)
    esign_template = models.FileField(storage=KYCMediaStorage, blank=True, null=True)
    downloaded_on = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ('-requested_on',)

    def __str__(self):
        return self.request_id
