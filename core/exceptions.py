import logging
logger = logging.getLogger(__name__)

from rest_framework.views import exception_handler


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)
    # Now add the HTTP status code to the response.
    if response is not None:

        status_code = response.status_code

        if 'detail' in response.data:
            response.data['status_message'] = response.data.pop('detail')
        else:
            response_data = response.data
            logger.debug(response_data)
            response.data = {}
            response.data['status_message'] = 'Error'
            if 'non_field_errors' in response_data:
                response.data['errors'] = '|'.join(response_data['non_field_errors']) \
                                                if isinstance(response_data['non_field_errors'], list) \
                                                else response_data['non_field_errors']
                response.data['status_message'] = 'Bad Request'
            elif isinstance(response_data, dict):
                response.data['errors'] = {
                    field: str(error[0]) if isinstance(error, list) and len(error) > 0 else str(error) for
                    field, error in response_data.items()}
                response.data['status_message'] = ' '.join(response.data['errors'].values())
            elif isinstance(response_data, list):
                response.data['errors'] = ' | '.join(response_data)
                response.data['status_message'] = 'Invalid request'
            else:
                response.data['errors'] = response_data
                response.data['status_message'] = response.data['errors']

        response.data['status_code'] = status_code
    logger.error("Error in api: %s" % response.data if response else "")
    return response
