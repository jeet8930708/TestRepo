from storages.backends.s3boto3 import S3Boto3Storage


class UserMediaStorage(S3Boto3Storage):
    bucket_name = 'fikaa-assets'
    location = 'user'


class MediaStorage(S3Boto3Storage):
    bucket_name = 'fikaa-assets'
    location = 'media'

class KYCMediaStorage(S3Boto3Storage):
    bucket_name = 'fikaa-assets'
    location = 'kyc'