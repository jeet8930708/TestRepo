from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import (
    User,
    UserOtp,
    UserKYCRequest,
    UserKYCDetail,
    UserKYCPersonalDetail,
    UserCitizenshipDetail,
    UserFamilyDetail,
    UserProfessionalDetail,
    UserBankDetail,
    UserEsign
)


class UserKYCDetailInline(admin.StackedInline):
    model = UserKYCDetail


class UserCitizenshipDetailInline(admin.TabularInline):
    model = UserCitizenshipDetail
    extra = 0


class UserFamilyDetailInline(admin.TabularInline):
    model = UserFamilyDetail
    extra = 0


class UserProfessionalDetailInline(admin.TabularInline):
    model = UserProfessionalDetail
    raw_id_fields = ('occupation', 'annual_income',)
    extra = 0


class UserBankDetail(admin.TabularInline):
    model = UserBankDetail
    raw_id_fields = ('bank',)
    extra = 0


UserAdmin.list_display = ('email', 'phone', 'first_name', 'last_name', 'referral_code', 'referral_count',
                          'bsestar_client_code', 'account_completed', 'kyc_submitted', 'pan_verified', 'is_active',
                          'is_staff')
UserAdmin.fieldsets += (('Additional Info', {'fields': ('phone', 'bsestar_client_code', 'bsestar_folio_no', 'image', 'referral_code',
                                                        'referrer_code', 'referred_by', 'referral_count',
                                                        'account_completed', 'kyc_submitted', 'pan_verified',
                                                        'pan_kra_status', 'ckyc_data_downloaded',
                                                        'pan_detail_submitted', 'citizenship_detail_submitted',
                                                        'family_detail_submitted', 'professional_detail_submitted',
                                                        'bank_detail_submitted', 'kyc_first_time_status',
                                                        'bsestar_ucc_registered', 'bsestar_fatca_uploaded',
                                                        'bsestar_aof_image_uploaded', 'bsestar_registered',
                                                        'nominee_authentication', 'nominee_authentication_datetime',
                                                        'get_whatsapp_comm', 'agree_terms')}),)
UserAdmin.inlines = [UserKYCDetailInline,
                     UserCitizenshipDetailInline,
                     UserFamilyDetailInline,
                     UserProfessionalDetailInline,
                     UserBankDetail,
                     ]
admin.site.register(User, UserAdmin)


class UserOtpAdmin(admin.ModelAdmin):
    list_display = ('phone', 'otp_generated_on', 'otp_expires_on', 'otp_validated_on')
    search_fields = ['phone']
admin.site.register(UserOtp, UserOtpAdmin)


class UserKYCRequestAdmin(admin.ModelAdmin):
    list_display = ('kid', 'user', 'created_at', 'status')
    search_fields = ('kid', 'user__phone')
    list_filter = ['status']
admin.site.register(UserKYCRequest, UserKYCRequestAdmin)


class UserKYCPersonalDetailAdmin(admin.ModelAdmin):
    list_display = ('ckyc_no', 'user', 'type', 'full_name', 'father_full_name', 'mother_full_name', 'gender', 'dob')
    search_fields = ('ckyc_no', 'father_full_name', 'mother_full_name')
    list_filter = ['gender']
admin.site.register(UserKYCPersonalDetail, UserKYCPersonalDetailAdmin)

class UserEsignAdmin(admin.ModelAdmin):
    list_display = ('request_id', 'user', 'requested_on', 'downloaded_on')
    search_fields = ['request_id', 'user__phone']
admin.site.register(UserEsign, UserEsignAdmin)
