# Create your views here.
import base64
import traceback
import logging

from django.db import transaction, IntegrityError
from django.db.models import Q, ProtectedError
from django.http import HttpResponseRedirect
from django.conf import settings
from django.utils import timezone
from django.core.files.base import ContentFile

from rest_framework.generics import CreateAPIView, UpdateAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework.status import HTTP_201_CREATED
from rest_framework.serializers import ValidationError

from utils import str_to_datetime, str_to_date
from utils.response import (
    response_any,
    response_200,
    response_400_invalid_params,
    response_400_bad_request
)

from master.models import Income, Occupation, Bank, BankBranch
from master.serializers import BankSerializer, BankListSerializer
from api.rest import (
    kra_check_pan_status,
    kyc_request_create,
    kyc_request_detail,
    esign_request,
    esign_download,
)
from core.enums import KYCRequestStatusEnum, UserOTPTypeEnum
from .models import UserOtp, User, UserKYCRequest, UserBankDetail, UserKYCDetail, UserEsign
from .serializers import (
    UserOTPSerializer,
    UserSerializer,
    UserCitizenshipSerializer,
    UserFamilySerializer,
    UserProfessionalSerializer,
    UserBankSerializer,
)

logger = logging.getLogger(__name__)


# ========================================================================
class OTPGenerateView(CreateAPIView):
    """
    Generate OTP
    :param:
    """
    permission_classes = (AllowAny,)
    serializer_class = UserOTPSerializer

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        phone = request.data.get("phone", "")
        response = super().create(request, *args, **kwargs)
        if response.status_code == HTTP_201_CREATED:
            return response_200(f"Otp sent to mobile {phone}")
        return response_any(response.status_code, response.status_text)


# ========================================================================
class OTPValidateView(APIView):
    permission_classes = (AllowAny,)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        phone = request.data.get("phone")
        otp = request.data.get("otp")
        if not phone:
            return response_400_invalid_params("phone is required")
        if not otp:
            return response_400_invalid_params("otp is required")
        valid_otp = UserOtp.objects.validate_otp(phone, otp)
        if not valid_otp:
            return response_400_bad_request("Invalid phone or otp")
        user = User.objects.get_or_create_user(phone=phone, email=f"{phone}@email.com")
        user_data = UserSerializer(instance=user).data
        user_data["token"] = user.refresh_token()
        return response_200("OTP verified successfully", user_data)


# ========================================================================
class UserVerifyReferrerCodeView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            _ = User.objects.get(Q(referral_code=request.data['referrer_code']) & ~(Q(id=request.user.id)))
            return response_200("Valid code")
        except User.DoesNotExist:
            return response_400_bad_request("Invalid code")


# ========================================================================
class UserProfileRetrieveUpdateView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user)
        return response_200("User details fetched successfully", serializer.data)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response_200("Account completed successfully")


# ========================================================================
class KYCRequestCreateView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status, data = kyc_request_create(request.user, {"username": request.user.username})
        if status:
            resp_data = {
                "kid": data["id"],
                "created_at": str_to_datetime(data['created_at']),
                "expire_in_days": int(data["expire_in_days"]),
                "status": data["status"],
                "reference_id": data["reference_id"],
                "token_id": data["access_token"]["id"],
                "token_validity": str_to_datetime(data["access_token"]["valid_till"]),
            }
            user_kyc = UserKYCRequest.objects.create(user=request.user, **resp_data)
            return_data = {
                'redirect_url': user_kyc.digio_kyc_url
            }
            return response_200('KYC request created successfully !', return_data)
        return response_400_bad_request(data)


# ========================================================================
class KYCRequestResponseView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        # http://localhost:8000/auth/kyc/request/response/?status=success&digio_doc_id=KID220916230246060ZHAHG7OGQZ154V&message=KYC%20Process%20Completed.
        params = request.query_params
        if params['status'] == 'success':
            try:
                kyc_request_obj = UserKYCRequest.objects.get(kid=params['digio_doc_id'])
                status, data = kyc_request_detail(kyc_request_obj.user, kyc_request_obj.kid)
                # logger.info(data)
                if status:
                    if kyc_request_obj.user.username != data['customer_identifier']:
                        return response_400_bad_request('Invalid request')
                    if data['status'] == KYCRequestStatusEnum.Requested.value:
                        return response_400_bad_request('KYC not completed')
                    if data['status'] == KYCRequestStatusEnum.Approval_Pending.value:
                        kyc_request_obj.update_kyc_details(data)
            except UserKYCRequest.DoesNotExist:
                return HttpResponseRedirect(settings.DIGIO_KYC_REDIRECT_URL + f'?status=failed&error=Invalid digio doc id')
            return HttpResponseRedirect(settings.DIGIO_KYC_REDIRECT_URL + f'?status=success')
        return HttpResponseRedirect(settings.DIGIO_KYC_REDIRECT_URL + f'?status=failed&error={params["message"]}')


# ========================================================================
class KYCPanVerifyView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        if UserKYCDetail.objects.filter(pan_number=request.data['id_no']).exclude(user=request.user).exists():
            raise ValidationError("User already registered with this PAN number")
        status, context = kra_check_pan_status(request.user, request.data)
        if status is None:
            return response_400_bad_request(context)
        verified = 'Verified' if status else 'Not Verified'
        request.user.update_pan_kra_status(context.get('kra_status') if status else context)
        if status:
            name = context["name"]
            if name.lower().startswith(('mr', 'mrs', 'miss', 'mr')):
                name = name.split(' ', 1)[1]
            return response_200(verified, {"name": name.strip()} if status else {})
        return response_200(verified, {'status': context})


# ========================================================================
class KYCPanRetrieveUpdateView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        kyc_det = request.user.get_kyc_detail()
        if not kyc_det.pan_number:
            return response_400_bad_request("KYC Pan details not found")
        data = {
            'id_number': kyc_det.pan_number,
            'name': kyc_det.name,
            'gender': kyc_det.gender,
            'dob': kyc_det.dob.strftime('%Y-%m-%d') if kyc_det.dob else None
        }
        return response_200("KYC Pan details fetched successfully", data)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        kyc_det = request.user.get_kyc_detail()
        data = request.data
        kyc_det.pan_number = data['id_number']
        kyc_det.name = data['registered_name'] if 'registered_name' in data else data.get('name', '')
        kyc_det.gender = data['gender']
        kyc_det.dob = str_to_date(data['dob'], '%d-%m-%Y')
        kyc_det.save()
        return response_200("KYC Pan details updated successfully", request.user.personal_details)


# ========================================================================
class UserCitizenRetrieveUpdateView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserCitizenshipSerializer

    def get(self, request, *args, **kwargs):
        if not hasattr(request.user, 'citizenship_detail'):
            return response_400_bad_request("Citizenship detail does not exist")
        serializer = self.serializer_class(request.user.citizenship_detail)
        return response_200("User citizenship details fetched successfully", serializer.data)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        instance = request.user.citizenship_detail if hasattr(request.user, 'citizenship_detail') else None
        serializer = self.serializer_class(instance, data=request.data, partial=True,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response_200("User citizenship details updated successfully")


# ========================================================================
class UserFamilyRetrieveUpdateView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserFamilySerializer

    def get(self, request, *args, **kwargs):
        if not hasattr(request.user, 'family_detail'):
            return response_400_bad_request("Family detail does not exist")
        serializer = self.serializer_class(request.user.family_detail)
        return response_200("User family details fetched successfully", serializer.data)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        instance = request.user.family_detail if hasattr(request.user, 'family_detail') else None
        serializer = self.serializer_class(instance, data=request.data, partial=True,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response_200("User family details updated successfully")


# ========================================================================
class UserProfessionalParamsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data = {
            'occupation': [(i.id, i.name) for i in Occupation.objects.filter(is_active=True)],
            'income': [(i.id, i.bucket) for i in Income.objects.filter(is_active=True)],
        }
        return response_200("Params fetched successfully", data)


# ========================================================================
class UserProfessionalRetrieveUpdateView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserProfessionalSerializer

    def get(self, request, *args, **kwargs):
        if not hasattr(request.user, 'professional_detail'):
            return response_400_bad_request("Professional detail does not exist")
        serializer = self.serializer_class(request.user.professional_detail)
        return response_200("User professional details fetched successfully", serializer.data)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        instance = request.user.professional_detail if hasattr(request.user, 'professional_detail') else None
        serializer = self.serializer_class(instance, data=request.data, partial=True,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return response_200("User professional details updated successfully")


# ========================================================================
class IFSCCodeSearchView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        try:
            bank_branch = BankBranch.objects.get(ifsc=request.data['ifsc'])
        except BankBranch.DoesNotExist:
            return response_400_bad_request("Invalid IFSC")
        return response_200("IFSC details fetched successfully",
                            data=BankSerializer(instance=bank_branch).data)


# ========================================================================
class IFSCSearchParamsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return response_200("Params fetched successfully",
                            data={
                                'banks': BankListSerializer(instance=Bank.objects.filter(is_active=True),
                                                            many=True).data
                            })


# ========================================================================
class IFSCCodeListView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data = request.GET
        bank_name = data.get('bank_name', '')
        bank_city = data.get('city', '')
        bank_branch = data.get('branch_name', '')

        banks = BankBranch.objects.filter(bank__name=bank_name, bank__is_active=True)
        if bank_city:
            banks = banks.filter(city__icontains=bank_city)
        if bank_branch:
            banks = banks.filter(branch__icontains=bank_branch)
        banks = banks.values('ifsc', 'branch', 'centre', 'city', 'district', 'state', 'address').order_by('ifsc')
        return response_200("Bank branches fetched successfully", data={'ifsc_codes': banks})


# ========================================================================
class UserBankListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserBankSerializer
    queryset = UserBankDetail.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def create(self, request, *args, **kwargs):
        try:
            response = super().create(request, *args, **kwargs)
        except IntegrityError as e:
            return response_400_bad_request("Bank details already exist")

        return response_200("User bank details added successfully", response.data)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("User bank details list fetched successfully", response.data)


# ========================================================================
class UserBankRetrieveUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserBankSerializer
    queryset = UserBankDetail.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("User bank details fetched successfully", response.data)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        return response_200("User bank details updated successfully", response.data)

    def destroy(self, request, *args, **kwargs):
        try:
            response = super().destroy(request, *args, **kwargs)
        except ProtectedError:
            return response_400_bad_request("Operation not allowed. Mandate registered for this bank.")
        return response_200("User bank details deleted successfully", response.data)


class UserBankPrimaryView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = UserBankDetail.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def update(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.is_primary_bank = kwargs.get('is_primary_bank', False)
        obj.save(update_fields=['is_primary_bank'])
        return response_200("User bank details updated successfully")


# ========================================================================
class UserNomineeOTPGenerateView(CreateAPIView):
    """
    Generate OTP
    :param:
    """
    permission_classes = (IsAuthenticated,)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        try:
            _ = UserOtp.objects.create(phone=request.user.phone,
                                       otp_type=UserOTPTypeEnum.NOMINEE_AUTHENTICATION.value)
            family_det = request.user.family_details
            if family_det["nominee_name"]:
                response = f"Please enter the code you have received to confirm that you have added " \
                           f"{family_det['nominee_name']} as nominee(s) to your account"
            else:
                response = "Please enter the code you have received to confirm that you have not added " \
                           "nominee(s) to your account"
            return response_200(response)
        except Exception as e:
            traceback.print_exc()
            logger.error(e)
            return response_400_bad_request("Error while generating OTP. Please try after sometime.")


# ========================================================================
class UserNomineeOTPValidateView(APIView):
    permission_classes = (IsAuthenticated,)

    @transaction.atomic
    def post(self, request, *args, **kwargs):
        phone = request.user.phone
        otp = request.data.get("otp")
        if not phone:
            return response_400_invalid_params("phone is required")
        if not otp:
            return response_400_invalid_params("otp is required")
        valid_otp = UserOtp.objects.validate_otp(phone, otp, UserOTPTypeEnum.NOMINEE_AUTHENTICATION.value)
        if not valid_otp:
            return response_400_bad_request("Invalid phone or otp")
        request.user.update_nominee_authetication()
        return response_200("OTP verified successfully")


class KYCEsignSubmitView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        status, resp = esign_request(request.user)
        if status:
            from utils.order_util import generate_random_token
            UserEsign.objects.create(user=request.user, request_id=resp['id'])
            redirect_url = f"{settings.DIGIO_ESIGN_BASE_URL}/#/gateway/login/{resp['id']}/{generate_random_token()}/" \
                           f"{request.user.phone}?redirect_url={settings.DIGIO_ESIGN_RETURN_URL}"
            return response_200("Esign url", {"redirect_url": redirect_url})
        return response_400_bad_request(resp)


class KYCEsignStatusView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        if request.GET['status'] == "success":
            try:
                esign = UserEsign.objects.get(request_id=request.GET['digio_doc_id'])
                status, data = esign_download(esign.user, request.GET['digio_doc_id'])
                if status:
                    file_data = ContentFile(data)
                    esign.esign_template.save(f"{esign.user.phone}_{request.GET['digio_doc_id']}.pdf", file_data, False)
                    esign.downloaded_on = timezone.now()
                    esign.save()
                else:
                    return HttpResponseRedirect(
                        settings.DIGIO_KYC_REDIRECT_URL + f"?status=failed&message={data}")
            except UserEsign.DoesNotExist:
                return HttpResponseRedirect(settings.DIGIO_KYC_REDIRECT_URL + f"?status=failed&message=invalid request")

        return HttpResponseRedirect(settings.DIGIO_KYC_REDIRECT_URL+f"?status={request.GET['status']}&message={request.GET['message']}")
