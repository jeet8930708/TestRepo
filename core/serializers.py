from rest_framework.serializers import (
    CharField,
    ModelSerializer,
    ValidationError,
)
from drf_extra_fields.fields import Base64ImageField
from master.models import BankBranch
from master.serializers import BankSerializer

from .models import (
    UserOtp,
    User,
    UserCitizenshipDetail,
    UserFamilyDetail,
    UserProfessionalDetail,
    UserBankDetail,
)


# ========================================================================
class UserOTPSerializer(ModelSerializer):

    class Meta:
        model = UserOtp
        fields = ['phone', 'otp_type']


# ========================================================================
class UserSerializer(ModelSerializer):

    fullname = CharField(max_length=100, required=True, write_only=True)
    image = Base64ImageField()
    name = CharField(source='get_full_name', read_only=True)
    referred_by = CharField(source='referred_by.get_full_name', read_only=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'phone', 'name', 'fullname', 'image', 'age_range',
                  'get_whatsapp_comm', 'agree_terms',
                  'referral_code', 'referrer_code', 'referred_by', 'referral_count', 'account_completed',
                  'pan_verified', 'pan_kra_status', 'ckyc_data_downloaded',
                  'kyc_first_time_submitted', 'kyc_first_time_approved', 'kyc_first_time_status',
                  'pan_detail_submitted', 'citizenship_detail_submitted', 'family_detail_submitted',
                  'professional_detail_submitted', 'bank_detail_submitted', 'kyc_submitted', 'nominee_authentication',
                  'personal_details']
        read_only_fields = ['id', 'phone', 'referral_code', 'referral_count', 'account_completed',
                            'pan_detail_submitted', 'citizenship_detail_submitted',
                            'family_detail_submitted', 'professional_detail_submitted', 'bank_detail_submitted',
                            'kyc_submitted', 'nominee_authentication', 'pan_verified', 'personal_details']

    def update(self, instance, validated_data):
        if 'fullname' in validated_data:
            fullname = validated_data['fullname']
            fullname = fullname.split(' ', 1)
            validated_data['first_name'] = fullname[0]
            validated_data['last_name'] = fullname[1] if len(fullname) == 2 else ''
        return super().update(instance, validated_data)


# ========================================================================
class UserCitizenshipSerializer(ModelSerializer):

    class Meta:
        model = UserCitizenshipDetail
        fields = ['resident_status', 'country_of_birth', 'city_of_birth']

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return super().create(validated_data)


# ========================================================================
class UserFamilySerializer(ModelSerializer):

    class Meta:
        model = UserFamilyDetail
        fields = ['marital_status', 'father_name', 'mother_name', 'nominee_name', 'nominee_relation']

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return super().create(validated_data)


# ========================================================================
class UserProfessionalSerializer(ModelSerializer):

    class Meta:
        model = UserProfessionalDetail
        fields = ['occupation', 'annual_income', 'politically_exposed']

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return super().create(validated_data)


# ========================================================================
class UserBankSerializer(ModelSerializer):
    ifsc_code = CharField(max_length=20, write_only=True)
    bank = BankSerializer(read_only=True)

    class Meta:
        model = UserBankDetail
        fields = ['id', 'ifsc_code', 'account_holder_name', 'account_type', 'account_number', 'bank', 'is_primary_bank']
        read_only_fields = ('id',)

    def create(self, validated_data):
        request = self.context['request']
        ifsc = validated_data.pop('ifsc_code')
        validated_data['user'] = request.user
        try:
            validated_data['bank'] = BankBranch.objects.get(ifsc=ifsc)
        except BankBranch.DoesNotExist:
            raise ValidationError('Invalid IFSC Code')
        return super().create(validated_data)
