# Generated by Django 4.0.2 on 2022-09-16 11:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0038_user_bsestar_client_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='userkycrequest',
            name='digio_kyc_url',
            field=models.TextField(blank=True, null=True),
        ),
    ]
