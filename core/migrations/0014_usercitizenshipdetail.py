# Generated by Django 4.0.2 on 2022-04-28 06:40

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('master', '0002_country'),
        ('core', '0013_userkyc_ckyc_number_alter_userkyc_kyc_request'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserCitizenshipDetail',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('resident_status', models.PositiveSmallIntegerField(choices=[(1, 'Indian'), (2, 'Non_Resident_Indian')])),
                ('city_of_birth', models.CharField(max_length=100)),
                ('country_of_birth', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='citizens', to='master.country')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='citizenship_detail', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
