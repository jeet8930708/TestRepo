# Generated by Django 4.0.2 on 2022-07-28 04:51

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0031_alter_userkyc_options_alter_userkycdetail_options_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserKYCProcessLog',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('step', models.PositiveSmallIntegerField(choices=[(1, 'UCC_REGISTRATION'), (2, 'FATCA_UPLOAD'), (3, 'AOF_IMAGE_UPLOAD')])),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'Started'), (2, 'Completed'), (3, 'Failed')])),
                ('remark', models.TextField(blank=True, null=True)),
                ('timestamp', models.DateField(auto_now_add=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
