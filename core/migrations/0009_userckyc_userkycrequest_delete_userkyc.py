# Generated by Django 4.0.2 on 2022-04-09 12:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_userbankdetail_beneficiary_name_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserCKYC',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pan_number', models.CharField(error_messages={'unique': 'A user with that PAN number already exists.'}, max_length=128, unique=True)),
                ('ckyc_number', models.CharField(max_length=100)),
                ('gender', models.PositiveSmallIntegerField(choices=[(1, 'Male'), (2, 'Female'), (3, 'Other')])),
                ('registered_name', models.CharField(max_length=100)),
                ('age', models.PositiveSmallIntegerField()),
                ('image_type', models.CharField(max_length=10)),
                ('photo', models.BinaryField()),
                ('kyc_date', models.DateField()),
                ('updated_date', models.DateField()),
                ('remarks', models.TextField(blank=True, null=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='kyc', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserKYCRequest',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('workflow_name', models.CharField(max_length=50)),
                ('kid', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField()),
                ('expire_in_days', models.PositiveSmallIntegerField()),
                ('status', models.CharField(choices=[('requested', 'Requested'), ('approval_pending', 'Approval_Pending')], max_length=20)),
                ('reference_id', models.CharField(max_length=50)),
                ('token_id', models.CharField(max_length=50)),
                ('token_validity', models.DateTimeField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='kyc_history', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.DeleteModel(
            name='UserKYC',
        ),
    ]
