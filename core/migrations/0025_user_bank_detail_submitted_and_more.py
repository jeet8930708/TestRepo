# Generated by Django 4.0.2 on 2022-07-08 15:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0024_user_referral_count'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='bank_detail_submitted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='citizenship_detail_submitted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='family_detail_submitted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='kyc_submitted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='kyc_verified',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='pan_detail_submitted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='user',
            name='professional_detail_submitted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='userbankdetail',
            name='is_primary_bank',
            field=models.BooleanField(default=False),
        ),
    ]
