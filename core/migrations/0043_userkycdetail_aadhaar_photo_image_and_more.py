# Generated by Django 4.0.2 on 2022-09-16 17:59

import core.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0042_alter_userkyc_file_alter_userkycdetail_aadhaar_image_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='userkycdetail',
            name='aadhaar_photo_image',
            field=models.ImageField(blank=True, null=True, storage=core.storage.UserMediaStorage, upload_to=''),
        ),
        migrations.AddField(
            model_name='userkycdetail',
            name='pan_photo_image',
            field=models.ImageField(blank=True, null=True, storage=core.storage.UserMediaStorage, upload_to=''),
        ),
    ]
