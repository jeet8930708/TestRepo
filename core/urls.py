from django.urls import path

from rest_framework_simplejwt.views import (
    TokenRefreshView,
)

from .enums import UserOTPTypeEnum

from .views import (
    OTPGenerateView,
    OTPValidateView,
    UserVerifyReferrerCodeView,
    UserProfileRetrieveUpdateView,

    KYCRequestCreateView,
    KYCRequestResponseView,
    KYCPanVerifyView,
    KYCPanRetrieveUpdateView,
    KYCEsignSubmitView,
    KYCEsignStatusView,
    UserCitizenRetrieveUpdateView,
    UserFamilyRetrieveUpdateView,
    UserProfessionalParamsView,
    UserProfessionalRetrieveUpdateView,

    IFSCSearchParamsView,
    IFSCCodeSearchView,
    IFSCCodeListView,
    UserBankListCreateView,
    UserBankRetrieveUpdateDeleteView,
    UserBankPrimaryView,

    UserNomineeOTPGenerateView,
    UserNomineeOTPValidateView
)


urlpatterns = [
    path('token-refresh/', TokenRefreshView.as_view(), name='token-refresh'),
    path('otp-generate/', OTPGenerateView.as_view(), name='otp-generate'),
    path('otp-validate/', OTPValidateView.as_view(), name='otp-validate'),

    path('user-profile/verify-referrer-code/', UserVerifyReferrerCodeView.as_view(), name='user-verify_referrer_code'),
    path('user-profile/update/', UserProfileRetrieveUpdateView.as_view(), name='user-profile-update'),
    path('user-profile/retrieve/', UserProfileRetrieveUpdateView.as_view(), name='user-retrieve'),

    path('kyc/request/create/', KYCRequestCreateView.as_view(), name='kyc-request-create'),
    path('kyc/request/response/', KYCRequestResponseView.as_view(), name='kyc-request-response'),
    path('kyc/pan/verify/', KYCPanVerifyView.as_view(), name='kyc-pan-verify'),
    path('kyc/pan/update/', KYCPanRetrieveUpdateView.as_view(), name='kyc-pan-update'),
    path('kyc/pan/retrieve/', KYCPanRetrieveUpdateView.as_view(), name='kyc-pan-retrieve'),
    path('kyc/esign/submit/', KYCEsignSubmitView.as_view(), name='kyc-esign-submit'),
    path('kyc/esign/status/', KYCEsignStatusView.as_view(), name='kyc-esign-status'),

    path('user-citizenship-detail/update/', UserCitizenRetrieveUpdateView.as_view(), name='user-citizenship-detail-update'),
    path('user-citizenship-detail/retrieve/', UserCitizenRetrieveUpdateView.as_view(), name='user-citizenship-detail-retrieve'),

    path('user-family-detail/update/', UserFamilyRetrieveUpdateView.as_view(), name='user-family-detail-update'),
    path('user-family-detail/retrieve/', UserFamilyRetrieveUpdateView.as_view(), name='user-family-detail-retrieve'),

    path('user-professional-detail/params/', UserProfessionalParamsView.as_view(),
         name='user-professional-detail-params'),
    path('user-professional-detail/update/', UserProfessionalRetrieveUpdateView.as_view(),
         name='user-professional-detail-update'),
    path('user-professional-detail/retrieve/', UserProfessionalRetrieveUpdateView.as_view(),
         name='user-professional-detail-retrieve'),

    path('user-bank-detail/ifsc/search/params/', IFSCSearchParamsView.as_view(), name='user-bank-detail-ifsc-params'),
    path('user-bank-detail/ifsc/code/search/', IFSCCodeSearchView.as_view(), name='user-bank-detail-ifsc-search'),
    path('user-bank-detail/ifsc/code/list/', IFSCCodeListView.as_view(), name='user-bank-detail-ifsc-list'),
    path('user-bank-detail/add/', UserBankListCreateView.as_view(), name='user-bank-detail-create'),
    path('user-bank-detail/list/', UserBankListCreateView.as_view(), name='user-bank-detail-list'),
    path('user-bank-detail/retrieve/<int:pk>/', UserBankRetrieveUpdateDeleteView.as_view(), name='user-bank-detail-retrieve'),
    path('user-bank-detail/update/<int:pk>/', UserBankRetrieveUpdateDeleteView.as_view(), name='user-bank-detail-update'),
    path('user-bank-detail/delete/<int:pk>/', UserBankRetrieveUpdateDeleteView.as_view(), name='user-bank-detail-delete'),
    path('user-bank-detail/make-primary/<int:pk>/', UserBankPrimaryView.as_view(), {'is_primary_bank': True},
         name='user-bank-detail-make-primary'),
    path('user-bank-detail/remove-primary/<int:pk>/', UserBankPrimaryView.as_view(), {'is_primary_bank': False},
         name='user-bank-detail-remove-primary'),

    path('user-nominee-authentication/otp-generate/', UserNomineeOTPGenerateView.as_view(),
         name='user-nominee-otp-generate'),
    path('user-nominee-authentication/otp-validate/', UserNomineeOTPValidateView.as_view(),
         name='user-nominee-otp-validate'),
]
