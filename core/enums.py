from enum import Enum


class ChoiceEnum(Enum):

    @classmethod
    def choices(cls):
        return tuple((i.value, i.name) for i in cls)

    @classmethod
    def help(cls):
        return ", ".join([f"{i.name} -> {i.value}" for i in cls])

    @classmethod
    def values(cls):
        return [i.value for i in cls]


class UserOTPTypeEnum(ChoiceEnum):
    LOGIN = 1
    NOMINEE_AUTHENTICATION = 2


class AgeRangeEnum(ChoiceEnum):
    Age_Below_25 = 1
    Age_25_to_35 = 2
    Age_35_to_55 = 3
    Age_55_Above = 4


class GenderEnum(ChoiceEnum):
    Male = "M"
    Female = "F"
    Other = "O"


class MaritalStatusEnum(ChoiceEnum):
    Married = 1
    Unmarried = 2
    Widowed = 3


class BankAccountTypeEnum(ChoiceEnum):
    Saving = 'SB'
    Current = 'CB'
    NRE = 'NE'
    NRO = 'NO'


class KYCRequestStatusEnum(ChoiceEnum):
    Requested = 'requested'
    Approval_Pending = 'approval_pending'
    Approved = 'approved'
    Rejected = 'rejected'


class KYCDocumentTypeEnum(ChoiceEnum):
    Aadhaar = 'aadhaar'
    Pan = 'pan'


class KYCProofTypeEnum(ChoiceEnum):
    id_and_address_proof = "ID_AND_ADDRESS_PROOF"
    id_proof = "ID_PROOF"


class ResidentStatusEnum(ChoiceEnum):
    Indian = 1
    Non_Resident_Indian = 2


class NomineeRelationEnum(ChoiceEnum):
    Father = 1
    Mother = 2
    Son = 3
    Daughter = 4
    Spouse = 5
    LegalGuardian = 6


class KYCProcessStepEnum(ChoiceEnum):
    UCC_REGISTRATION = 1
    FATCA_UPLOAD = 2
    AOF_IMAGE_UPLOAD = 3


class KYCProcessStatusEnum(ChoiceEnum):
    Started = 1
    Completed = 2
    Failed = 3
