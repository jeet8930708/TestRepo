import logging
from time import time

logger = logging.getLogger(__name__)


def timeit(func):
    # This function shows the execution time of
    # the function object passed
    def wrap_func(*args, **kwargs):
        logger.info(f'Function {func.__name__!r} execution starts with args:{args} & kwargs:{kwargs}')
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        logger.info(f'Function {func.__name__!r} executed in {(t2 - t1):.4f}s')
        return result
    return wrap_func
