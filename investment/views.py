import logging
import traceback

from django.conf import settings

from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.status import HTTP_400_BAD_REQUEST

from scripts.recommendation_engine import run
from utils.response import response_any, response_200
from utils.investment_util import get_user_investments

from .models import Instrument
from .serializers import InstrumentSerializer

logger = logging.getLogger(__name__)


class RecommendationView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        age = int(request.GET['age'])
        risk_profile = int(request.GET['risk_profile'])
        inv_interval = int(request.GET['investment_interval'])
        inv_amount = float(request.GET['investment_amount'])

        try:
            result = run(settings.MUTUAL_FUNDS_COUNT, age, risk_profile, inv_interval, inv_amount)
            return response_200("Recommendations fetched successfully", result)
        except Exception as e:
            traceback.print_exc()
            logger.error(str(e))
            return response_any(HTTP_400_BAD_REQUEST, "Error in getting recommendations", str(e))


class InstrumentRetrieveView(RetrieveAPIView):
    permission_classes = (AllowAny,)
    lookup_field = 'code'
    serializer_class = InstrumentSerializer
    queryset = Instrument.objects.filter(is_active=True)


class MyInvestmentView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return response_200("My investments fetched successfully",
                            get_user_investments(request.user, self.kwargs.get('summary', False)))
