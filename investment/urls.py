from django.urls import path

from .views import (
    InstrumentRetrieveView,
    MyInvestmentView,
)

urlpatterns = [
    path('mutual-fund/detail/<str:code>/', InstrumentRetrieveView.as_view(), name='instrument-retrieve'),
    path('my-investments/summary/', MyInvestmentView.as_view(), {'summary': True}, name='my-investments-summary'),
    path('my-investments/detail/', MyInvestmentView.as_view(), {'summary': False}, name='my-investments-detail'),
]