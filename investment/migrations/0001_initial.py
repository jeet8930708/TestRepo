# Generated by Django 4.0.2 on 2022-04-19 19:07

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='InstrumentType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.CreateModel(
            name='Instrument',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('code', models.CharField(max_length=50, unique=True)),
                ('return_1y', models.FloatField(default=0.0)),
                ('return_3y', models.FloatField(default=0.0)),
                ('return_5y', models.FloatField(default=0.0)),
                ('expected_return', models.FloatField(default=0.0)),
                ('max_monthly_drawdown', models.FloatField(default=0.0)),
                ('min_investment_amount', models.FloatField(default=1000, validators=[django.core.validators.MinValueValidator(100)])),
                ('is_active', models.BooleanField(default=True)),
                ('instrument_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='instruments', to='investment.instrumenttype')),
            ],
            options={
                'ordering': ('name',),
                'unique_together': {('name', 'instrument_type')},
            },
        ),
        migrations.CreateModel(
            name='InstrumentMonthlyReturn',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('month', models.PositiveSmallIntegerField(choices=[(1, 'January'), (2, 'February'), (3, 'March'), (4, 'April'), (5, 'May'), (6, 'June'), (7, 'July'), (8, 'August'), (9, 'September'), (10, 'October'), (11, 'November'), (12, 'December')])),
                ('year', models.PositiveSmallIntegerField(choices=[(2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015), (2016, 2016), (2017, 2017), (2018, 2018), (2019, 2019), (2020, 2020), (2021, 2021), (2022, 2022), (2023, 2023), (2024, 2024)])),
                ('return_percent', models.FloatField(default=0.0)),
                ('instrument', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='monthly_returns', to='investment.instrument')),
            ],
            options={
                'ordering': ('year', 'month'),
                'unique_together': {('instrument', 'month', 'year')},
            },
        ),
    ]
