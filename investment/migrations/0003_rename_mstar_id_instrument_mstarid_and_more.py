# Generated by Django 4.0.2 on 2022-05-28 11:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('investment', '0002_instrument_isin_instrument_mstar_id_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='instrument',
            old_name='mstar_id',
            new_name='mstarid',
        ),
        migrations.RenameField(
            model_name='instrumentfundmanager',
            old_name='manager_id',
            new_name='managerid',
        ),
    ]
