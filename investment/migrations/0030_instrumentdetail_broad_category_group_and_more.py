# Generated by Django 4.0.2 on 2022-10-05 12:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('investment', '0029_rename_gross_expense_ratio_instrumentdetail_net_expense_ratio_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='instrumentdetail',
            name='broad_category_group',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='instrumentdetail',
            name='investment_philosophy',
            field=models.TextField(blank=True, null=True),
        ),
    ]
