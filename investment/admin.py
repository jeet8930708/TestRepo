from django.contrib import admin

from .models import (
    InstrumentType,
    Instrument,
    InstrumentMonthlyReturn,
    InstrumentDetail,
    InstrumentFundManager,
    InstrumentNAVDetail,
)


class InstrumentTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_active']
    list_filter = ['is_active']
admin.site.register(InstrumentType, InstrumentTypeAdmin)


class InstrumentMonthlyReturnAdmin(admin.ModelAdmin):
    list_display = ['instrument', 'month', 'year', 'return_percent']
    list_filter = ['month', 'year']
    search_fields = ['instrument__name', 'instrument__mstarid', 'instrument__isin']
admin.site.register(InstrumentMonthlyReturn, InstrumentMonthlyReturnAdmin)


class InstrumentDetailInline(admin.StackedInline):
    model = InstrumentDetail


class InstrumentFundManagerInline(admin.StackedInline):
    model = InstrumentFundManager
    extra = 0


class InstrumentAdmin(admin.ModelAdmin):
    list_display = ['name', 'isin', 'mstarid', 'bsestar_scheme_code',
                    'current_nav_value', 'current_nav_date',
                    'sip_weekly_allowed', 'sip_monthly_allowed',
                    'sip_weekly_dates', 'sip_monthly_dates',
                    'is_active']
    list_filter = ['instrument_type', 'sip_weekly_allowed', 'sip_monthly_allowed', 'is_active']
    search_fields = ['name', 'isin', 'mstarid', 'bsestar_scheme_code']
    inlines = [InstrumentDetailInline, InstrumentFundManagerInline]
admin.site.register(Instrument, InstrumentAdmin)


class InstrumentNAVDetailAdmin(admin.ModelAdmin):
    list_display = ['instrument', 'nav_date', 'nav_value']
    search_fields = ['instrument__name', 'instrument__mstarid', 'instrument__isin']
admin.site.register(InstrumentNAVDetail, InstrumentNAVDetailAdmin)