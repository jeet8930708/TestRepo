import datetime
import logging
import pandas as pd

from django.db import models
from django.conf import settings
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.text import slugify

from api.rest import mutual_fund_detail, nav_history, nav_price
from utils import date_range
from .storage import InstrumentMediaStorage

logger = logging.getLogger('api')


class InstrumentType(models.Model):
    name = models.CharField(max_length=50, unique=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Instrument(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100, unique=True)
    isin = models.CharField(max_length=12)
    mstarid = models.CharField(max_length=10, blank=True, null=True)
    bsestar_scheme_code = models.CharField(max_length=20, blank=True, null=True)
    instrument_type = models.ForeignKey(InstrumentType, on_delete=models.CASCADE, related_name='instruments')
    logo = models.ImageField(storage=InstrumentMediaStorage, blank=True, null=True)
    return_1y = models.FloatField(default=0.0)
    return_3y = models.FloatField(default=0.0)
    return_5y = models.FloatField(default=0.0)
    expected_return = models.FloatField(default=0.0)
    max_monthly_drawdown = models.FloatField(default=0.0)
    current_nav_value = models.FloatField(default=0.0)
    current_nav_date = models.DateField(blank=True, null=True)
    current_nav_change = models.FloatField(default=0.0)
    sip_weekly_allowed = models.BooleanField(default=True)
    sip_monthly_allowed = models.BooleanField(default=True)
    sip_weekly_dates = models.CharField(max_length=100, blank=True, null=True)
    sip_monthly_dates = models.CharField(max_length=100, blank=True, null=True)
    sip_max_gap = models.PositiveSmallIntegerField(default=10)
    sip_min_installment_amount = models.PositiveBigIntegerField(default=100)
    sip_max_installment_amount = models.PositiveBigIntegerField(default=999999999)
    sip_multiplier_amount = models.PositiveSmallIntegerField(default=1)
    sip_min_installment_number = models.PositiveSmallIntegerField(default=1)
    sip_max_installment_number = models.PositiveIntegerField(default=50)
    lumpsum_min_investment_amount = models.PositiveBigIntegerField(default=5000)
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = ('name', 'instrument_type')
        ordering = ('name',)

    def __str__(self):
        return self.name

    @property
    def legal_name(self):
        return self.detail.legal_name if hasattr(self, 'detail') else self.legal_name

    @property
    def sip_monthly_valid_start_dates(self):
        dates = date_range(timezone.now(), self.sip_max_gap)
        sip_dates = self.sip_monthly_dates.split(',')
        valid_dates = [d[1].strftime('%d-%m-%Y') for d in dates if str(d[0]) in sip_dates]
        return valid_dates

    @property
    def sip_weekly_valid_start_dates(self):
        dates = date_range(timezone.now(), self.sip_max_gap)
        sip_dates = self.sip_weekly_dates.split(',')
        valid_dates = [d[1].strftime('%d-%m-%Y') for d in dates if str(d[0]) in sip_dates]
        return valid_dates

    @property
    def nav_details_index(self):
        from utils.investment_util import fetch_index_nav_history
        return fetch_index_nav_history()

    @property
    def growth_10k(self):
        from utils.investment_util import fetch_growth_10k
        return fetch_growth_10k(self.mstarid)

    def fetch_detail_morningstar(self):
        logger.info(f"Fetching mutual fund details from morning star for {self.name} - {self.isin}")
        status, data = mutual_fund_detail(self.isin)
        if status == 'Success':
            # updating mstarid
            self.mstarid = data['FSCBI-MStarID']
            self.name = data['FSCBI-LegalName']
            self.save()

            if hasattr(self, 'detail'):
                det = self.detail
            else:
                det = InstrumentDetail(instrument=self)

            # snapshot
            det.fund_category_name = data['AT-FundLevelCategoryName']
            det.investment_strategy = data['IC-InvestmentStrategy']
            det.legal_name = data['FSCBI-LegalName']
            det.day_end_nav = data['TS-DayEndNAV']
            det.day_end_nav_date = datetime.datetime.strptime(data['TS-DayEndNAVDate'], "%Y-%m-%d").date()
            det.nav_change = data['DP-NAVChange']

            # returns
            det.cumulative_total_return_3yr = data['DP-CumulativeReturn3Yr']
            det.cumulative_total_return_5yr = data['DP-CumulativeReturn5Yr']
            det.cumulative_total_return_10yr = data['DP-CumulativeReturn10Yr']
            det.total_return_1day = data['DP-Return1Day']
            det.total_return_1week = data['DP-Return1Week']
            det.total_return_1mth = data['DP-Return1Mth']
            det.total_return_3mth = data['DP-Return3Mth']
            det.total_return_6mth = data['DP-Return6Mth']
            det.total_return_ytd = data['DP-ReturnYTD']
            det.total_return_1yr = data['DP-Return1Yr']
            det.nav_52wk_high = data['TS-NAV52wkHigh']
            det.nav_52wk_high_date = datetime.datetime.strptime(data['TS-NAV52wkHighDate'], "%Y-%m-%d").date()
            det.nav_52wk_low = data['TS-NAV52wkLow']
            det.nav_52wk_low_date = datetime.datetime.strptime(data['TS-NAV52wkLowDate'], "%Y-%m-%d").date()

            # analysis
            det.information_ratio_1yr = data.get('RMC-InformationRatio1Yr', 0.0)
            det.treynor_ratio_1yr = data.get('RMC-TreynorRatio1Yr', 0.0)
            det.max_drawdown_1yr = data['RM-MaxDrawdown1Yr']
            det.max_drawdown_3yr = data['RM-MaxDrawdown3Yr']
            det.max_drawdown_5yr = data['RM-MaxDrawdown5Yr']
            det.sharpe_ratio_1yr = data['RM-SharpeRatio1Yr']
            det.sortino_ratio_1yr = data['RM-SortinoRatio1Yr']
            det.std_dev_1yr = data['RM-StdDev1Yr']
            det.net_expense_ratio = data['ARF-NetExpenseRatio']
            det.annual_report_turnover_ratio = data['ARFS-AnnualReportTurnoverRatio']

            # scheme details
            det.amc_name = data['FSCBI-ProviderCompanyName']
            det.fund_name = data['FSCBI-FundName']
            det.inception_date = datetime.datetime.strptime(data['FSCBI-InceptionDate'], "%Y-%m-%d").date()
            det.distribution_status = 'Growth' if data['FSCBI-DistributionStatus'] == 'Accumulated' \
                                       else 'Dividend' if data['FSCBI-DistributionStatus'] == 'Income' else ''
            det.isin = data['FSCBI-ISIN']
            det.sip_availability = "Yes" if data['PI-SIPAvailability'] == "true" else "No"
            det.exit_load = 0.0
            det.maximum_management_fee = 0.0
            det.minimum_redemption_amount = 0.0
            si_min_amounts = [int(d['MinAmount']) for d in data['IAIP-AIP'] if d['Frequency'] == "Monthly"]
            det.si_min_amount = min(si_min_amounts)
            det.risk_level = data['FSCBI-IndianRiskLevel']
            det.investment_philosophy = "NA"
            det.aum = data['FNA-AsOfOriginalReported']
            det.broad_category_group = data['FSCBI-BroadCategoryGroup']
            det.save()
            logger.info(f"Saved mutual fund details")

            self.fund_managers.all().delete()
            for fm in data['FM-Managers']:
                inst_fm = InstrumentFundManager(instrument=self)
                inst_fm.display = fm['Display']
                inst_fm.managerid = fm['ManagerId']
                inst_fm.name = fm['Name']
                inst_fm.role = fm['Role']
                inst_fm.start_date = datetime.datetime.strptime(fm['StartDate'], '%Y-%m-%d').date()
                inst_fm.tenure = fm['Tenure']
                inst_fm.save()
                logger.info(f"Added fund manager {inst_fm.name}")
            logger.error(f"Fetching mutual fund details completed successfully for {self.name} - {self.mstarid} !")
        else:
            logger.error(f"Fetching mutual fund details failed: {data}")

    def fetch_nav_history(self, start_date=None, end_date=None):
        logger.info(f"Fetching nav history from morning star for {self.name} - {self.mstarid}")
        nowtime = timezone.now()
        if not start_date:
            start_date = nowtime.replace(year=nowtime.year-5).strftime("%Y-%m-%d")
        if not end_date:
            end_date = nowtime.strftime("%Y-%m-%d")
        status, data = nav_history(self.mstarid, start_date, end_date)
        logger.info(f"{len(data)} records found")
        if status == 'Success':
            for prices in data:
                nav_date = datetime.datetime.strptime(prices['d'], '%Y-%m-%d')
                try:
                    InstrumentNAVDetail.objects.get(instrument=self, nav_date=nav_date)
                except InstrumentNAVDetail.DoesNotExist:
                    InstrumentNAVDetail.objects.create(instrument=self, nav_date=nav_date, nav_value=float(prices['v']))
            logger.info(f"Fetching nav history completed !")
        else:
            logger.error(f"Fetching nav history failed: {data}")

    def fetch_current_nav_price(self):
        logger.info(f"Fetching current nav price from morning star for {self.name} - {self.mstarid}")
        status, data = nav_price(self.mstarid)

        if status == 'Success':
            nav_date = datetime.datetime.strptime(data['DP-DayEndDate'], '%Y-%m-%d').date()
            nav_value = float(data['DP-DayEndNAV'])
            try:
                det = InstrumentNAVDetail.objects.get(instrument=self, nav_date=nav_date)
                if det.nav_value == data['DP-DayEndNAV']:
                    logger.info(f"Nav details already exist for {nav_date}")
                    return
            except InstrumentNAVDetail.DoesNotExist:
                det = InstrumentNAVDetail(instrument=self, nav_date=nav_date)
            det.nav_value = nav_value
            det.nav_change = float(data['DP-NAVChange'])
            det.nav_change_percent = float(data['DP-NAVChangePercentage'])
            det.nav_52_week_high_value = float(data['TS-NAV52wkHigh'])
            det.nav_52_week_high_date = datetime.datetime.strptime(data['TS-NAV52wkHighDate'], '%Y-%m-%d').date()
            det.nav_52_week_low_value = float(data['TS-NAV52wkLow'])
            det.nav_52_week_low_date = datetime.datetime.strptime(data['TS-NAV52wkLowDate'], '%Y-%m-%d').date()
            det.nav_return_1_week = float(data['DP-Return1Week'])
            det.nav_return_1_month = float(data['DP-Return1Mth'])
            det.nav_return_1_year = float(data['DP-Return1Yr'])
            det.nav_return_3_year = float(data['DP-Return3Yr'])
            det.nav_return_5_year = float(data['DP-Return5Yr'])
            det.nav_return_since_inception = float(data['DP-ReturnSinceInception'])
            det.nav_return_cumulative_3_year = float(data['DP-CumulativeReturn3Yr'])
            det.nav_return_cumulative_5_year = float(data['DP-CumulativeReturn5Yr'])
            det.save()

            # updating exp return
            self.current_nav_value = nav_value
            self.current_nav_date = nav_date
            self.current_nav_change = det.nav_change
            self.return_1y = det.nav_return_1_year
            self.return_3y = det.nav_return_3_year
            self.return_5y = det.nav_return_5_year
            self.save()

            logger.info(f"Fetching nav price completed !")
        else:
            logger.error(f"Fetching nav price failed: {data}")

    def fetch_detail_bsestar(self):
        df = pd.read_csv(settings.DATA_DIR / "bsestar/BSEStar_Scheme_Master.csv")
        df_scheme = df[df['ISIN'] == self.isin]
        if not df_scheme.empty:
            self.bsestar_scheme_code = df_scheme['Scheme Code'].values[0]
            self.save()
        df = pd.read_csv(settings.DATA_DIR / "bsestar/sip_details.csv")
        df_sip = df[(df['SCHEME CODE'] == self.bsestar_scheme_code)]
        if not df_sip.empty:
            self.sip_weekly_allowed = False
            self.sip_monthly_allowed = False
            self.sip_weekly_dates = None
            self.sip_monthly_dates = None
            for _, row in df_sip.iterrows():
                if row['SIP FREQUENCY'] == "WEEKLY":
                    self.sip_weekly_allowed = True
                    self.sip_weekly_dates = row['SIP DATES']
                elif row['SIP FREQUENCY'] == "MONTHLY":
                    self.sip_monthly_allowed = True
                    self.sip_monthly_dates = row['SIP DATES']
                else:
                    continue
                if row['SIP TRANSACTION MODE'] not in ("P", "DP"):
                    continue
                self.sip_max_gap = int(row['SIP MAXIMUM GAP'])
                self.sip_min_installment_amount = int(row['SIP MINIMUM INSTALLMENT AMOUNT'])
                self.sip_max_installment_amount = int(row['SIP MAXIMUM INSTALLMENT AMOUNT'])
                self.sip_multiplier_amount = int(row['SIP MULTIPLIER AMOUNT'])
                self.sip_min_installment_number = int(row['SIP MINIMUM INSTALLMENT NUMBERS'])
                self.sip_max_installment_amount = int(row['SIP MAXIMUM INSTALLMENT NUMBERS'])
            self.save()


@receiver(pre_save, sender=Instrument)
def populate_values(instance, sender, **kwargs):
    if not instance.code:
        instance.code = slugify(instance.name)
    instance.expected_return = round((instance.return_1y + instance.return_3y + instance.return_5y)/3, 2)


@receiver(post_save, sender=Instrument)
def fetch_mutual_fund_details(instance, sender, created, **kwargs):
    if not instance.mstarid:
        instance.fetch_detail_morningstar()
        instance.fetch_nav_history()
        instance.fetch_current_nav_price()
    if not instance.bsestar_scheme_code:
        instance.fetch_detail_bsestar()


class InstrumentDetail(models.Model):
    instrument = models.OneToOneField(Instrument, on_delete=models.CASCADE, related_name='detail')

    # snapshot
    fund_category_name = models.CharField(max_length=200)
    investment_strategy = models.TextField()
    legal_name = models.CharField(max_length=300)
    day_end_nav = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    day_end_nav_date = models.DateField()
    nav_change = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)

    # returns
    cumulative_total_return_3yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    cumulative_total_return_5yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    cumulative_total_return_10yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    total_return_1day = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    total_return_1week = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    total_return_1mth = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    total_return_3mth = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    total_return_6mth = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    total_return_ytd = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    total_return_1yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    nav_52wk_high = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    nav_52wk_high_date = models.DateField()
    nav_52wk_low = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    nav_52wk_low_date = models.DateField()

    # analysis
    information_ratio_1yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    treynor_ratio_1yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    max_drawdown_1yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    max_drawdown_3yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    max_drawdown_5yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    sharpe_ratio_1yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    sortino_ratio_1yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    std_dev_1yr = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    net_expense_ratio = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    annual_report_turnover_ratio = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)

    # scheme details
    amc_name = models.CharField(max_length=300, default="", blank=True)
    fund_name = models.CharField(max_length=100, default="", blank=True)
    inception_date = models.DateField()
    distribution_status = models.CharField(max_length=50, default="", blank=True)
    isin = models.CharField(max_length=12, default="", blank=True)
    sip_availability = models.CharField(max_length=10, default="No", blank=True, null=True)
    exit_load = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    maximum_management_fee = models.DecimalField(default=0.0, max_digits=7, decimal_places=1)
    minimum_redemption_amount = models.PositiveBigIntegerField(default=0)
    si_min_amount = models.IntegerField(default=0)
    risk_level = models.CharField(max_length=100)
    aum = models.CharField(max_length=20, blank=True, null=True)
    broad_category_group = models.CharField(max_length=50, blank=True, null=True)
    investment_philosophy = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.legal_name


class InstrumentFundManager(models.Model):
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, related_name='fund_managers')
    display = models.CharField(max_length=100)
    managerid = models.PositiveIntegerField()
    name = models.CharField(max_length=200)
    role = models.CharField(max_length=100)
    start_date = models.DateField()
    tenure = models.FloatField()

    def __str__(self):
        return self.name


class InstrumentNAVDetail(models.Model):
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, related_name='nav_details')
    nav_date = models.DateField()
    nav_value = models.FloatField()
    nav_change = models.FloatField(default=0.0)
    nav_change_percent = models.FloatField(default=0.0)
    nav_52_week_high_value = models.FloatField(default=0.0)
    nav_52_week_high_date = models.DateField(blank=True, null=True)
    nav_52_week_low_value = models.FloatField(default=0.0)
    nav_52_week_low_date = models.DateField(blank=True, null=True)
    nav_return_1_week = models.FloatField(default=0.0)
    nav_return_1_month = models.FloatField(default=0.0)
    nav_return_1_year = models.FloatField(default=0.0)
    nav_return_3_year = models.FloatField(default=0.0)
    nav_return_5_year = models.FloatField(default=0.0)
    nav_return_since_inception = models.FloatField(default=0.0)
    nav_return_cumulative_3_year = models.FloatField(default=0.0)
    nav_return_cumulative_5_year = models.FloatField(default=0.0)

    class Meta:
        unique_together = ('instrument', 'nav_date')
        ordering = ('-nav_date',)

    def __str__(self):
        return f"{self.nav_date} : {self.nav_value}"


@receiver(post_save, sender=InstrumentNAVDetail)
def update_investment_monthly_return(instance, sender, created, **kwargs):
    nav_date = instance.nav_date.date() if isinstance(instance.nav_date, datetime.datetime) else instance.nav_date
    try:
        imr = InstrumentMonthlyReturn.objects.get(instrument=instance.instrument,
                                                  month=nav_date.month,
                                                  year=nav_date.year)
        if nav_date < imr.month_start_date:
            imr.month_start_date = nav_date
            imr.month_start_nav = float(instance.nav_value)
        elif nav_date > imr.month_end_date:
            imr.month_end_date = nav_date
            imr.month_end_nav = float(instance.nav_value)

    except InstrumentMonthlyReturn.DoesNotExist:
        imr = InstrumentMonthlyReturn(instrument=instance.instrument,
                                      month=nav_date.month,
                                      year=nav_date.year)
        imr.month_start_date = nav_date
        imr.month_start_nav = float(instance.nav_value)
        imr.month_end_date = nav_date
        imr.month_end_nav = float(instance.nav_value)

    imr.save()


class InstrumentMonthlyReturn(models.Model):
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE, related_name="monthly_returns")
    month = models.PositiveSmallIntegerField(validators=[MinValueValidator(1), MaxValueValidator(12)])
    year = models.PositiveSmallIntegerField(validators=[MinValueValidator(1970), MaxValueValidator(2100)])
    month_start_date = models.DateField(blank=True, null=True)
    month_end_date = models.DateField(blank=True, null=True)
    month_start_nav = models.FloatField(default=0.0)
    month_end_nav = models.FloatField(default=0.0)
    return_percent = models.FloatField(default=0.0)

    class Meta:
        unique_together = ('instrument', 'month', 'year')
        ordering = ('year', 'month')

    def __str__(self):
        return f"{self.month} {self.year}"


@receiver(pre_save, sender=InstrumentMonthlyReturn)
def update_return_percent(instance, sender, **kwargs):
    if instance.month_start_nav:
        instance.return_percent = round((instance.month_end_nav - instance.month_start_nav) * 100 / instance.month_start_nav, 2)
