from core.storage import MediaStorage


class InstrumentMediaStorage(MediaStorage):
    bucket_name = 'fikaa-assets'
    location = 'instrument'
