from rest_framework.serializers import ModelSerializer, CharField
from .models import Instrument, InstrumentDetail, InstrumentFundManager, InstrumentNAVDetail


class InstrumentNAVDetail(ModelSerializer):

    class Meta:
        model = InstrumentNAVDetail
        fields = ('nav_date', 'nav_value')


class InstrumentFundManagerSerializer(ModelSerializer):

    class Meta:
        model = InstrumentFundManager
        fields = ('name', 'role', 'start_date', 'tenure')


class InstrumentDetailSerializer(ModelSerializer):

    class Meta:
        model = InstrumentDetail
        fields = '__all__'


class InstrumentSerializer(ModelSerializer):

    name = CharField(source='legal_name')
    detail = InstrumentDetailSerializer()
    fund_managers = InstrumentFundManagerSerializer(many=True)
    nav_details = InstrumentNAVDetail(many=True)

    class Meta:
        model = Instrument
        fields = ('id', 'name', 'code', 'isin', 'expected_return', 'max_monthly_drawdown',
                  'detail', 'fund_managers', 'nav_details', 'nav_details_index', 'growth_10k')
