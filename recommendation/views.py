import logging

from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, CreateAPIView, ListAPIView, UpdateAPIView, RetrieveAPIView
from rest_framework.status import HTTP_400_BAD_REQUEST

from utils.response import response_any, response_200
from .rule_engine import RuleEngine

from .models import (
    WealthCard,
    WealthWishlist,
    UserRecommendation,
)

from .serializers import (
    WealthCardSerializer,
    WealthWishlistSerializer,
)

logger = logging.getLogger(__name__)


class RecommendWealthCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = WealthCard.objects.all()
    serializer_class = WealthCardSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Wealth Card created successfully", response.data)


class RecommendWealthListView(ListAPIView):
    permission_classes = (AllowAny,)
    queryset = WealthCard.objects.all()
    serializer_class = WealthCardSerializer

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("Wealth Card list fetched successfully", response.data)


class RecommendWealthUpdateView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = WealthCard.objects.all()
    serializer_class = WealthCardSerializer

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        response = super().update(request, *args, **kwargs)
        return response_200("Wealth Card updated successfully", response.data)


class RecommendWealthRetrieveView(RetrieveAPIView):
    permission_classes = (AllowAny,)
    queryset = WealthCard.objects.all()
    serializer_class = WealthCardSerializer

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("Wealth Card data fetched successfully", response.data)


class RecommendWealthWishlistView(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = WealthWishlistSerializer

    def post(self, request, *args, **kwargs):
        queryset = WealthWishlist.objects.filter(amount_from__lte=request.data['amount'],
                                                 amount_to__gt=request.data['amount'],
                                                 is_active=True)
        data = self.serializer_class(queryset, many=True).data
        return response_200("Wealth Card wishilist fetched successfully", data)


class RecommendWishlistCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = WealthWishlist.objects.all()
    serializer_class = WealthWishlistSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Wishlist created sucessfully", response.data)


class RecommendWishlistListView(ListAPIView):
    permission_classes = (AllowAny,)
    queryset = WealthWishlist.objects.all()
    serializer_class = WealthWishlistSerializer

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("Wishlist list fetched sucessfully", response.data)


class RecommendWishlistUpdateView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = WealthWishlist.objects.all()
    serializer_class = WealthWishlistSerializer

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        response = super().update(request, *args, **kwargs)
        return response_200("Wishlist updated sucessfully", response.data)


class RecommendWishlistRetrieveView(RetrieveAPIView):
    permission_classes = (AllowAny,)
    queryset = WealthWishlist.objects.all()
    serializer_class = WealthWishlistSerializer

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("Wishlist fetched sucessfully", response.data)


class RecommendInvestmentView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        risk_profile = int(request.GET['risk_profile'])
        inv_interval = int(request.GET['investment_interval'])
        inv_amount = float(request.GET['investment_amount'])
        target_amount = float(request.GET['target_amount'])

        try:
            rule_engine = RuleEngine()
            result = rule_engine.get_recommendations(request.user.age, risk_profile, inv_amount, inv_interval,
                                                     target_amount)
            result = UserRecommendation.objects.add_recommendations(request.user, request.user.age,
                                                                    inv_amount, inv_interval, target_amount, result)
            return response_200("Recommendations fetched successfully", result)
        except Exception as e:
            logger.error(str(e))
            return response_any(HTTP_400_BAD_REQUEST, "Error in getting recommendations", str(e))


