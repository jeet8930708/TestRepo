# Generated by Django 4.0.2 on 2022-05-04 17:53

import core.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recommendation', '0012_alter_userrecommendation_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wealthcard',
            name='image',
            field=models.ImageField(storage=core.storage.MediaStorage, upload_to=''),
        ),
        migrations.AlterField(
            model_name='wealthwishlist',
            name='image',
            field=models.ImageField(storage=core.storage.MediaStorage, upload_to=''),
        ),
    ]
