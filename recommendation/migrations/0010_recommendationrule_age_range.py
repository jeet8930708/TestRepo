# Generated by Django 4.0.2 on 2022-04-30 16:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recommendation', '0009_alter_wealthcard_unique_together_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='recommendationrule',
            name='age_range',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Age_Below_25'), (2, 'Age_25_to_35'), (3, 'Age_35_to_55'), (4, 'Age_55_Above')], default=2),
        ),
    ]
