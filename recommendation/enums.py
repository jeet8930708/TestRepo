from core.enums import ChoiceEnum


class DenominationEnum(ChoiceEnum):
    Lakh = 'Lakh'
    Crore = 'Crore'
    Million = 'Million'
    Billion = 'Billion'


class InvestmentIntervalEnum(ChoiceEnum):
    Monthly = 1
    Weekly = 2
    Lumpsum = 3


class RiskProfileEnum(ChoiceEnum):
    Low = 1
    Medium = 2
    High = 3
