from django.contrib import admin

# Register your models here.

from .models import (
    WealthCard,
    WealthWishlist,
    Basket,
    RecommendationRule,
    UserRecommendation,
    UserRecommendationInvestment,
)


class WealthCardAdmin(admin.ModelAdmin):
    list_display = ['title', 'earn_amount', 'denomination', 'amount', 'is_active']
    list_filter = ['is_active']
    search_fields = ['title']
admin.site.register(WealthCard, WealthCardAdmin)


class WealthWishlistAdmin(admin.ModelAdmin):
    list_display = ['name', 'amount_from', 'amount_to', 'is_active']
    list_filter = ['is_active']
    search_fields = ['name']
admin.site.register(WealthWishlist, WealthWishlistAdmin)


class BasketAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_active']
    list_filter = ['is_active']
admin.site.register(Basket, BasketAdmin)


class RecommendationRuleAdmin(admin.ModelAdmin):
    list_display = ['age_from', 'age_to', 'age_range', 'risk_profile', 'inv_amount_from', 'inv_amount_to', 'max_drawdown', 'basket', 'is_active']
    list_filter = ['age_range', 'basket', 'risk_profile', 'is_active']
admin.site.register(RecommendationRule, RecommendationRuleAdmin)


class UserRecommendationInvestmentInline(admin.TabularInline):
    model = UserRecommendationInvestment


class UserRecommendationAdmin(admin.ModelAdmin):
    list_display = ['user', 'age', 'risk_profile', 'inv_amount', 'inv_interval', 'target_amount',
                    'expected_return', 'max_drawdown', 'estimated_tenure', 'created_on']
    list_filter = ['risk_profile', 'inv_interval', 'created_on']
    inlines = [UserRecommendationInvestmentInline]
admin.site.register(UserRecommendation, UserRecommendationAdmin)
