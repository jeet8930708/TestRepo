import re
import functools

from django.db import models
from django.db.transaction import atomic
from django.contrib.auth import get_user_model

from core.enums import AgeRangeEnum
from core.storage import MediaStorage
from investment.models import Instrument
from .enums import DenominationEnum, RiskProfileEnum, InvestmentIntervalEnum

User = get_user_model()


class WealthCard(models.Model):
    title = models.CharField(max_length=100)
    earn_amount = models.PositiveSmallIntegerField()
    denomination = models.CharField(max_length=10, choices=DenominationEnum.choices())
    amount = models.FloatField(unique=True)
    image_vertical = models.ImageField(storage=MediaStorage, blank=True, null=True)
    image_horizontal = models.ImageField(storage=MediaStorage, blank=True, null=True)
    image_json = models.JSONField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name='wealthcard_created_by')
    created_on = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name='wealthcard_modified_by')
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.title} - {self.earn_amount}"


class WealthWishlist(models.Model):
    amount_from = models.FloatField()
    amount_to = models.FloatField()
    name = models.CharField(max_length=50)
    image = models.ImageField(storage=MediaStorage)
    is_active = models.BooleanField(default=True)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name='wishlist_created_by')
    created_on = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name='wishlist_modified_by')
    modified_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Basket(models.Model):
    name = models.CharField(max_length=50, unique=True)
    description = models.TextField()
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class RecommendationRule(models.Model):
    age_from = models.PositiveSmallIntegerField()
    age_to = models.PositiveSmallIntegerField()
    age_range = models.PositiveSmallIntegerField(choices=AgeRangeEnum.choices(), default=AgeRangeEnum.Age_25_to_35.value)
    risk_profile = models.PositiveSmallIntegerField(choices=RiskProfileEnum.choices())
    inv_amount_from = models.FloatField()
    inv_amount_to = models.FloatField()
    max_drawdown = models.FloatField()
    max_return_from = models.FloatField(default=0.0)
    max_return_to = models.FloatField(default=0.0)
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = ('age_from', 'age_to', 'risk_profile', 'inv_amount_from', 'inv_amount_to')


class UserRecommendationManager(models.Manager):

    @atomic
    def add_recommendations(self, user, age, inv_amount, inv_interval, target_amount, recommendations):
        """
        recommendations: [
            {
                "investment_recommendation": [
                    {
                        "fund_no": "edelweiss-adv-dg",
                        "fund_name": "Edelweiss Balanced Adv Dir Gr",
                        "inv_amount": 1000,
                        "inv_split_percent": 50,
                        "exp_return": 16.6
                    },
                    {
                        "fund_no": "quant-tax-plan-g",
                        "fund_name": "Quant Tax Plan Dir Gr",
                        "inv_amount": 1000,
                        "inv_split_percent": 50,
                        "exp_return": 41.8
                    }
                ],
                "expected_return": 29.2,
                "max_drawdown": 15.82,
                "basket_name": "Conservative & Dynamic",
                "basket_description": "The winning combination of diversified funds for dynamic people like ...",
                "estimated_tenure": "11 years 2 months",
                "risk_profile": "Low"
            },
        ]
        """
        risk_profile_mapping = {name: value for value, name in RiskProfileEnum.choices()}
        # above is needed as risk_profile name is available in recommendation, need to store value
        for rec in recommendations:
            user_recommend = UserRecommendation.objects.create(user=user,
                                                               age=age,
                                                               risk_profile=risk_profile_mapping[rec['risk_profile']],
                                                               inv_amount=inv_amount,
                                                               inv_interval=inv_interval,
                                                               target_amount=target_amount,
                                                               expected_return=rec['expected_return'],
                                                               max_drawdown=rec['max_drawdown'],
                                                               basket=Basket.objects.get(name=rec['basket_name']),
                                                               estimated_tenure=rec['estimated_tenure']
                                                               )
            for inv in rec['investment_recommendation']:
                UserRecommendationInvestment.objects.create(user_recommendation=user_recommend,
                                                            instrument=Instrument.objects.get(code=inv['fund_no']),
                                                            inv_amount=inv['inv_amount'],
                                                            inv_split_percent=inv['inv_split_percent'],
                                                            exp_return=inv['exp_return'])
            rec['id'] = user_recommend.id

        return recommendations


class UserRecommendation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="recommendations")
    age = models.PositiveSmallIntegerField()
    risk_profile = models.PositiveSmallIntegerField(choices=RiskProfileEnum.choices())
    inv_amount = models.FloatField()
    inv_interval = models.PositiveSmallIntegerField(choices=InvestmentIntervalEnum.choices())
    target_amount = models.FloatField()
    expected_return = models.FloatField()
    max_drawdown = models.FloatField()
    basket = models.ForeignKey(Basket, on_delete=models.PROTECT, related_name="recommendations")
    estimated_tenure = models.CharField(max_length=50)
    created_on = models.DateTimeField(auto_now_add=True)

    objects = UserRecommendationManager()

    class Meta:
        ordering = ('-created_on',)

    @property
    def sip_valid_start_dates(self):
        valid_dates = []
        for inv in self.investments.all():
            if self.inv_interval == InvestmentIntervalEnum.Weekly.value:
                valid_dates.append(inv.instrument.sip_weekly_valid_start_dates)
            else:
                valid_dates.append(inv.instrument.sip_monthly_valid_start_dates)
        return list(functools.reduce(lambda x, y: set(x).intersection(y), valid_dates))

    @property
    def total_installments(self):
        tenure_num = re.findall(r"\d+", self.estimated_tenure)  # [11, 3]
        tenure = 0
        if tenure_num:
            tenure = int(tenure_num[0]) * 12 + (int(tenure_num[1]) if len(tenure_num) > 1 else 0)
        if self.inv_interval == InvestmentIntervalEnum.Weekly.value:
            tenure *= 4
        return tenure


class UserRecommendationInvestment(models.Model):
    user_recommendation = models.ForeignKey(UserRecommendation, on_delete=models.CASCADE, related_name="investments")
    instrument = models.ForeignKey(Instrument, on_delete=models.PROTECT, related_name="recommendations")
    inv_amount = models.FloatField()
    inv_split_percent = models.FloatField()
    exp_return = models.FloatField()

    class Meta:
        ordering = ('-inv_amount',)
