import logging
import pandas as pd
import time

from functools import reduce
from itertools import permutations, chain
from django.conf import settings
from django.core.cache import cache

from core.decorators import timeit
from investment.models import Instrument, InstrumentMonthlyReturn
from recommendation.models import RecommendationRule, RiskProfileEnum, InvestmentIntervalEnum
from recommendation.calculator import calc_tenure

logger = logging.getLogger('recommendation')


class RuleEngine:

    def __init__(self):
        self.inst = Instrument.objects.filter(is_active=True)
        self.inst_ids = self.inst.values_list('code', flat=True)
        self.inst_count = len(self.inst_ids)
        self.data_root_dir = settings.DATA_DIR / 'recommendation'
        self.investment_ratio_path = self.data_root_dir / f'investment_ratio_{self.inst_count}.csv'
        self.exp_return_path = self.data_root_dir / f'exp_return_{self.inst_count}.csv'
        self.monthly_return_path = self.data_root_dir / f'monthly_return_{self.inst_count}.csv'
        self.drawdown_path = self.data_root_dir / f'drawdown_{self.inst_count}.csv'
        self.recommendation_path = self.data_root_dir / f'recommendation_{self.inst_count}.csv'
        self.risk_profile_mapping = dict(RiskProfileEnum.choices())

    @staticmethod
    def find_numbers(ans, arr, temp, total, index, max_len):
        """
        Recursive function to find numbers that adds up to total
        """
        if (total == 0) and len(temp) == max_len:
            # Adding deep copy of list to ans
            ans.append(list(temp))
            return

        # Iterate from index to len(arr) - 1
        for i in range(index, len(arr)):

            # checking that sum does not become negative
            if (total - arr[i]) >= 0 and len(temp) < max_len:
                # adding element which can contribute to
                # sum
                temp.append(arr[i])
                RuleEngine.find_numbers(ans, arr, temp, total - arr[i], i, max_len)

                # removing element from list (backtracking)
                temp.remove(arr[i])

    @staticmethod
    def combination_sum(arr, total, max_len):
        """
        Getting combination of numbers that sums up to total
        Params:
            arr: list of integers to be added for getting total e.g.: [0, 5, 10, 15, 25, 30, 35, 40, 45, 50]
            total: total sum Eg.: 50
            max_len: maximum size of an array of each combination e.g.: 5
        """
        ans = []
        temp = []

        # first do hashing nothing but set{}
        # since set does not always sort
        # removing the duplicates using Set and
        # Sorting the List
        arr = sorted(list(set(arr)))
        RuleEngine.find_numbers(ans, arr, temp, total, 0, max_len)
        return ans

    @staticmethod
    def perm(x):
        return set(permutations(x, len(x)))

    def generate_investment_ratio_data(self):
        """
        Generates all permutations of 100% ratio split across mutual funds and saves locally
        Return:
            DataFrame
        """
        arr = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100]
        ans = self.combination_sum(arr, 100, self.inst_count)
        logger.info(f"Total combinations -> {len(ans)}")

        final = list(map(self.perm, ans))
        split_final = list(chain(*final))
        logger.info(f"Total permutations -> {len(split_final)}")

        df_split = pd.DataFrame(split_final, columns=self.inst_ids)
        df_split.index = df_split.apply(lambda row: reduce(lambda x, y: str(x) + '-' + str(y), row), axis=1)
        df_split.index.name = "SPLIT_RATIO"
        df_split.to_csv(self.investment_ratio_path)
        return df_split

    def fetch_investment_ratio_data(self):
        return pd.read_csv(self.investment_ratio_path, index_col='SPLIT_RATIO')

    def generate_expected_return_data(self):
        """
        Derived expected return total for each combination
        """
        # get split ratios
        df_split = self.fetch_investment_ratio_data()

        # multiply split ratios with expected return
        series_return = pd.Series([i.expected_return for i in self.inst])
        series_return.index = self.inst_ids

        df_exp_return = df_split * series_return / 100

        # calculate max return
        df_exp_return['EXP_RETURN'] = round(df_exp_return.sum(axis=1), 2)

        df_exp_return.to_csv(self.exp_return_path)
        return df_exp_return

    def fetch_expected_return_data(self):
        return pd.read_csv(self.exp_return_path, index_col='SPLIT_RATIO')

    @staticmethod
    def clean_monthly_return_data(df_monthly_return):
        # replace NaN by 0
        df_monthly_return.fillna(0.0, inplace=True)

        # discard row where all mutual funds return is greater than 0.0
        df_monthly_return = df_monthly_return.loc[
            df_monthly_return.apply(lambda row: any([v < 0.0 for v in row]), axis=1)]

        return df_monthly_return

    @staticmethod
    def derive_max_drawdown(row_split_ratio, df_monthly_return):
        df_split_drawdown = row_split_ratio * df_monthly_return / 100
        df_split_drawdown['DRAWDOWN'] = df_split_drawdown.sum(axis=1)
        return abs(round(df_split_drawdown['DRAWDOWN'].min(), 2))

    def fetch_monthly_return_data(self):
        inv_rr = InstrumentMonthlyReturn.objects.filter(instrument__code__in=self.inst_ids)\
                                                .values('instrument__code',
                                                        'month',
                                                        'year',
                                                        'return_percent')
        df_mr = pd.DataFrame(list(inv_rr))
        df_mr = df_mr.pivot(index=['month', 'year'], columns='instrument__code', values='return_percent').reset_index()
        df_mr.drop(['month', 'year'], axis=1, inplace=True)
        df_mr.to_csv(self.monthly_return_path)
        return df_mr

    def generate_max_drawdown_data(self):
        df_monthly_return = self.fetch_monthly_return_data()
        logger.info(f"Max drawdown master :::: {len(df_monthly_return)}")

        df_monthly_return = self.clean_monthly_return_data(df_monthly_return)
        logger.info(f"Max drawdown master after cleanup :::: {len(df_monthly_return)}")

        df_split = self.fetch_investment_ratio_data()
        logger.info(f"Fetched investment ratios :::: {len(df_split)}")

        logger.info(f"Deriving max drawdown START")
        df_split['MAX_DRAWDOWN'] = 0.0

        for idx, row in df_split.iterrows():
            # logger.info(f"Deriving Max Drawdown for {idx}")
            max_drawdown = self.derive_max_drawdown(row, df_monthly_return)
            df_split.at[idx, 'MAX_DRAWDOWN'] = max_drawdown
            logger.info(f"Max Drawdown for {idx} = {max_drawdown}")

        logger.info(f"Deriving max drawdown END")
        df_split.to_csv(self.drawdown_path)

        return df_split

    def fetch_max_drawdown_data(self):
        return pd.read_csv(self.drawdown_path, index_col='SPLIT_RATIO')

    def generate_recommendation_data(self):
        df_split = self.fetch_investment_ratio_data()
        df_exp_return = self.fetch_expected_return_data()
        df_max_drawdown = self.fetch_max_drawdown_data()
        df_recommend = df_split.join(df_exp_return[["EXP_RETURN"]]).join(df_max_drawdown[["MAX_DRAWDOWN"]])
        df_recommend.to_csv(self.recommendation_path)
        return df_recommend

    def fetch_recommendation_data(self):
        key = "recommendation-data"
        df = cache.get(key)
        if df is None:
            logger.info("Fetching recommendation data from server")
            df = pd.read_csv(self.recommendation_path, index_col='SPLIT_RATIO')
            cache.set(key, df)
        return df

    def build_rules(self):
        self.generate_investment_ratio_data()
        self.generate_expected_return_data()
        self.generate_max_drawdown_data()
        self.generate_recommendation_data()

    @staticmethod
    def derive_recommendation_rule(age, risk_profile, inv_amount):
        logger.info(f"Fetching recommendation rule for Age:{age}, Risk:{risk_profile}, Amount:{inv_amount}")
        try:
            rule = RecommendationRule.objects.get(is_active=True,
                                                  age_from__lte=age,
                                                  age_to__gt=age,
                                                  risk_profile=risk_profile,
                                                  inv_amount_from__lte=inv_amount,
                                                  inv_amount_to__gt=inv_amount)
            rule_data = {"max_drawdown": rule.max_drawdown,
                         "max_return_to": rule.max_return_to,
                         "basket_name": rule.basket.name,
                         "basket_description": rule.basket.description}
            return rule_data
        except RecommendationRule.DoesNotExist:
            return None

    def discard_split_below_min_inv_amt(self, df, inv_interval):
        min_inv_values = [(i.code,
                           i.lumpsum_min_investment_amount if inv_interval==InvestmentIntervalEnum.Lumpsum.value \
                           else i.sip_min_installment_amount) for i in self.inst]
        query = ' & '.join([f'((@df["{inst_code}"] == 0) | (@df["{inst_code}"] > 0 & @df["{inst_code}"] >= {min_amt}))' \
                            for inst_code, min_amt in min_inv_values])
        df = df.query(query)
        return df

    @staticmethod
    def empty_recommendation():
        return {
                "investment_recommendation": [],
                "expected_return": 0.0,
                "max_drawdown": 0.0,
                "basket_name": "",
                "basket_description": "",
                "estimated_tenure": 0.0,
                "risk_profile": "",
            }

    @timeit
    def recommend(self, age, risk_profile, inv_amount, target_amount, inv_interval):
        # derive recommendation rule
        rule = self.derive_recommendation_rule(age, risk_profile, inv_amount)
        logger.info(f"Recommendation Rule :::: {rule}")
        if rule is None:
            logger.error("No recommendation rule found !")
            return self.empty_recommendation()

        cache_key = f"recommendation-{age}-{risk_profile}-{inv_amount}-{inv_interval}"
        mf_max_return = cache.get(cache_key)
        if mf_max_return is None:
            st = time.time()
            logger.info("Deriving recommendations...")

            # step 1: Fetch recommendation data
            df = self.fetch_recommendation_data()
            logger.info(f"Total recommendations :::::: {round((time.time()-st), 2)} seconds")

            # step 2: Discard splits exceeding max drawdown threshold
            df = df.loc[df['MAX_DRAWDOWN'] <= rule['max_drawdown']]
            logger.info(f"Total recommendations after discarding ones exceeding drawdown threshold ::::: {len(df)}:"
                        f" {round((time.time()-st), 2)} seconds")

            # step 3: Discard splits exceeding max exp return
            df = df.loc[(df['EXP_RETURN'] <= rule['max_return_to'])]
            logger.info(f"Total recommendations after discarding ones exceeding exp return cap ::::: {len(df)}: "
                        f"{round((time.time()-st), 2)} seconds")

            # step 4: Allocate amount to each fund
            for n in self.inst_ids:
                df[n] = df[n] * inv_amount / 100

            # step 5: Discard splits which does not satisfy min inv amount criteria for each mutual fund
            df = self.discard_split_below_min_inv_amt(df, inv_interval)
            df['EXP_RETURN'] = df['EXP_RETURN'].astype(float)
            df['MAX_DRAWDOWN'] = df['MAX_DRAWDOWN'].astype(float)
            df.sort_values(['EXP_RETURN', 'MAX_DRAWDOWN'], ascending=False, inplace=True)
            df.reset_index(inplace=True, drop=True)
            logger.info(f"Total splits after discarding min inv amount criteria ::::: {len(df)}: "
                        f"{round((time.time()-st), 2)} seconds")

            if df.empty:
                return self.empty_recommendation()

            # step 6: Return split with maximum return
            mf_max_return = df.iloc[0]
            cache.set(cache_key, mf_max_return)
            logger.info(f"Splits with max return {mf_max_return}: {round((time.time()-st), 2)} seconds")

        # calculate tenure
        tenure_num, tenure_text = calc_tenure(target_amount, inv_amount, mf_max_return[-2], inv_interval)
        logger.info(f"Estimated tenure {tenure_num}")
        # if tenure_num > 30:
        #     logger.warning("Tenure greater than 30 years")
        #     return self.empty_recommendation()

        inv_split = []
        for inst in self.inst:
            split_amount = mf_max_return[inst.code]
            if split_amount > 0.0:
                inv_split.append({
                    'fund_no': inst.code,
                    'fund_name': inst.legal_name,
                    'fund_logo': inst.logo.url if inst.logo else None,
                    'inv_amount': round(split_amount),
                    'inv_split_percent': round(split_amount * 100 / inv_amount, 2),
                    'exp_return': inst.expected_return,
                })
        result = {
            "investment_recommendation": sorted(inv_split, key=lambda x: x['inv_amount'], reverse=True),
            "expected_return": mf_max_return[-2],
            "max_drawdown": mf_max_return[-1],
            "basket_name": rule['basket_name'],
            "basket_description": rule['basket_description'],
            "estimated_tenure": tenure_text,
            "risk_profile": self.risk_profile_mapping[risk_profile]
        }
        # logger.info(f"Recommendation: {result}")
        return result

    def get_recommendations(self, age, risk_profile, inv_amount, inv_interval, target_amount):
        if risk_profile == RiskProfileEnum.Low.value:
            recommend_risk_profiles = [RiskProfileEnum.Low.value, RiskProfileEnum.Medium.value,
                                       RiskProfileEnum.High.value]
        elif risk_profile == RiskProfileEnum.Medium.value:
            recommend_risk_profiles = [RiskProfileEnum.Medium.value, RiskProfileEnum.High.value]
        else:
            recommend_risk_profiles = [RiskProfileEnum.High.value]

        recommendations = []
        for rp in recommend_risk_profiles:
            logger.info(f"***** Fetching recommendations for Age:{age}, RiskProile: {rp}, Inv Amount: {inv_amount}, "
                        f"Target Amount: {target_amount}, Interval: {inv_interval}")
            result = self.recommend(age, rp, inv_amount, target_amount, inv_interval)
            if result["investment_recommendation"]:
                recommendations.append(result)

        return recommendations


# def main():
#      rule_engine = RuleEngine()
#      rule_engine.get_recommendations(27, 1, 10000, 1, 1000000)

