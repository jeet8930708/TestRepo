from django.urls import path

from .views import (
    RecommendInvestmentView,
    RecommendWealthCreateView,
    RecommendWealthListView,
    RecommendWealthUpdateView,
    RecommendWealthRetrieveView,
    RecommendWealthWishlistView,

    RecommendWishlistCreateView,
    RecommendWishlistListView,
    RecommendWishlistRetrieveView,
    RecommendWishlistUpdateView,
)

urlpatterns = [
    path('wealth-card/create/', RecommendWealthCreateView.as_view(), name='recommend-wealth-card-create'),
    path('wealth-card/list/', RecommendWealthListView.as_view(), name='recommend-wealth-card-list'),
    path('wealth-card/retrieve/<int:pk>/', RecommendWealthRetrieveView.as_view(),
         name='recommend-wealth-card-retrieve'),
    path('wealth-card/update/<int:pk>/', RecommendWealthUpdateView.as_view(),
         name='recommend-wealth-card-update'),
    path('wealth-card/wishlist/', RecommendWealthWishlistView.as_view(),
         name='recommend-wealth-card-wishlist'),

    path('wealth/wishlist/create/', RecommendWishlistCreateView.as_view(),
         name='recommend-wealth-wishlist-create'),
    path('wealth/wishlist/list/', RecommendWishlistListView.as_view(),
         name='recommend-wealth-wishlist-list'),
    path('wealth/wishlist/retrieve/<int:pk>/', RecommendWishlistRetrieveView.as_view(),
         name='recommend-wealth-wishlist-retrieve'),
    path('wealth/wishlist/update/<int:pk>/', RecommendWishlistUpdateView.as_view(),
         name='recommend-wealth-wishlist-update'),

    path('investment/', RecommendInvestmentView.as_view(), name='recommend-investment'),
]