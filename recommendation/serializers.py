from rest_framework.serializers import (
    CharField,
    ModelSerializer,
    BooleanField
)

from .models import (
    WealthCard,
    WealthWishlist,
)

from drf_extra_fields.fields import Base64ImageField


# ========================================================================
class WealthCardSerializer(ModelSerializer):

    image_vertical = Base64ImageField()
    image_horizontal = Base64ImageField()

    class Meta:
        model = WealthCard
        fields = ['id', 'title', 'earn_amount', 'denomination', 'amount', 'image_vertical', 'image_horizontal',
                  'image_json', 'is_active']
        read_only_fields = ['id']

    def create(self, validated_data):
        request = self.context['request']
        validated_data['created_by'] = request.user
        validated_data['modified_by'] = request.user
        validated_data['is_active'] = True
        return super().create(validated_data)


class WealthWishlistSerializer(ModelSerializer):

    image = Base64ImageField()

    class Meta:
        model = WealthWishlist
        fields = ['id', 'amount_from', 'amount_to', 'name', 'image', 'is_active']
        read_only_fields = ['id']

    def create(self, validated_data):
        request = self.context['request']
        validated_data['created_by'] = request.user
        validated_data['modified_by'] = request.user
        validated_data['is_active'] = True
        return super().create(validated_data)
