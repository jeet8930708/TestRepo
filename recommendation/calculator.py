import math
import logging

logger = logging.getLogger('recommendation')


def calc_tenure(target_amount, inv_amount, interest, inv_interval):
    """
         inv_interval: 1-> Monthly, 2 -> Weekly, 3 -> Lumpsum

         SIP Calculator (Monthly & Weekly)
         ==================================
         M = P x (((1 + i) ^ n - 1) / i) x (1 + i)

         M: the amount you receive upon maturity
         P: the amount you invest at regular intervals
         n: the number of payments you have made
         i: the periodic interest rate

         Take for example you want to invest Rs. 1000 per month for 12 months at a periodic rate of interest of 12%

         then monthly rate of return will be 12%/12 = 1/100 = 0.01

         Hence, M = 1000 x (((1 + 0.01) ^ 12 - 1) / 0.01) x (1 + 0.01)

         Deriving n
         ===========
         M / (P x (1 + i)) =  (((1 + i) ^ n - 1) / i)

         (M x i) / (P x (1 + i)) = (1 + i) ^ n - 1

         ((M x i) / (P x (1 + i)) + 1) = (1 + i) ^ n

         n = log (1+i) (((M x i) / (P x (1 + i))) + 1)

         Lumpsum Calculator
         ===================
         M = P (1 + i) ^ n
         n = log (1+i) M/p
    """
    logger.info(f"Calculating tenure for "
                 f"Target Amount: {target_amount}, "
                 f"Inv Amount: {inv_amount}, "
                 f"Interest: {interest}, "
                 f"Inv INterval: {inv_interval}")
    inv_amount = inv_amount * 4.33 if inv_interval == 2 else inv_amount
    i = (interest/100) if inv_interval == 3 else (interest / 100) / 12
    if inv_interval == 3:
        a = target_amount / inv_amount
    else:
        a = (((target_amount * i) / (inv_amount * (1 + i))) + 1)
    if inv_interval == 3:
        tenure_years = math.log(a, (1 + i))
        logger.debug(f"tenure years {tenure_years}")
        years, months = str(tenure_years).split('.')
        years = int(years)
        months = round(int(months[:2]) / 12)
    else:
        tenure_months = math.ceil(math.log(a, (1+i)))
        logger.debug(f"tenure months {tenure_months}")
        years, months = (tenure_months // 12), (tenure_months % 12)
    tenure_text = f"{years} years" + (f" {months} months" if months>0 else "")
    tenure_num = years + (months/10 if months>0 else 0)
    logger.info(f"Tenure: {tenure_text}")
    return tenure_num, tenure_text
