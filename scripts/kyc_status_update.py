import logging

from django.conf import settings
from django.core.mail import mail_admins
from core.decorators import timeit
from core.models import UserKYCRequest, KYCRequestStatusEnum

logger = logging.getLogger(__name__)


@timeit
def run():
    logger.info(":::::::::::::::::: cron:kyc_status_update start ::::::::::::::::::::")
    for r in UserKYCRequest.objects.filter(status = KYCRequestStatusEnum.Approval_Pending.value):
        logger.info(f"Updating KYC status for {r.kid}")
        r.add_or_update_kyc_detail()
    logger.info(":::::::::::::::::: cron:kyc_status_update end ::::::::::::::::::::")
    mail_admins(f"[{settings.ENV}]: KYC Status update cron ran", "Automated email")
