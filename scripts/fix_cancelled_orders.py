import datetime
import logging
import traceback


from django.conf import settings
from django.core.mail import mail_admins
from django.contrib.auth import get_user_model
from django.utils import timezone

from order.models import Order, OrderStatusEnum, BuySellEnum, BSEStarOrderStatusEnum

from api.rest import order_log

logger = logging.getLogger(__name__)
User = get_user_model()

ORDERS_MISSING = []
ORDERS_NOT_FOUND = []


def update_order_status(dt):
    logger.info(f"Updating cancelled orders for {dt}")
    status, message = order_log(from_date=dt, to_date=dt)
    if not status:
        logger.error(message)
        return
    if isinstance(message, dict):
        message = [message]
    logger.info(f"Found {len(message)} orders")
    for det in message:
        # {'b:ALLUnits': 'N', 'b:Amount': '1000.0000', 'b:BuySell': 'P', 'b:BuySellType': 'FRESH',
        #  'b:ClientCode': '9867215014', 'b:ClientName': 'DEVIN P JOSHI', 'b:DPC': 'Y', 'b:DPFolioNo': '24224181',
        #  'b:DPTrans': 'PHYSICAL', 'b:Date': '20/12/2022', 'b:EUIN': None, 'b:EUINDecl': 'N', 'b:EntryBy': '300523',
        #  'b:FirstOrder': 'N', 'b:FolioNo': '24224181/51', 'b:ISIN': 'INF109K01CQ1', 'b:InternalRefNo': None,
        #  'b:KYCFlag': 'Y', 'b:MINRedeemFlag': 'N', 'b:MemberCode': '29132', 'b:MemberRemarks': None,
        #  'b:OrderNumber': '586039741', 'b:OrderRemarks': 'ALLOTMENT DONE', 'b:OrderStatus': 'VALID',
        #  'b:OrderType': 'XSP', 'b:SIPRegnDate': '17/11/2022', 'b:SIPRegnNo': '38055338', 'b:SchemeCode': 'ICICI1477-GR',
        #  'b:SchemeName': 'ICICI PRUDENTIAL CORPORATE BOND FUND - GROWTH', 'b:SettNo': '2223178', 'b:SettType': 'T1',
        #  'b:SubBrCode': None, 'b:SubBrokerARNCode': None, 'b:SubOrderType': 'NRM', 'b:Time': '01:30:10',
        #  'b:Units': '0.0000'}
        logger.info(det)
        logger.info("++++++++++++++")
        if det['b:SIPRegnNo'] == '0':
            continue

        try:
            order = Order.objects.get(bsestar_order_number=det['b:OrderNumber'])
            if order.status == OrderStatusEnum.Completed.value:
                continue
        except Order.DoesNotExist:
            try:
                order = Order.objects.get(status=OrderStatusEnum.Cancelled.value,
                                          bsestar_sip_reg_no=det['b:SIPRegnNo'],
                                          instrument__bsestar_scheme_code=det['b:SchemeCode'],
                                          buy_sell=BuySellEnum.Buy.value,
                                          bsestar_order_number__isnull=True)
            except Order.DoesNotExist:
                try:
                    order = Order.objects.get(status=OrderStatusEnum.Upcoming.value,
                                              bsestar_sip_reg_no=det['b:SIPRegnNo'],
                                              instrument__bsestar_scheme_code=det['b:SchemeCode'],
                                              buy_sell=BuySellEnum.Buy.value,
                                              bsestar_order_number__isnull=True)
                except Order.DoesNotExist:
                    if det['b:OrderStatus'] == 'INVALID':
                        ORDERS_NOT_FOUND.append(det)
                    else:
                        ORDERS_MISSING.append(det)
                    continue
        logger.info(order)
        order.bsestar_order_number = det['b:OrderNumber']
        order.folio_number = det['b:FolioNo']
        order.bsestar_order_datetime = timezone.make_aware(
            datetime.datetime.strptime(f"{det['b:Date']} {det['b:Time']}",
                                       "%d/%m/%Y %H:%M:%S"))
        order.bsestar_settlement_number = det['b:SettNo']
        order.units = float(det['b:Units'])
        order.bsestar_settlement_type = det['b:SettType']
        order.bsestar_status = det['b:OrderStatus']
        order.order_remarks = det['b:OrderRemarks']
        if order.bsestar_status == BSEStarOrderStatusEnum.Valid.value:
            if order.order_remarks == "ALLOTMENT DONE":
                order.status = OrderStatusEnum.Completed.value
        else:
            order.status = OrderStatusEnum.Cancelled.value
        if order.status == OrderStatusEnum.Cancelled.value:
            order.cancelled_on = order.bsestar_order_datetime
            order.cancelled_reason = order.order_remarks
        elif order.status == OrderStatusEnum.Completed.value:
            order.update_units_allotment(save=False)
        order.save()
        logger.info("*****************************")
    return True


def run():
    for dt in [datetime.date(2022,11,1) + datetime.timedelta(days=idx) for idx in range(61)]:
       _ = update_order_status(dt)
    logger.info("===================ORDERS MISSING============================")
    logger.info(ORDERS_MISSING)
    logger.info("===================ORDERS NOT FOUND============================")
    logger.info(ORDERS_NOT_FOUND)
