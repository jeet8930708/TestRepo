import pandas as pd
from master.models import Country, State, Occupation, Income, SourceOfWealth
from django.conf import settings


def update_occupations(df_occ):
    print("Updating occupations")
    for _, row in df_occ.iterrows():
        name, code, occ_type = row['name'], str(row['code']).zfill(2), row['type']
        print(name, code, occ_type)
        try:
            occ = Occupation.objects.get(name=name)
            occ.code = code
            occ.type = occ_type
        except Occupation.DoesNotExist:
            occ = Occupation(code=code, name=name, type=occ_type)
        occ.save()


def update_country(df_count):
    print("Updating countries")
    for _, row in df_count.iterrows():
        name, code = row['name'], str(row['code']).zfill(3)
        print(name, code)
        try:
            country = Country.objects.get(name=name)
            country.code = code
        except Country.DoesNotExist:
            country = Country(code=code, name=name)
        country.save()


def update_country_text_code(df_count):
    print("Updating countries")
    for _, row in df_count.iterrows():
        name, code = row['name'], str(row['code'])
        print(name, code)
        try:
            country = Country.objects.get(name=name)
            country.code_text = code
        except Country.DoesNotExist:
            country = Country(code_text=code, name=name)
        country.save()


def update_state(df_state):
    print("Updating states")
    for _, row in df_state.iterrows():
        name, code = row['name'], str(row['code']).zfill(2)
        print(name, code)
        try:
            state = State.objects.get(name=name)
            state.code = code
        except State.DoesNotExist:
            state = State(code=code, name=name)
        state.country = Country.objects.get(name="India")
        state.save()


def update_income(df_inc):
    print("Updating income")
    for _, row in df_inc.iterrows():
        name, code = row['name'], str(row['code']).zfill(2)
        print(name, code)
        try:
            inc = Income.objects.get(bucket=name)
            inc.code = code
        except Income.DoesNotExist:
            inc = Income(code=code, bucket=name)
        inc.save()


def update_sow(df_sow):
    print("Updating source of wealth")
    for _, row in df_sow.iterrows():
        name, code = row['name'], str(row['code']).zfill(2)
        print(name, code)
        try:
            sow = SourceOfWealth.objects.get(name=name)
            sow.code = code
        except SourceOfWealth.DoesNotExist:
            sow = SourceOfWealth(code=code, name=name)
        sow.save()


def run():
    xlsx = pd.ExcelFile(settings.DATA_DIR / 'bsestar/masters.xlsx')
    df_occ = pd.read_excel(xlsx, 'Occupation')
    update_occupations(df_occ)

    df_count = pd.read_excel(xlsx, 'Country')
    update_country(df_count)

    df_state = pd.read_excel(xlsx, 'State')
    update_state(df_state)

    df_inc = pd.read_excel(xlsx, 'Income')
    update_income(df_inc)

    df_sow = pd.read_excel(xlsx, 'Source of Wealth')
    update_sow(df_sow)

    df_count = pd.read_excel(xlsx, 'Country Text Code')
    update_country_text_code(df_count)

