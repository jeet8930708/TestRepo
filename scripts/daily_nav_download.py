import logging
import traceback

from django.conf import settings
from django.core.mail import mail_admins
from django.utils import timezone
from core.decorators import timeit
from investment.models import Instrument
from utils.investment_util import fetch_index_nav_from_morning_star, fetch_growth_10k

logger = logging.getLogger(__name__)

@timeit
def run():
    logger.info(":::::::::::::::::: cron:daily_nav_download start ::::::::::::::::::::")
    for inst in Instrument.objects.filter(is_active=True):
        try:
            inst.fetch_current_nav_price()
            inst.fetch_detail_morningstar()
            #if timezone.now().day == 1:
            fetch_growth_10k(inst.mstarid)
        except Exception as e:
            traceback.print_exc()
            mail_admins(f"[{settings.ENV}] Error: Instrument NAV update for {inst.name}", str(e))
    try:
        fetch_index_nav_from_morning_star()
    except Exception as e:
        traceback.print_exc()
        mail_admins(f"[{settings.ENV}] Error: Growth 10k Index NAV update", str(e))
    logger.info(":::::::::::::::::: cron:daily_nav_download end ::::::::::::::::::::")
    mail_admins(f"[{settings.ENV}]: Daily NAV Download cron ran", "Automated email")
