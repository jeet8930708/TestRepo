import logging
import traceback

from django.conf import settings
from django.core.mail import mail_admins
from order.models import OrderLumpsum, OrderLumpsumStatusEnum, SIPMandate, SIPMandateStatusEnum, Order, OrderStatusEnum

logger = logging.getLogger(__name__)


def run():
    logger.info(f"Updating payment for lumpsum orders")
    for order in OrderLumpsum.objects.filter(status=OrderLumpsumStatusEnum.Pending.value):
        logger.info(f"Updating payment status for {order.id}-{order.order_numbers}")
        try:
            order.update_payment_status()
        except Exception as e:
            traceback.print_exc()
            mail_admins(f"[{settings.ENV}] Error: BSEStar lumpsum payment status update for {order.id}", str(e))

    logger.info(f"Updating payment for SIP orders")
    for mandate in SIPMandate.objects.filter(status=SIPMandateStatusEnum.Pending.value):
        logger.info(f"Updating payment status for {mandate.id}-{mandate.order_numbers}")
        try:
            mandate.update_payment_status()
        except Exception as e:
            traceback.print_exc()
            mail_admins(f"[{settings.ENV}] Error: BSEStar SIP Mandate payment status update for {order.id}", str(e))
    
    logger.info(f"Updating allotment for completed orders")
    for order in Order.objects.filter(status=OrderStatusEnum.Completed.value, units=0.0):
        logger.info(f"Updating allotment for {order.id}-{order.bsestar_order_number}")
        try:
            order.update_units_allotment()
        except Exception as e:
            traceback.print_exc()
            mail_admins(f"[{settings.ENV}] Error: BSEStar units allotment update for {order.id}", str(e))
