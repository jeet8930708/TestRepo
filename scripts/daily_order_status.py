import datetime
import logging
import traceback


from django.conf import settings
from django.core.mail import mail_admins
from django.contrib.auth import get_user_model
from django.utils import timezone

from order.models import Order, OrderStatusEnum, BuySellEnum, BSEStarOrderStatusEnum, SIP

from api.rest import order_log

logger = logging.getLogger(__name__)
User = get_user_model()


def update_pending_orders():
    logger.info(f"Updating pending orders for {timezone.localdate()}")
    for order in Order.objects.filter(status=OrderStatusEnum.Pending.value, order_date__lt=timezone.localdate()):
        logger.info(f"daily_order_status: updating status for {order.id}")
        try:
            order.update_bsestar_pending_order_status()
        except Exception as e:
            traceback.print_exc()
            mail_admins(f"[{settings.ENV}] Error: BSEStar order status update for {order.id}", str(e))


def update_upcoming_orders():
    logger.info(f"Updating upcoming orders for {timezone.localdate()}")
    status, message = order_log(from_date=timezone.localdate(), to_date=timezone.localdate())
    #import datetime
    #status, message = order_log(from_date=datetime.date(2023,1,19), to_date=datetime.date(2023,1,22))
    if not status:
        logger.error(message)
        return
    if isinstance(message, dict):
        message = [message]
    logger.info(f"Found {len(message)} orders")
    for det in message:
        # {'b:ALLUnits': 'N', 'b:Amount': '1000.0000', 'b:BuySell': 'P', 'b:BuySellType': 'FRESH',
        #  'b:ClientCode': '9867215014', 'b:ClientName': 'DEVIN P JOSHI', 'b:DPC': 'Y', 'b:DPFolioNo': '24224181',
        #  'b:DPTrans': 'PHYSICAL', 'b:Date': '20/12/2022', 'b:EUIN': None, 'b:EUINDecl': 'N', 'b:EntryBy': '300523',
        #  'b:FirstOrder': 'N', 'b:FolioNo': '24224181/51', 'b:ISIN': 'INF109K01CQ1', 'b:InternalRefNo': None,
        #  'b:KYCFlag': 'Y', 'b:MINRedeemFlag': 'N', 'b:MemberCode': '29132', 'b:MemberRemarks': None,
        #  'b:OrderNumber': '586039741', 'b:OrderRemarks': 'ALLOTMENT DONE', 'b:OrderStatus': 'VALID',
        #  'b:OrderType': 'XSP', 'b:SIPRegnDate': '17/11/2022', 'b:SIPRegnNo': '38055338', 'b:SchemeCode': 'ICICI1477-GR',
        #  'b:SchemeName': 'ICICI PRUDENTIAL CORPORATE BOND FUND - GROWTH', 'b:SettNo': '2223178', 'b:SettType': 'T1',
        #  'b:SubBrCode': None, 'b:SubBrokerARNCode': None, 'b:SubOrderType': 'NRM', 'b:Time': '01:30:10',
        #  'b:Units': '0.0000'}
        logger.info(det)
        logger.info("++++++++++++++")
        if det['b:SIPRegnNo'] == '0':
            continue

        try:
            order = Order.objects.get(bsestar_order_number=det['b:OrderNumber'])
            if order.status == OrderStatusEnum.Completed.value:
                continue
        except Order.DoesNotExist:
            try:
                order = Order.objects.get(status=OrderStatusEnum.Upcoming.value,
                                          bsestar_sip_reg_no=det['b:SIPRegnNo'],
                                          instrument__bsestar_scheme_code=det['b:SchemeCode'],
                                          buy_sell=BuySellEnum.Buy.value,
                                          bsestar_order_number__isnull=True)
            except Order.DoesNotExist:
                try:
                    sip = SIP.objects.get(bsestar_xsip_reg_id=det['b:SIPRegnNo'])
                    order = Order(user=User.objects.get(username=det['b:ClientCode']),
                                  sip=sip,
                                  order_date=datetime.datetime.strptime(f"{det['b:Date']}", "%d/%m/%Y"),
                                  bsestar_sip_reg_no=sip.bsestar_xsip_reg_id,
                                  instrument=sip.instrument,
                                  buy_sell=BuySellEnum.Buy.value,
                                  amount=sip.sip_amount)

                except SIP.DoesNotExist:
                    mail_admins(f"[{settings.ENV}] Error: BSEStar upcoming order missing for {det['b:SIPRegnNo']}", str(det))
                    continue
        logger.info(order)
        order.bsestar_order_number = det['b:OrderNumber']
        order.folio_number = det['b:FolioNo']
        order.bsestar_order_datetime = timezone.make_aware(
            datetime.datetime.strptime(f"{det['b:Date']} {det['b:Time']}",
                                       "%d/%m/%Y %H:%M:%S"))
        order.bsestar_settlement_number = det['b:SettNo']
        order.units = float(det['b:Units'])
        order.bsestar_settlement_type = det['b:SettType']
        order.bsestar_status = det['b:OrderStatus']
        order.order_remarks = det['b:OrderRemarks']
        if order.bsestar_status == BSEStarOrderStatusEnum.Valid.value:
            if order.order_remarks == "ALLOTMENT DONE":
                order.status = OrderStatusEnum.Completed.value
            elif order.order_remarks == "SENT TO RTA FOR VALIDATION":
                order.status = OrderStatusEnum.Pending.value
        else:
            order.status = OrderStatusEnum.Cancelled.value
        if order.status == OrderStatusEnum.Cancelled.value:
            order.cancelled_on = order.bsestar_order_datetime
            order.cancelled_reason = order.order_remarks
        elif order.status == OrderStatusEnum.Completed.value:
            order.update_units_allotment(save=False)
        order.save()
        logger.info("*****************************")


def run():
    update_pending_orders()
    update_upcoming_orders()
