import pandas as pd
from django.conf import settings
from master.models import Bank, BankBranch


def run():
    df = pd.read_csv(settings.DATA_DIR / 'bank/ifsc.csv')
    for idx, row in df.iterrows():
        print(f"Bank: {row['BANK']} - {row['IFSC']}")
        bank, _ = Bank.objects.get_or_create(name=row['BANK'])
        try:
            branch = BankBranch.objects.get(bank=bank, ifsc=row['IFSC'])
        except BankBranch.DoesNotExist:
            branch = BankBranch(bank=bank, ifsc=row['IFSC'])
        branch.centre = row['CENTRE']
        branch.branch = row['BRANCH']
        branch.district = row['DISTRICT']
        branch.state = row['STATE']
        branch.address = row['ADDRESS']
        branch.contact = row['CONTACT']
        branch.imps = row['IMPS']
        branch.rtgs = row['RTGS']
        branch.city = row['CITY']
        branch.neft = row['NEFT']
        branch.micr = row['MICR']
        branch.upi = row['UPI']
        branch.swift = row['SWIFT']
        branch.save()
