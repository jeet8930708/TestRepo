import logging
import pandas as pd

from functools import reduce
from itertools import permutations, chain
from django.conf import settings

logger = logging.getLogger(__name__)


def combination_sum(arr, total, max_len):
    """
    Getting combination of numbers that sums up to total
    Params:
        arr: list of integers to be added for getting total e.g.: [0, 5, 10, 15, 25, 30, 35, 40, 45, 50]
        total: total sum Eg.: 50
        max_len: maximum size of an array of each combination e.g.: 5
    """
    ans = []
    temp = []

    # first do hashing nothing but set{}
    # since set does not always sort
    # removing the duplicates using Set and
    # Sorting the List
    arr = sorted(list(set(arr)))
    find_numbers(ans, arr, temp, total, 0, max_len)
    return ans


def find_numbers(ans, arr, temp, total, index, max_len):
    """
    Recursive function to find numbers that adds up to total
    """
    if (total == 0) and len(temp) == max_len:
        # Adding deep copy of list to ans
        ans.append(list(temp))
        return

    # Iterate from index to len(arr) - 1
    for i in range(index, len(arr)):

        # checking that sum does not become negative
        if (total - arr[i]) >= 0 and len(temp) < max_len:
            # adding element which can contribute to
            # sum
            temp.append(arr[i])
            find_numbers(ans, arr, temp, total - arr[i], i, max_len)

            # removing element from list (backtracking)
            temp.remove(arr[i])


def perm(x):
    return set(permutations(x, len(x)))


def derive_age_bucket(age):
    if age <= 25:
        return 'Below 25'
    elif 25 < age < 35:
        return '26 to 35'
    elif 35 <= age < 55:
        return '35 to 55'
    elif age >= 55:
        return '55 and above'


def derive_inv_bucket(amount):
    if amount <= 2000:
        return 'Below 2000'
    elif 2000 < amount <= 6000:
        return '2001-6000'
    elif 6000 < amount <= 15000:
        return '6001-15000'
    elif amount > 15000:
        return '15001 & Above'


def derive_risk_bucket(risk):
    risk_profile_mapping = {
        1: 'Low',
        2: 'Medium',
        3: 'High'
    }
    return risk_profile_mapping.get(risk)


def derive_inv_interval_bucket(interval):
    inv_interval_mapping = {
        1: 'Monthly',
        2: 'Weekly',
        3: 'Lumpsum',
    }
    return inv_interval_mapping.get(interval)


class RecommendationEngine:

    DATA_ROOT_DIR = settings.DATA_DIR / 'investment/recommendation'
    MUTUAL_FUND_PATH = DATA_ROOT_DIR / 'mutual_fund.csv'
    MAX_DRAWDOWN_PATH = DATA_ROOT_DIR / f'max_drawdown_mapping.csv'
    MONTHLY_RETURN_PATH = DATA_ROOT_DIR / f'monthly_return_{settings.MUTUAL_FUNDS_COUNT}.csv'
    INVESTMENT_RATIO_PATH = DATA_ROOT_DIR / f'investment_ratio_{settings.MUTUAL_FUNDS_COUNT}.csv'
    EXP_RETURN_PATH = DATA_ROOT_DIR / f'exp_return_{settings.MUTUAL_FUNDS_COUNT}.csv'
    DRAWDOWN_PATH = DATA_ROOT_DIR / f'drawdown_{settings.MUTUAL_FUNDS_COUNT}.csv'
    RECOMMENDATION_PATH = DATA_ROOT_DIR / f'recommendation_{settings.MUTUAL_FUNDS_COUNT}.csv'

    def __init__(self, mf_count, age, risk_profile, inv_interval, inv_amount, **kwargs):
        self.mf_count = int(mf_count)
        self.age = int(age)
        self.age_bucket = derive_age_bucket(self.age)
        self.risk_profile = derive_risk_bucket(int(risk_profile))
        self.inv_interval = derive_inv_interval_bucket(int(inv_interval))
        self.inv_amount = float(inv_amount)
        self.inv_bucket = derive_inv_bucket(self.inv_amount)
        self.df_mf_master = pd.read_csv(self.MUTUAL_FUND_PATH)
        self.df_max_drawdown = pd.read_csv(self.MAX_DRAWDOWN_PATH)

        logger.info(f"Total MFs: {self.mf_count}"
                    f"| Age: {self.age}"
                    f"| Age Bucket: {self.age_bucket}"
                    f"| Risk Profile: {self.risk_profile}"
                    f"| Inv Interval: {self.inv_interval}"
                    f"| Inv Amount: {self.inv_amount}"
                    f"| Inv Bucket: {self.inv_bucket}")

    def generate_investment_ratio_data(self):
        """
        Generates all permutations of 100% ratio split across mutual funds and saves locally
        Return:
            DataFrame
        """
        arr = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100]
        ans = combination_sum(arr, 100, self.mf_count)
        logger.info(f"Total combinations -> {len(ans)}")

        final = list(map(perm, ans))
        split_final = list(chain(*final))
        logger.info(f"Total permutations -> {len(split_final)}")

        df_split = pd.DataFrame(split_final, columns=list(map(str, range(1, 9))))
        df_split.index = df_split.apply(lambda row: reduce(lambda x, y: str(x) + '-' + str(y), row), axis=1)
        df_split.index.name = "SPLIT_RATIO"
        df_split.to_csv(self.INVESTMENT_RATIO_PATH)
        return df_split

    def fetch_investment_ratio_data(self):
        return pd.read_csv(self.INVESTMENT_RATIO_PATH, index_col='SPLIT_RATIO')

    def generate_expected_return_data(self):
        """
        Derived expected return total for each combination
        """
        # get split ratios
        df_split = self.fetch_investment_ratio_data()

        # multiply split ratios with expected return
        series_return = self.df_mf_master['Expected Return']
        series_return.index = list(map(str, range(1, self.mf_count+1)))

        df_exp_return = df_split * series_return / 100

        # calculate max return
        df_exp_return['EXP_RETURN'] = round(df_exp_return.sum(axis=1), 2)

        df_exp_return.to_csv(self.EXP_RETURN_PATH)
        return df_exp_return

    def fetch_expected_return_data(self):
        return pd.read_csv(self.EXP_RETURN_PATH, index_col='SPLIT_RATIO')

    @staticmethod
    def clean_monthly_return_data(df_monthly_return):
        # replace NaN by 0
        df_monthly_return.fillna(0.0, inplace=True)

        # discard row where all mutual funds return is greater than 0.0
        df_monthly_return = df_monthly_return.loc[df_monthly_return.apply(lambda row: any([v<0.0 for v in row]), axis=1)]

        return df_monthly_return

    @staticmethod
    def derive_max_drawdown(row_split_ratio, df_monthly_return):
        df_split_drawdown = row_split_ratio * df_monthly_return / 100
        df_split_drawdown['DRAWDOWN'] = df_split_drawdown.sum(axis=1)
        return abs(round(df_split_drawdown['DRAWDOWN'].min(), 2))

    def generate_max_drawdown_data(self):
        df_monthly_return = pd.read_csv(self.MONTHLY_RETURN_PATH, index_col="MONTHLY_RETURN")
        logger.info(f"Max drawdown master :::: {len(df_monthly_return)}")

        df_monthly_return = self.clean_monthly_return_data(df_monthly_return)
        logger.info(f"Max drawdown master after cleanup :::: {len(df_monthly_return)}")

        df_split = self.fetch_investment_ratio_data()
        logger.info(f"Fetched investment ratios :::: {len(df_split)}")

        logger.info(f"Deriving max drawdown START")
        df_split['MAX_DRAWDOWN'] = 0.0

        for idx, row in df_split.iterrows():
            logger.info(f"Deriving Max Drawdown for {idx}")
            max_drawdown = self.derive_max_drawdown(row, df_monthly_return)
            df_split.at[idx, 'MAX_DRAWDOWN'] = max_drawdown
            logger.info(f"Max Drawdown for {idx} = {max_drawdown}")

        logger.info(f"Deriving max drawdown END")
        df_split.to_csv(self.DRAWDOWN_PATH)

        return df_split

    def fetch_max_drawdown_data(self):
        return pd.read_csv(self.DRAWDOWN_PATH, index_col='SPLIT_RATIO')

    def generate_recommendation_data(self):
        df_split = self.fetch_investment_ratio_data()
        df_exp_return = self.fetch_expected_return_data()
        df_max_drawdown = self.fetch_max_drawdown_data()
        df_recommend = df_split.join(df_exp_return[["EXP_RETURN"]]).join(df_max_drawdown[["MAX_DRAWDOWN"]])
        df_recommend.to_csv(self.RECOMMENDATION_PATH)
        return df_recommend

    def fetch_recommendation_data(self):
        return pd.read_csv(self.RECOMMENDATION_PATH, index_col='SPLIT_RATIO')

    def generate_data(self):
        self.generate_investment_ratio_data()
        self.generate_expected_return_data()
        self.generate_max_drawdown_data()
        self.generate_recommendation_data()

    def get_drawdown_threshold(self):
        df = self.df_max_drawdown
        return df.loc[(df['AGE'] == self.age_bucket)\
                      & (df['RISK_PROFILE'] == self.risk_profile) \
                      & (df['INVESTMENT_AMOUNT'] == self.inv_bucket), 'MAX_DRAWDOWN'].tolist()[0]

    def discard_split_below_min_inv_amt(self, df):
        min_inv_values = self.df_mf_master[['Fund No', 'Min Inv Amount']].values.tolist()
        query = ' & '.join([f'((@df["{fund_no}"] == 0) | (@df["{fund_no}"] > 0 & @df["{fund_no}"] >= {min_amt}))' for fund_no, min_amt in min_inv_values])
        logger.info(query)
        df = df.query(query)
        logger.info(len(df), df.head())
        return df

    def recommend(self):
        # step 1: Fetch recommendation data
        df = self.fetch_recommendation_data()
        logger.info(f"Total recommendations ::::: {len(df)}")

        # step 2: Getch drawdown threshold
        drawdown_threshold = self.get_drawdown_threshold()
        logger.info(f"Drawdown Threshold :::: {drawdown_threshold}")

        # step 3: Discard splits exceeding max drawdown threshold
        df = df.loc[df['MAX_DRAWDOWN'] <= drawdown_threshold]
        logger.info(f"Total recommendations after discarding ones exceeding drawdown threshold ::::: {len(df)}")

        # step 4: Allocate amount to each fund
        for n in self.df_mf_master['Fund No'].values.tolist():
            df[str(n)] = df[str(n)] * self.inv_amount / 100

        # step 5: Discard splits which does not satisfy min inv amount criteria for each mutual fund
        df = self.discard_split_below_min_inv_amt(df)
        df['EXP_RETURN'] = df['EXP_RETURN'].astype(float)
        df['MAX_DRAWDOWN'] = df['MAX_DRAWDOWN'].astype(float)
        df.sort_values(['EXP_RETURN', 'MAX_DRAWDOWN'], ascending=False, inplace=True)
        df.reset_index(inplace=True, drop=True)
        logger.info(f"Total splits after discarding min inv amount criteria ::::: {len(df)}")

        # step 6: Return split with maximum return
        mf_max_return = df.iloc[0]
        logger.info(f"Splits with max return {mf_max_return}")

        inv_split = []
        for mf in self.df_mf_master['Fund No'].values.tolist():
            fund_det = self.df_mf_master.loc[self.df_mf_master['Fund No'] == mf].values.tolist()[0]
            # logger.info(fund_det)
            split_amount = mf_max_return[mf-1]
            if split_amount > 0.0:
                inv_split.append({
                    'fund_no': mf,
                    'fund_name': fund_det[1],
                    'inv_amount': split_amount,
                    'inv_split_percent': round(split_amount * 100 / self.inv_amount, 2),
                    'exp_return': fund_det[5],
                })
        result = {
            "investment_recommendation": inv_split,
            "expected_return": mf_max_return[-2],
            "max_drawdown": mf_max_return[-1]
        }
        logger.info(f"Recommendation: {result}")
        return result


# def run(mf_count, age, risk_profile, inv_interval, inv_amount):
#     eng = RecommendationEngine(mf_count, age, risk_profile, inv_interval, inv_amount)
#     return eng.recommend()

def run():
    from recommendation.rule_engine import RuleEngine
    r = RuleEngine()
    r.build_rules()




