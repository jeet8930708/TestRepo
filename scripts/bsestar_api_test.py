from api.bse_star import (
    ucc_registration,
    fatca_upload,
    aof_image_upload,
    order_log,
)


def api_ucc_registration(u):
    print(ucc_registration(u))


def api_fatca_upload(u):
    print(fatca_upload(u))


def api_aof_image_upload(u):
    print(aof_image_upload(u))


def run():
    from core.models import User
    u = User.objects.get(username='9833854753')
    import datetime
    order_log(u, datetime.date(2022,7,1), datetime.date(2022,7,31))
