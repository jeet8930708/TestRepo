import pandas as pd

from django.conf import settings
from investment.models import Instrument, InstrumentType


def run():
    Instrument.objects.all().update(is_active=False)
    df = pd.read_csv(settings.DATA_DIR / "investment/mutual_funds_09082022.csv")
    inst_type = InstrumentType.objects.get(name='Mutual Fund')
    for idx, row in df.iterrows():
        try:
            inst = Instrument.objects.get(isin=row['ISIN'])
            inst.is_active = True
            inst.save()
            print(f"{row['Name']} - {row['ISIN']} exists !")
        except Instrument.DoesNotExist:
            inst = Instrument.objects.create(name=row['Name'],
                                             isin=row['ISIN'],
                                             instrument_type=inst_type)
            print(f"{row['Name']} - {row['ISIN']} created !")
