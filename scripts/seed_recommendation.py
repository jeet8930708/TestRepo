import pandas as pd

from recommendation.models import Basket, RecommendationRule, RiskProfileEnum
from django.conf import settings


def seed_baskets():
    df = pd.read_csv(settings.DATA_DIR / 'seed/recommendation_baskets.csv')
    for _, row in df.iterrows():
        print(f"Adding {row['Name']}")
        try:
            basket = Basket.objects.get(name=row['Name'])
        except Basket.DoesNotExist:
            basket = Basket(name=row['Name'])
        basket.description = row['Description']
        basket.save()


def seed_rules():
    df = pd.read_csv(settings.DATA_DIR / 'seed/recommendation_rules.csv')
    risk_profile_mapping = {
        'High': RiskProfileEnum.High.value,
        'Medium': RiskProfileEnum.Medium.value,
        'Low': RiskProfileEnum.Low.value
    }
    for _, row in df.iterrows():
        print(f"Adding {row}")
        try:
            rule = RecommendationRule.objects.get(age_from=row['Age From'],
                                                  age_to=row['Age To'],
                                                  risk_profile=risk_profile_mapping[row['Risk Profile']],
                                                  inv_amount_from=row['Inv From'],
                                                  inv_amount_to=row['Inv To'])
        except RecommendationRule.DoesNotExist:
            rule = RecommendationRule(age_from=row['Age From'],
                                      age_to=row['Age To'],
                                      risk_profile=risk_profile_mapping[row['Risk Profile']],
                                      inv_amount_from=row['Inv From'],
                                      inv_amount_to=row['Inv To'])
        rule.age_range = row['Age Range']
        rule.max_drawdown = row['Max Drawdown']
        rule.max_return = row['Max Return']
        rule.basket = Basket.objects.get(name=row['Basket'])
        rule.save()


def run():
    seed_baskets()
    seed_rules()
