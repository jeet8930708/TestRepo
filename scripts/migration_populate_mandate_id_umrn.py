from order.models import Mandate, MandateStatusEnum
from api.models import ApiLog
from ast import literal_eval

def run():
    for m in Mandate.objects.filter(status=MandateStatusEnum.Success.value):
        if m.web_url:
            m.enach_id = m.web_url.split('/')[6]
            print("Enach ID >>>>>", m.enach_id)
            m.save()
