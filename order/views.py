import logging

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect

from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveAPIView, ListAPIView, CreateAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.serializers import ValidationError

from core.models import UserBankDetail
from core.custom_permissions import IsOwner
from utils.response import response_200, response_400_bad_request
from recommendation.models import UserRecommendation

from .enums import BuySellEnum, PaymentModeEnum, OrderStatusEnum
from .models import Mandate, MandateStatusEnum, Order, SIP, OrderLumpsum, SIPMandate
from .serializers import (
    MandateSerializer,
    OrderListSerializer,
    OrderSerializer,
    OrderSellSIPCreateSerializer,
    SIPRedeemSerializer,
    OrderLumpsumCreateSerializer,
    LumpsumRedeemSerializer,
    OrderSellLumpsumCreateSerializer,
)

logger = logging.getLogger('api')


class SIPStartDatesView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        obj = get_object_or_404(UserRecommendation, pk=self.kwargs['recid'], user=request.user)
        return response_200("SIP start dates fetched successfully", {'valid_sip_start_dates': obj.sip_valid_start_dates})


class MandateCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = MandateSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Mandate request submitted successfully", response.data)


class MandateBSEStarURLView(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    queryset = Mandate.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(user=self.request.user)

    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        status, msg = obj.get_bsestar_emandate_auth_url()
        return response_200("Link generated" if status else "Link not generated", {"web_url": msg if status else ""})


class MandateStatusView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        mandate = get_object_or_404(Mandate, pk=request.data['mandate_id'], user=request.user,
                                    status=MandateStatusEnum.Partial.value)
        mandate.update_data(
            {"status": MandateStatusEnum.Success.value if request.data['status'] == "success" else MandateStatusEnum.Fail.value}
        )
        if request.data['status'] == "success":
            sip_mandate = SIPMandate.objects.create_from_mandate(mandate)
            return response_200("Auto-pay setup is successful", sip_mandate.summary)
        return response_400_bad_request("Auto-pay setup failed")


class OrderListView(ListAPIView):
    permission_classes = (IsAuthenticated, IsOwner)
    queryset = Order.objects.all()
    serializer_class = OrderListSerializer

    def get_queryset(self):
        logger.info(f"Fetching transactions for {self.kwargs} - {self.request.query_params}")
        queryset = super().get_queryset()
        if "status" not in self.kwargs:
            raise ValidationError("Invalid Request")
        queryset = queryset.filter(user=self.request.user, status=self.kwargs["status"])
        buy_sell = self.request.query_params.get("type", "all")
        if buy_sell == "buy":
            queryset = queryset.filter(buy_sell=BuySellEnum.Buy.value)
        elif buy_sell == "sell":
            queryset = queryset.filter(buy_sell=BuySellEnum.Sell.value)
        return queryset

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("Transactions fetched successfully", response.data)


class OrderDetailView(RetrieveAPIView):
    permission_classes = (IsAuthenticated, IsOwner)
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("Order details fetched successfully", response.data)


class SIPStopView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        obj = get_object_or_404(SIP, pk=request.data['sip_id'], user=request.user)
        success, message = obj.cancel()
        if success:
            return response_200(message, request.data)
        return response_400_bad_request(message)


class SIPRedeemDetailView(RetrieveAPIView):
    permission_classes = (IsAuthenticated, IsOwner,)
    serializer_class = SIPRedeemSerializer
    queryset = SIP.objects.filter(order_created=True)

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("SIP Redeem details fetched successfully", response.data)


class LumpsumRedeemDetailView(RetrieveAPIView):
    permission_classes = (IsAuthenticated, IsOwner,)
    serializer_class = LumpsumRedeemSerializer
    queryset = Order.objects.filter(status=OrderStatusEnum.Completed.value,
                                    sip__isnull=True,
                                    order_lumpsum__isnull=False,
                                    buy_sell=BuySellEnum.Buy.value)

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("Lumpsum Redeem details fetched successfully", response.data)


class OrderSellSIPCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderSellSIPCreateSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Order submitted successfully", response.data)


class OrderBuyLumpsum(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderLumpsumCreateSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Lumpsum order billdesk request msg generated succesfully", response.data)


class OrderBuyPGResponse(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        logger.info(request.data)
        return HttpResponseRedirect(settings.BSESTAR_PG_REDIRECT_URL + f'?status=success')


class OrderSellLumpsumCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = OrderSellLumpsumCreateSerializer

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Order submitted successfully", response.data)


class OrderLumpsumPaymentStatusView(APIView):
    permission_classes = (IsAuthenticated, IsOwner)

    def post(self, request, *args, **kwargs):
        order = get_object_or_404(OrderLumpsum, pk=request.data['order_id'])
        _ = order.update_payment_status()
        return response_200("Order payment status fetched successfully", {'payment_status': order.get_status_display()})


class OrderSIPPaymentGatewayView(APIView):
    permission_classes = (IsAuthenticated, IsOwner)

    def post(self, request, *args, **kwargs):
        sip_mandate = get_object_or_404(SIPMandate, pk=request.data['mandate_id'], user=request.user)
        sip_mandate.payment_mode = int(request.data['payment_mode'])

        try:
            user_bank = UserBankDetail.objects.get(id=request.data['bank'], user=request.user)
        except UserBankDetail.DoesNotExist:
            raise ValidationError(f"Invalid bank value: {request.data['bank']}")
        bank = user_bank.bank.bank
        sip_mandate.bank_account_number = user_bank.account_number
        sip_mandate.bank_name = bank.name
        sip_mandate.bank_code = bank.code_pg
        sip_mandate.bank_ifsc = user_bank.bank.ifsc
        if sip_mandate.payment_mode == PaymentModeEnum.UPI.value:
            sip_mandate.vpa_id = request.data['vpa_id']
        sip_mandate.save()
        return response_200("Payment gateway fetched", {"id": sip_mandate.id,
                                                        "pg_response": sip_mandate.get_payment_gateway_response()})


class OrderSIPPGResponse(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        logger.info(f"OrderSIPPGResponse: {request.data}")
        return HttpResponseRedirect(settings.BSESTAR_PG_REDIRECT_URL + f'?status=success')


class OrderSIPPaymentStatusView(APIView):
    permission_classes = (IsAuthenticated, IsOwner)

    def post(self, request, *args, **kwargs):
        sip_mandate = get_object_or_404(SIPMandate, pk=request.data['mandate_id'])
        _ = sip_mandate.update_payment_status()
        return response_200("Mandate payment status fetched successfully",
                            {'payment_status': sip_mandate.get_status_display()})
