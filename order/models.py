import datetime
import logging
import numpy as np
import pandas as pd
import time

from django.core.mail import mail_admins
from django.conf import settings
from django.db import models
from django.db.models import F, Sum
from django.db.transaction import atomic
from django.core.validators import MinValueValidator
from django.contrib.auth import get_user_model
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from rest_framework.serializers import ValidationError
from dateutil import relativedelta

from core.enums import BankAccountTypeEnum
from core.models import UserBankDetail
from investment.models import Instrument
from recommendation.models import Basket, RiskProfileEnum, InvestmentIntervalEnum, UserRecommendation
from api.rest import (
    xsip_order_entry,
    bsestar_registration,
    mandate_register_bsestar,
    emandate_auth_url,
    order_buy_sell,
    payment_gateway,
    get_child_order_details,
    order_payment_status,
    allotment_statement,
    redemption_statement,
    order_log,
    redeem_2fa,
)

from utils.order_util import (
    generate_bsestar_unique_ref_no,
)


from .enums import (
    MandateFrequencyEnum,
    MandateCategoryEnum,
    MandateStatusEnum,
    MandateInstrumentTypeEnum,
    BuySellEnum,
    BSEStarOrderStatusEnum,
    OrderStatusEnum,
    OrderLumpsumStatusEnum,
    PaymentModeEnum,
    SIPMandateStatusEnum,
)

User = get_user_model()
logger = logging.getLogger(__name__)


# Create your models here.
class Mandate(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='mandates')

    bank_ifsc = models.CharField(max_length=11)
    bank_name = models.CharField(max_length=140)
    bank_code = models.CharField(max_length=5, blank=True, null=True)
    management_category = models.CharField(max_length=4, choices=MandateCategoryEnum.choices(),
                                           default=MandateCategoryEnum.Mutual_Fund_Payment.value)
    customer_account_number = models.CharField(max_length=18)
    customer_account_type = models.CharField(max_length=2, choices=BankAccountTypeEnum.choices())
    customer_name = models.CharField(max_length=40)
    instrument_type = models.CharField(max_length=6, choices=MandateInstrumentTypeEnum.choices(),
                                       default=MandateInstrumentTypeEnum.Debit.value)
    maximum_amount = models.FloatField()
    is_recurring = models.BooleanField(default=True)
    frequency = models.CharField(max_length=10, choices=MandateFrequencyEnum.choices())
    first_collection_date = models.DateField()
    web_url = models.CharField(max_length=300)
    mandate_id_bsestar = models.CharField(max_length=15, blank=True, null=True)
    enach_id = models.CharField(max_length=40, blank=True, null=True)
    status = models.CharField(max_length=15, choices=MandateStatusEnum.choices(),
                              default=MandateStatusEnum.Partial.value)
    recommendation = models.ForeignKey(UserRecommendation, on_delete=models.SET_NULL, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    cancelled = models.BooleanField(default=False)
    cancelled_on = models.DateTimeField(blank=True, null=True)
    cancelled_reason = models.TextField(blank=True, null=True)

    def update_data(self, data):
        for field, value in data.items():
            setattr(self, field, value)
        self.save()

    def _validate_first_collection_date(self):
        if self.recommendation:
            valid_dates = self.recommendation.sip_valid_start_dates
            if self.first_collection_date.strftime("%d-%m-%Y") not in valid_dates:
                logger.error(f"_validate_first_collection_date: {self.user_id} - Invalid SIP Start Date")
                raise ValidationError("Invalid SIP Start Date")

    def _validate_bank(self):
        try:
            UserBankDetail.objects.get(user=self.user,
                                       bank__ifsc=self.bank_ifsc,
                                       bank__bank__name=self.bank_name,
                                       account_number=self.customer_account_number,
                                       account_type=self.customer_account_type)
        except UserBankDetail.DoesNotExist:
            logger.error(f"_validate_bank: {self.user_id} - Invalid bank")
            raise ValidationError("Invalid bank")

    def _mandate_register_bsestar(self):
        if not self.mandate_id_bsestar and not self.cancelled:
            logger.info(f"_mandate_register_bsestar: {self.user_id}")
            status, message = bsestar_registration(self.user)
            if not status:
                error_text = f"BSEStar user registration - {message}"
                logger.error(f"_mandate_register_bsestar: {self.user_id} - {error_text}")
                raise ValidationError(error_text)
            else:
                status, message = mandate_register_bsestar(self.user, self)
                if not status:
                    error_text = f"BSEStar mandate registration {message}"
                    logger.error(f"_mandate_register_bsestar: {self.user_id} - {error_text}")
                    raise ValidationError(error_text)
                else:
                    self.mandate_id_bsestar = str(message)

    def get_bsestar_emandate_auth_url(self):
        logger.info(f"get_bsestar_emandate_auth_url: {self.user_id}")
        if self.web_url:
            return True, self.web_url
        status, message = emandate_auth_url(self.user, self.mandate_id_bsestar)
        if not status:
            error_text = f"BSEStar mandate registration {message}"
            logger.error(f"get_bsestar_emandate_auth_url: {self.user_id} - {error_text}")
            return False, error_text
        else:
            self.web_url = message
            self.enach_id = message.split('/')[6]
            self.save()
            return True, self.web_url

    def cancel(self, cancel_reason=None, cancel_sips=True):
        if cancel_sips:
            for sip in self.sips.all():
                sip.cancel(cancel_reason or "Manually cancelled mandate")
        self.cancelled = True
        self.cancelled_reason = cancel_reason or "Manually cancelled"
        self.cancelled_on = timezone.now()
        self.save()
        mail_admins(f"[{settings.ENV}] Mandate cancelled {self.id}",
                    f"User: {self.user.phone}\n"
                    f"Cancelled Reason: {cancel_reason}")

    def save(self, *args, **kwargs):
        if not self.pk:
            self._validate_first_collection_date()
            self._validate_bank()
        self._mandate_register_bsestar()
        super().save(*args, **kwargs)


class SIPMandateManager(models.Manager):

    @atomic
    def create_from_mandate(self, mandate):
        logger.info(f"create_from_mandate: {mandate.user_id}")
        rec = mandate.recommendation
        if not rec:
            raise ValidationError("Cannot create SIP Mandate, recommendation missing !")
        try:
            sip_mandate = self.get(mandate=mandate)
            logger.info("SIP Mandate already exist !")
        except SIPMandate.DoesNotExist:
            logger.info("Creating SIP mandate")
            sip_mandate = SIPMandate(mandate=mandate)
        sip_mandate.user = mandate.user
        sip_mandate.age = rec.age
        sip_mandate.risk_profile = rec.risk_profile
        sip_mandate.inv_amount = rec.inv_amount
        sip_mandate.inv_interval = rec.inv_interval
        sip_mandate.target_amount = rec.target_amount
        sip_mandate.expected_return = rec.expected_return
        sip_mandate.max_drawdown = rec.max_drawdown
        sip_mandate.basket = rec.basket
        sip_mandate.estimated_tenure = rec.estimated_tenure
        sip_mandate.first_collection_date = mandate.first_collection_date
        sip_mandate.recommendation_ref_id = rec.id
        sip_mandate.save()

        sip_mandate.sips.all().delete()

        order_numbers = []

        logger.info(f"create_from_mandate: {mandate.user_id} - Creating SIPs")
        for inv in rec.investments.all():
            total_installments = rec.total_installments
            total_installments = total_installments if total_installments < inv.instrument.sip_max_installment_number \
                                    else inv.instrument.sip_max_installment_number
            sip = SIP.objects.create(
                        sip_mandate=sip_mandate,
                        user = mandate.user,
                        instrument = inv.instrument,
                        first_sip_date = mandate.first_collection_date,
                        next_sip_date = mandate.first_collection_date,
                        sip_amount = inv.inv_amount,
                        inv_interval = sip_mandate.inv_interval,
                        installments_total = total_installments,
                        inv_split_percent = inv.inv_split_percent,
                        exp_return = inv.exp_return,
                    )
            sip.create_pending_order()
            order_numbers.extend(sip.get_order_numbers_bsestar())
            time.sleep(1)
        sip_mandate.order_numbers = "|".join(order_numbers)
        sip_mandate.save()

        return sip_mandate


class SIPMandate(models.Model):
    mandate = models.ForeignKey(Mandate, on_delete=models.CASCADE, related_name='sips')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="sip_mandates")
    age = models.PositiveSmallIntegerField()
    risk_profile = models.PositiveSmallIntegerField(choices=RiskProfileEnum.choices())
    inv_amount = models.FloatField()
    inv_interval = models.PositiveSmallIntegerField(choices=InvestmentIntervalEnum.choices())
    target_amount = models.FloatField()
    expected_return = models.FloatField()
    max_drawdown = models.FloatField()
    basket = models.ForeignKey(Basket, on_delete=models.PROTECT, related_name="sip_mandates")
    estimated_tenure = models.CharField(max_length=50)
    first_collection_date = models.DateField()
    last_collection_date = models.DateField(blank=True, null=True)
    recommendation_ref_id = models.PositiveBigIntegerField(default=1)

    order_numbers = models.CharField(max_length=100, blank=True, null=True)
    payment_mode = models.CharField(choices=PaymentModeEnum.choices(), blank=True, null=True)
    payment_status = models.CharField(max_length=200, blank=True, null=True)
    payment_mode = models.PositiveSmallIntegerField(choices=PaymentModeEnum.choices(),
                                                    default=PaymentModeEnum.NetBanking.value)
    bank_account_number = models.CharField(max_length=18, blank=True, null=True)
    bank_name = models.CharField(max_length=100, blank=True, null=True)
    bank_ifsc = models.CharField(max_length=15, blank=True, null=True)
    bank_code = models.CharField(max_length=4, blank=True, null=True)
    vpa_id = models.CharField(max_length=100, blank=True, null=True)
    pg_response = models.TextField(blank=True, null=True)
    # status
    # Pending -> order placed, payment initiated
    # Success -> payment is successfully completed
    # Failed -> bsestar payment status response is
    #           status code is 101 /
    #           status code 100 and response is REJECTED
    #           payment is pending for more than 5 mins (script bsestar_get_payment_status is ran every 5 mins
    #                                                    via cron to check status)
    status = models.PositiveSmallIntegerField(choices=SIPMandateStatusEnum.choices(),
                                              default=SIPMandateStatusEnum.Pending.value)
    payment_status = models.TextField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    cancelled = models.BooleanField(default=False)
    cancelled_on = models.DateTimeField(blank=True, null=True)
    cancelled_reason = models.TextField(blank=True, null=True)

    objects = SIPMandateManager()

    class Meta:
        ordering = ('-first_collection_date',)

    @property
    def summary(self):
        data = {'id': self.id, 'basket_name': self.basket.name, 'estimated_tenure': self.estimated_tenure,
                'inv_amount': self.inv_amount, 'risk_profile': self.get_risk_profile_display(), 'sip_details': []}
        for det in self.sips.all():
            sip_data = {
                'fund_name': det.instrument.name,
                'fund_logo': det.instrument.logo.url if det.instrument.logo else None,
                'order_id': f"{self.id}-{det.id}",
                'sip_date': self.first_collection_date.strftime("%d/%m/%Y"),
                'sip_amount': det.sip_amount
            }
            data['sip_details'].append(sip_data)
        return data

    @atomic
    def cancel(self, cancel_reason=None, cancel_sips=True):
        if cancel_sips:
            for sip in self.sips.all():
                sip.cancel(cancel_reason)
        self.cancelled = True
        self.cancelled_reason = cancel_reason or "Manually cancelled"
        self.cancelled_on = timezone.now()
        self.save()

    def update_payment_status(self):
        if self.status == SIPMandateStatusEnum.Pending.value:
            if self.order_numbers:
                api_resp_status = []
                for order_number in self.order_numbers.split("|"):
                    status, message = order_payment_status(self.user, order_number)
                    api_resp_status.append(f"{message}")
                self.payment_status = "|".join(api_resp_status)

                status_list = list(set(api_resp_status))
                if len(status_list) == 1:
                    if status_list[0].startswith("APPROVED"):
                        self.status = SIPMandateStatusEnum.Success.value
                    elif status_list[0].startswith("REJECTED") or status_list[0].startswith("INVALID"):
                        self.status = SIPMandateStatusEnum.Failed.value
                        self.cancel(self.payment_status)
                        mail_admins(f"[{settings.ENV}] Payment failed for mandate {self.mandate.id}",
                                    f"SIP Mandate: {self.id}\n"
                                    f"User: {self.user.phone}\n"
                                    f"Status: {self.payment_status}")
            if (timezone.now() - self.created_on).seconds > settings.BSESTAR_PAYMENT_TIMELIMIT and \
                    self.status != SIPMandateStatusEnum.Success.value:
                logger.info("SIPMandate.update_payment_status: cancelling sip as payment is not done")
                self.status = SIPMandateStatusEnum.Failed.value
                if not self.payment_status:
                    self.payment_status = f"{'|' if self.payment_status else ''}Payment timelimit expired"
                self.cancel(self.payment_status)
            self.save(update_fields=['status', 'payment_status'])
        return self.payment_status

    def _validate_first_collection_date(self):
        if self.mandate.recommendation:
            valid_dates = self.mandate.recommendation.sip_valid_start_dates
            if self.first_collection_date.strftime("%d-%m-%Y") not in valid_dates:
                raise ValidationError("Invalid SIP Start Date")

    def _validate_bank(self):
        if not self.bank_account_number or not self.bank_name or not self.bank_ifsc or not self.bank_code:
            raise ValidationError("Please enter all bank details for NetBanking payment: Bank account number, name, ifsc and code")
        logger.info("OrderLumpsum.save._validate_bank")
        try:
            UserBankDetail.objects.get(user=self.user,
                                       account_number=self.bank_account_number,
                                       bank__bank__name=self.bank_name,
                                       bank__ifsc=self.bank_ifsc,
                                       bank__bank__code_pg=self.bank_code)
        except UserBankDetail.DoesNotExist:
            raise ValidationError("Invalid bank details")

    def _validate_upi(self):
        if self.payment_mode == PaymentModeEnum.UPI.value and not self.vpa_id:
            raise ValidationError("Please enter vpa id for UPI payment")

    def get_payment_gateway_response(self):
        order_det = {
            "bank_code": self.bank_code,
            "bank_account_number": self.bank_account_number,
            "bank_ifsc": self.bank_ifsc,
            "order_numbers": self.order_numbers,
            "amount": self.inv_amount,
            "payment_mode": self.payment_mode,
            "vpa_id": self.vpa_id,
        }
        status, message = payment_gateway(self.user, order_det)
        self.pg_response = message if not status else message.replace("\r\n", "").replace("\t", "").replace('\"', "'")
        self.save()
        if not status:
            raise ValidationError(message)
        return self.pg_response

    def _validate_mandate_amount(self):
        """ Incase of multi-sips against one mandate, total SIPMandate amount should not exceed Mandate amount
        """
        # TODO: Validate amount to allow multi sip feature
        pass

    def save(self, *args, **kwargs):
        if not self.pk:
            self._validate_first_collection_date()
        return super().save(*args, **kwargs)


class SIP(models.Model):
    sip_mandate = models.ForeignKey(SIPMandate, on_delete=models.CASCADE, related_name='sips')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="sips")
    bsestar_unique_ref_no = models.CharField(max_length=19)
    bsestar_xsip_reg_id = models.CharField(max_length=10, blank=True, null=True)
    instrument = models.ForeignKey(Instrument, on_delete=models.PROTECT, related_name="sips")
    first_sip_date = models.DateField()
    next_sip_date = models.DateField(blank=True, null=True)
    sip_amount = models.FloatField()
    inv_interval = models.PositiveSmallIntegerField(choices=InvestmentIntervalEnum.choices())
    inv_split_percent = models.FloatField()
    installments_total = models.PositiveSmallIntegerField(default=12)
    installments_completed = models.PositiveSmallIntegerField(default=1)
    installments_pending = models.PositiveSmallIntegerField(default=11)
    exp_return = models.FloatField()
    created_on = models.DateTimeField(auto_now_add=True)
    bsestar_unique_ref_no_cancel = models.CharField(max_length=19, blank=True, null=True)
    cancelled = models.BooleanField(default=False)
    cancelled_on = models.DateTimeField(blank=True, null=True)
    cancelled_reason = models.TextField(blank=True, null=True)
    order_created = models.BooleanField(default=False)

    class Meta:
        ordering = ('-first_sip_date',)

    def __str__(self):
        return self.bsestar_unique_ref_no

    def _validate_first_sip_date(self):
        if not self.cancelled:
            if self.inv_interval == InvestmentIntervalEnum.Weekly.value:
                valid_dates = self.instrument.sip_weekly_valid_start_dates
            else:
                valid_dates = self.instrument.sip_monthly_valid_start_dates
            if self.first_sip_date.strftime("%d-%m-%Y") not in valid_dates:
                raise ValidationError("Invalid SIP Start Date")

    def _create_bsestar_xsip_order(self):
        logger.info('SIP.save: create_bsestar_xsip_order')
        if not self.bsestar_unique_ref_no:
            self.bsestar_unique_ref_no = generate_bsestar_unique_ref_no()
        if not self.bsestar_xsip_reg_id:
            logger.info(
                f"Creating BSEStar XSIP order for {self.user.bsestar_client_code}-{self.instrument.name}")
            status, message = xsip_order_entry(self.user, self)
            if not status:
                error_text = f"BSESTAR XSIP ORDER {message}"
                logger.error(error_text)
                self.cancelled = True
                self.cancelled_on = timezone.now()
                self.cancelled_reason = error_text
            else:
                self.bsestar_xsip_reg_id = message
                logger.info(
                    f"BSEStar XSIP order for {self.user.bsestar_client_code}-{self.instrument.name} created successfully!")

    def create_pending_order(self):
        logger.info('SIP.save: create_pending_order')
        if self.bsestar_xsip_reg_id and not self.order_created:
            for child_order in self.get_child_order_details_bsestar():
                Order.objects.create(
                    user=self.user,
                    sip=self,
                    bsestar_order_number=child_order['OrderNumber'],
                    order_date=self.first_sip_date,
                    bsestar_sip_reg_no=self.bsestar_xsip_reg_id,
                    instrument=self.instrument,
                    buy_sell=BuySellEnum.Buy.value,
                    amount=self.sip_amount
                )
                self.order_created = True
            if self.order_created:
                self.save()
            else:  # if first order is not created, cancel sip
                self.cancel(cancel_reason="First order not created")

    def save(self, *args, **kwargs):
        if not self.pk:
            self._validate_first_sip_date()
            self._create_bsestar_xsip_order()
        super().save(*args, **kwargs)

    @property
    def summary(self):
        order_det = self.orders.filter(status=OrderStatusEnum.Completed.value)\
                               .values('buy_sell', 'amount', 'units', 'sip_id',
                                       current_nav=F('instrument__current_nav_value'))
        if not order_det:
            data = {
                'investment_total': 0.0,
                'units_total': 0.0,
                'current_value_total': 0.0,
                'gain_total': 0.0,
                'return_total': 0.0
            }
            return data

        df = pd.DataFrame(list(order_det))
        df = df.groupby(['sip_id', 'buy_sell']) \
            .agg({'amount': np.sum, 'units': np.sum, 'current_nav': np.max}) \
            .reset_index()
        df['nav'] = round(df['amount'] / df['units'], 4)

        df_sell = df[df['buy_sell'] == 2]
        if not df_sell.empty:
            df_sell = df_sell.rename(columns={'amount': 'amount_sell', 'units': 'units_sell', 'nav': 'nav_sell'})
            df_sell = df_sell.drop(['buy_sell', 'current_nav'], axis=1)
            df = pd.merge(df[df['buy_sell'] == 1], df_sell, on=['sip_id'],
                          how='left')
            df = df.fillna({
                'amount_sell': 0.0,
                'units_sell': 0.0,
                'nav_sell': 0.0})
        else:
            df = df.assign(amount_sell=0.0,
                           units_sell=0.0,
                           nav_sell=0.0)
        df['investment'] = round(df['amount'] - df['amount_sell'], 0)
        df['total_units'] = round(df['units'] - df['units_sell'], 4)
        df['current_value'] = round(df['total_units'] * df['current_nav'], 0)
        investment_total = int(df['investment'].sum())
        current_value_total = int(df['current_value'].sum())
        units_total = round(df['total_units'].sum(), 4)
        gain_total = current_value_total - investment_total
        return_total = round(gain_total * 100 / investment_total, 2)
        return {
            'investment_total': investment_total,
            'current_value_total': current_value_total,
            'gain_total': gain_total,
            'units_total': units_total,
            'return_total': return_total,
            'current_nav': self.instrument.current_nav_value
        }

    @property
    def inv_interval_display_bsestar(self):
        disp = self.get_inv_interval_display()
        return disp.upper()

    def update_next_sip_date(self):
        today = timezone.localdate()
        if self.first_sip_date.day < today.day:
            next_month = timezone.localdate() + relativedelta.relativedelta(months=1)
            next_date = datetime.date(next_month.year, next_month.month, self.first_sip_date.day)
        else:
            next_date = datetime.date(today.year, today.month, self.first_sip_date.day)
        self.next_sip_date = next_date

    def update_installment_details(self):
        completed = self.orders.filter(status=OrderStatusEnum.Completed.value).count()
        pending = self.installments_total - completed
        self.installments_completed = completed
        self.installments_pending = pending
        self.update_next_sip_date()
        self.save()


    def get_child_order_details_bsestar(self):
        """
        [
            {
                "Amount": "1000.0000",
                "BSESchemeCode": "ICICI1477-GR",
                "BuySell": "P",
                "BuySellType": "FRESH",
                "ClientCode": "7021838972",
                "ClientName": "SAUMIL HEMANT DALAL",
                "DPTxnType": "P",
                "EUINFlag": "N",
                "EUINNumber": "",
                "FirstOrderTodayFlag": "Y",
                "FolioNo": "",
                "IntRefNo": "",
                "KYCFlag": "Y",
                "MemberCode": "29132",
                "OrderNumber": "8252683",
                "OrderType": "XSP",
                "Quantity": "0.0000",
                "RTASchemeCode": "1477",
                "SchemeName": "ICICI PRUDENTIAL CORPORATE BOND FUND - GROWTH",
                "SubBrokerARNCode": "",
                "SubBrokerCode": "",
                "SubOrderType": "NRM"
            }
        ]
        """
        if self.bsestar_xsip_reg_id:
            status, det = get_child_order_details(self.user, {"date": timezone.localdate().strftime("%d/%m/%Y"),
                                                              "xsip_regno": self.bsestar_xsip_reg_id})
            if not status:
                raise ValidationError(status)
            return det
        return []

    def get_order_numbers_bsestar(self):
        det = self.get_child_order_details_bsestar()
        if det:
            return [d["OrderNumber"] for d in det]
        return []

    @atomic
    def cancel(self, cancel_reason=None, cancel_orders=True):
        if self.cancelled:
            return False, f"SIP already stopped. Reason: {self.cancelled_reason}"
        self.bsestar_unique_ref_no_cancel = generate_bsestar_unique_ref_no()
        status, message = xsip_order_entry(self.user, self, cancel=True)
        if not status:
            logger.error(f"Error in cancelling sip {message}")
            return False, message
        self.cancelled = True
        self.cancelled_on = timezone.now()
        self.cancelled_reason = cancel_reason or "Stopped by user"
        self.save()
        if cancel_orders:
            Order.objects.filter(sip=self, status__in=[OrderStatusEnum.Pending.value, OrderStatusEnum.Upcoming.value])\
                         .update(status=OrderStatusEnum.Cancelled.value, cancelled_on=self.cancelled_on,
                                 cancelled_reason=self.cancelled_reason)
        return True, f"SIP {'cancelled' if cancel_reason else 'stopped'} successfully !"


class OrderLumpsum(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='lumpsum_orders')
    order_numbers = models.CharField(max_length=100, blank=True, null=True)
    payment_mode = models.PositiveSmallIntegerField(choices=PaymentModeEnum.choices(),
                                                    default=PaymentModeEnum.NetBanking.value)
    bank_account_number = models.CharField(max_length=18, blank=True, null=True)
    bank_name = models.CharField(max_length=100, blank=True, null=True)
    bank_ifsc = models.CharField(max_length=15, blank=True, null=True)
    bank_code = models.CharField(max_length=4, blank=True, null=True)
    vpa_id = models.CharField(max_length=100, blank=True, null=True)
    amount = models.FloatField(default=1000.0, validators=[MinValueValidator(1000.0)])
    recommendation = models.ForeignKey(UserRecommendation, on_delete=models.PROTECT, related_name='lumpsum_orders')
    # status
    # Pending -> order placed, payment initiated
    # Success -> payment is successfully completed
    # Failed -> bsestar payment status response is
    #           status code is 101 /
    #           status code 100 and response is REJECTED
    #           payment is pending for more than 5 mins (script bsestar_get_payment_status is ran every 5 mins
    #                                                    via cron to check status)
    status = models.PositiveSmallIntegerField(choices=OrderLumpsumStatusEnum.choices(),
                                              default=OrderLumpsumStatusEnum.Pending.value)
    payment_status = models.TextField(blank=True, null=True)
    pg_response = models.TextField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    order_created = models.BooleanField(default=False)

    def update_payment_status(self):
        if self.status == OrderLumpsumStatusEnum.Pending.value:
            if self.order_numbers:
                api_resp_status = []
                for order_number in self.order_numbers.split("|"):
                    status, message = order_payment_status(self.user, order_number)
                    api_resp_status.append(message)
                self.payment_status = "|".join(api_resp_status)

                status_list = list(set(api_resp_status))
                if len(status_list) == 1:
                    self.payment_status = status_list[0]
                    if status_list[0].startswith("APPROVED"):
                        self.status = OrderLumpsumStatusEnum.Success.value
                    elif status_list[0].startswith("REJECTED") or status_list[0].startswith("INVALID"):
                        self.status = OrderLumpsumStatusEnum.Failed.value
                        self._cancel_orders("Time limit expired")
            if self.created_on and (timezone.now() - self.created_on).seconds > settings.BSESTAR_PAYMENT_TIMELIMIT and \
                    self.status != OrderLumpsumStatusEnum.Success.value:
                self.status = OrderLumpsumStatusEnum.Failed.value
                if not self.payment_status:
                    self.payment_status = f"{'|' if self.payment_status else ''}Payment timelimit expired"
                self._cancel_orders("Time limit expired")
            self.save(update_fields=['status', 'payment_status'])
        return self.payment_status

    def _validate_bank(self):
        if not self.bank_account_number or not self.bank_name or not self.bank_ifsc or not self.bank_code:
            raise ValidationError("Please enter all bank details for NetBanking payment: Bank account number, name, ifsc and code")
        logger.info("OrderLumpsum.save._validate_bank")
        try:
            UserBankDetail.objects.get(user=self.user,
                                       account_number=self.bank_account_number,
                                       bank__ifsc=self.bank_ifsc,
                                       bank__bank__code_pg=self.bank_code)
        except UserBankDetail.DoesNotExist:
            raise ValidationError("Invalid bank details")

    def _validate_upi(self):
        if self.payment_mode == PaymentModeEnum.UPI.value and not self.vpa_id:
            raise ValidationError("Please enter vpa id for UPI payment")

    def _cancel_orders(self, cancel_reason):
        """
        Cancel Order if status is Failed
        """
        if self.status == OrderLumpsumStatusEnum.Pending.value:
            self.orders.all().update(status=OrderStatusEnum.Cancelled.value,
                                     cancelled_on=timezone.now(),
                                     cancelled_reason=cancel_reason)

    def _create_orders_and_fetch_pg_redirect_html(self):
        if not self.order_created and self.status != OrderStatusEnum.Cancelled.value:
            if self.created_on and (timezone.now() - self.created_on).seconds > settings.BSESTAR_PAYMENT_TIMELIMIT:
                self._cancel_orders("Time limit expired")
            else:
                logger.info("OrderLumpsum.save._create_orders")
                status, message = bsestar_registration(self.user)
                if not status:
                    error_text = f"BSEStar user registration - {message}"
                    logger.error(error_text)
                    raise ValidationError(error_text)
                order_numbers = []
                for inv in self.recommendation.investments.all():
                    o = Order.objects.create(
                        order_lumpsum=self,
                        user=self.user,
                        instrument=inv.instrument,
                        order_date=timezone.localdate(),
                        amount=inv.inv_amount,
                        buy_sell=BuySellEnum.Buy.value,
                        user_bank=UserBankDetail.objects.get(account_number=self.bank_account_number,
                                                             user=self.user) \
                                  if self.payment_mode == PaymentModeEnum.NetBanking.value else None
                    )
                    order_numbers.append(o.bsestar_order_number)
                self.order_numbers = "|".join(order_numbers)
                self.order_created = True
        if self.order_created and not self.pg_response:
            order_det = {
                "bank_code": self.bank_code,
                "bank_account_number": self.bank_account_number,
                "bank_ifsc": self.bank_ifsc,
                "order_numbers": self.order_numbers,
                "amount": self.amount,
                "payment_mode": self.payment_mode,
                "vpa_id": self.vpa_id,
            }
            status, message = payment_gateway(self.user, order_det)
            if not status:
                self.status = OrderLumpsumStatusEnum.Failed.value
                self._cancel_orders(message)
                self.pg_response = message
                self.save()
                raise ValidationError(message)

            self.pg_response = message.replace("\r\n", "").replace("\t", "").replace('\"', "'")
            self.save()

    def save(self, *args, **kwargs):
        if not self.pk:
            self._validate_bank()
            self._validate_upi()
        super().save(*args, **kwargs)
        self._create_orders_and_fetch_pg_redirect_html()


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="orders")
    sip = models.ForeignKey(SIP, on_delete=models.CASCADE, related_name='orders', blank=True, null=True)
    order_lumpsum = models.ForeignKey(OrderLumpsum, on_delete=models.CASCADE, related_name='orders',
                                      blank=True, null=True)
    order_date = models.DateField()
    bsestar_sip_reg_no = models.CharField(max_length=10, blank=True, null=True)
    bsestar_unique_ref_no = models.CharField(max_length=19, blank=True, null=True)
    bsestar_order_number = models.CharField(max_length=16, blank=True, null=True)
    bsestar_order_datetime = models.DateTimeField(blank=True, null=True)
    bsestar_settlement_number = models.CharField(max_length=7, blank=True, null=True)
    bsestar_sip_installment_number = models.PositiveSmallIntegerField(default=1)
    instrument = models.ForeignKey(Instrument, on_delete=models.CASCADE)
    buy_sell = models.PositiveSmallIntegerField(choices=BuySellEnum.choices())
    folio_number = models.CharField(max_length=15, blank=True, null=True)
    amount = models.FloatField(default=0.0, validators=[MinValueValidator(0.0)])
    units = models.FloatField(default=0.0, validators=[MinValueValidator(0.0)])
    allotted_amount = models.FloatField(default=0.0, validators=[MinValueValidator(0.0)])
    nav = models.FloatField(default=0.0, validators=[MinValueValidator(0.0)])
    redeem_all = models.BooleanField(default=False)
    user_bank = models.ForeignKey(UserBankDetail, on_delete=models.PROTECT, related_name='orders',
                                  blank=True, null=True) # sell lumpsum order
    status = models.PositiveSmallIntegerField(choices=OrderStatusEnum.choices(),
                                              default=OrderStatusEnum.Pending.value,
                                              db_index=True)
    bsestar_status = models.CharField(max_length=10, choices=BSEStarOrderStatusEnum.choices(), blank=True, null=True)
    bsestar_settlement_type = models.CharField(max_length=2, blank=True, null=True)
    bsestar_remarks = models.CharField(max_length=200, blank=True, null=True)
    order_remarks = models.CharField(max_length=200, blank=True, null=True)
    redeem_2fa_url = models.CharField(max_length=200, blank=True, null=True)
    cancelled_on = models.DateTimeField(blank=True, null=True)
    cancelled_reason = models.TextField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-modified_on',)

    def __str__(self):
        return f"{self.user.bsestar_client_code} - {self.instrument.name}"

    @property
    def order_type(self):
        return 'lumpsum' if self.order_lumpsum is not None else 'sip'

    @property
    def order_created_date(self):
        return self.created_on.strftime("%Y-%m-%d")

    @property
    def risk_profile_display(self):
        return self.sip.sip_mandate.get_risk_profile_display() if self.sip \
            else self.order_lumpsum.recommendation.get_risk_profile_display()

    @property
    def first_sip_date(self):
        return self.sip.first_sip_date if self.sip else None

    @property
    def bank_account(self):
        if self.sip:
            return self.sip.sip_mandate.mandate.bank_name
        elif self.order_lumpsum and self.user_bank:
            return self.user_bank.bank.bank.name

    @property
    def bank_account_number(self):
        if self.sip:
            return self.sip.sip_mandate.mandate.customer_account_number
        elif self.order_lumpsum and self.buy_sell == BuySellEnum.Sell.value:
            return self.user_bank.account_number

    @property
    def sip_installment_number(self):
        return f"{self.bsestar_sip_installment_number}/{self.sip.installments_total}" if self.sip and self.buy_sell == BuySellEnum.Buy.value else 0

    @property
    def sip_installments_completed(self):
        return Order.objects.filter(sip=self.sip).exclude(id=self.id).count()

    def _validate_values(self):
        logger.info("Order.save._validate_values")
        # amount / units should be present for buy / sell order
        if not self.redeem_all and self.amount == 0.0 and self.units == 0.0:
            raise ValidationError("Either amount or units should be greater than 0")

    def _populate_values(self):
        logger.info("Order.save._populate_values")
        # populate bsestar_sip_reg_no incase not entered for SIP order
        if self.sip and (not self.bsestar_sip_reg_no or self.bsestar_sip_reg_no != self.sip.bsestar_xsip_reg_id):
            self.bsestar_sip_reg_no = self.sip.bsestar_xsip_reg_id

        # populate bsestar_unique_ref_no for lumpsum buy/sell order
        if not self.bsestar_unique_ref_no:
            self.bsestar_unique_ref_no = generate_bsestar_unique_ref_no()

        if self.amount > 0.0 and self.units > 0.0 and not self.nav:
            self.nav = round(self.amount / self.units, 4)

        if self.sip and not self.bsestar_sip_installment_number:
            self.bsestar_sip_installment_number = self.sip_installments_completed + 1

    def _create_bsestar_lumpsum_buy_order(self):
        if self.order_lumpsum and self.buy_sell == BuySellEnum.Buy.value and not self.bsestar_order_number:
            logger.info("Order.save._create_bsestar_lumpsum_buy_order")
            status, message = bsestar_registration(self.user)
            if not status:
                error_text = f"BSESTAR ORDER {message}"
                logger.error(error_text)
                self.status = OrderStatusEnum.Cancelled.value
                self.cancelled_on = timezone.now()
                self.cancelled_reason = error_text
                raise ValidationError(error_text)
            else:
                status, message = order_buy_sell(self.user, self)
                if not status:
                    error_text = f"BSESTAR ORDER {message}"
                    logger.error(error_text)
                    self.status = OrderStatusEnum.Cancelled.value
                    self.cancelled_on = timezone.now()
                    self.cancelled_reason = error_text
                    raise ValidationError(error_text)
                else:
                    self.bsestar_order_number = message
                    logger.info(
                        f"BSEStar order for {self.user.bsestar_client_code}-{self.instrument.name} created successfully!")

    def _create_bsestar_sell_order(self):
        if self.buy_sell == BuySellEnum.Sell.value and not self.bsestar_order_number:
            logger.info("Order.save._create_bsestar_sell_order")
            # redeem amount cannot be greater than current_value
            if self.amount > self.redeem_summary['current_value_total']:
                raise ValidationError("Redeem amount cannot be greater than current value")

            status, message = order_buy_sell(self.user, self)
            if not status:
                error_text = f"BSESTAR ORDER {message}"
                logger.error(error_text)
                self.status = OrderStatusEnum.Cancelled.value
                self.cancelled_on = timezone.now()
                self.cancelled_reason = error_text
                raise ValidationError(error_text)
            else:
                self.bsestar_order_number = message
                logger.info("Generating 2FA url")
                status, message = redeem_2fa(self.user)
                logger.info(f"{status}-{message}")
                if status and message['StatusCode'] == '100':
                    self.redeem_2fa_url = message['ReturnUrl']
                logger.info(
                    f"BSEStar order for {self.user.bsestar_client_code}-{self.instrument.name} created successfully!")

    def save(self, *args, **kwargs):
        self._validate_values()
        self._populate_values()
        self._create_bsestar_lumpsum_buy_order()
        self._create_bsestar_sell_order()
        super().save(*args, **kwargs)
        if self.sip:
            if self.status == OrderStatusEnum.Cancelled.value and self.bsestar_sip_installment_number == 1:
                # cancel sip if first order is cancelled
                cancel_reason = f"First Order Cancelled: {self.cancelled_reason}"
                self.sip.cancel(cancel_reason=cancel_reason, cancel_orders=False)

                # cancel sip mandate if all first orders are cancelled
                if not SIP.objects.filter(cancelled=False, sip_mandate=self.sip.sip_mandate).exists():
                    self.sip.sip_mandate.cancel(cancel_reason=cancel_reason, cancel_sips=False)

                # cancel mandate if all sips are cancelled
                if not SIPMandate.objects.filter(cancelled=False, mandate=self.sip.sip_mandate.mandate).exists():
                    self.sip.sip_mandate.mandate.cancel(cancel_reason=cancel_reason, cancel_sips=False)

            self.sip.update_installment_details()


    @property
    def redeem_summary(self):
        if self.sip:
            return self.sip.summary

        order_buy = Order.objects.get(buy_sell=BuySellEnum.Buy.value,
                                      order_lumpsum_id=self.order_lumpsum_id,
                                      instrument=self.instrument,
                                      status=OrderStatusEnum.Completed.value)

        order_sell_det = Order.objects.filter(buy_sell=BuySellEnum.Sell.value,
                                              order_lumpsum_id=self.order_lumpsum_id,
                                              instrument=self.instrument,
                                              status__in=[OrderStatusEnum.Completed.value, OrderStatusEnum.Pending.value]) \
                                      .aggregate(amount=Sum('amount'), units=Sum('units'))
        amount_sell = order_sell_det['amount'] or 0.0
        units_sell = order_sell_det['units'] or 0.0
        if amount_sell and units_sell:
            nav_sell = round(amount_sell / units_sell, 4)
        else:
            nav_sell = 0.0

        investment_total = int(order_buy.amount - amount_sell)
        units_total = round(order_buy.units - units_sell, 4)
        current_value_total = int(units_total * self.instrument.current_nav_value)
        gain_total = current_value_total - investment_total
        return_total = round(gain_total * 100 / investment_total, 2)
        data = {
            'investment_total': investment_total,
            'current_value_total': current_value_total,
            'gain_total': gain_total,
            'units_total': units_total,
            'return_total': return_total,
            'current_nav': self.instrument.current_nav_value
        }
        return data

    def _update_allotment_buy(self):
        status, message = allotment_statement(from_date=self.bsestar_order_datetime,
                                              to_date=self.bsestar_order_datetime,
                                              order_id=self.bsestar_order_number,
                                              client_code=self.user.bsestar_client_code)
        if not status:
            logger.error(message)
            return False
        else:
            if isinstance(message, list):
                message = message[0]
            self.folio_number = message['b:FolioNo']
            if not self.user.bsestar_folio_no:
                self.user.bsestar_folio_no = self.folio_number
                self.user.save()
            self.allotted_amount = float(message['b:AllottedAmt'])
            self.units = float(message['b:AllottedUnit'])
            self.nav = float(message['b:AllottedNav'])
            return True

    def _update_allotment_sell(self):
        status, message = redemption_statement(from_date=self.bsestar_order_datetime,
                                               to_date=self.bsestar_order_datetime,
                                               order_id=self.bsestar_order_number,
                                               client_code=self.user.bsestar_client_code)
        if not status:
            logger.error(message)
            return False
        else:
            if isinstance(message, list):
                message = message[0]
            self.allotted_amount = float(message['b:Amt'])
            self.units = float(message['b:Unit'])
            self.nav = float(message['b:Nav'])
            return True

    def update_units_allotment(self, save=True):
        if self.status != OrderStatusEnum.Completed.value:
            logger.error(f"order.update_units_allotment: Order Number: {self.id} status: {self.status}")
            return self
        if self.buy_sell == BuySellEnum.Buy.value:
            status = self._update_allotment_buy()
        else:
            status = self._update_allotment_sell()
        if status and save:
            self.save()
        return self


    def update_bsestar_pending_order_status(self):
        if self.status != OrderStatusEnum.Pending.value:
            return
        if not self.bsestar_order_number:
            self.status = OrderStatusEnum.Cancelled.value
            self.cancelled_on = timezone.now()
            self.cancelled_reason = "BSEStar Order Number missing"
            self.save()
            return

        status, message = order_log(user=self.user, from_date=self.order_date, to_date=self.order_date,
                                    order_no=self.bsestar_order_number)
        if not status:
            logger.error(message)
            return
        if isinstance(message, dict):
            message = [message]
        for idx, det in enumerate(message):
            logger.info(f"Order.update_bsestar_order_status: {det}")
            self.bsestar_order_datetime = timezone.make_aware(
                datetime.datetime.strptime(f"{det['b:Date']} {det['b:Time']}",
                                           "%d/%m/%Y %H:%M:%S"))
            self.bsestar_settlement_number = det['b:SettNo']
            self.units = float(det['b:Units'])
            self.bsestar_settlement_type = det['b:SettType']
            self.bsestar_status = det['b:OrderStatus']
            self.order_remarks = det['b:OrderRemarks']
            if self.bsestar_status == BSEStarOrderStatusEnum.Valid.value:
                if self.order_remarks == "ALLOTMENT DONE":
                    self.status = OrderStatusEnum.Completed.value
            else:
                self.status = OrderStatusEnum.Cancelled.value
            if self.status == OrderStatusEnum.Cancelled.value:
                self.cancelled_on = self.bsestar_order_datetime
                self.cancelled_reason = self.order_remarks
            elif self.status == OrderStatusEnum.Completed.value:
                self.update_units_allotment(save=False)
            self.save()


@receiver(post_save, sender=Order)
def create_upcoming_order(sender, instance, created, **kwargs):
    """ Create upcoming order if
        1. SIP order
        2. Status is completed
            OR
            Status is cancelled due to Invalid Nomination (In this case bsestar doesn't cancel the sip,
                                                           user can update nomination and next sip would be successful)
            OR
            Status is cancelled due to payment not initiated from client end
        3. No upcoming order is created
    """
    logger.info("Order.post_save: create_upcoming_order")
    if instance.sip and instance.sip.installments_pending > 0 and not instance.sip.cancelled \
            and (instance.status == OrderStatusEnum.Completed.value or \
                 (instance.status == OrderStatusEnum.Cancelled.value and \
                  instance.bsestar_status == BSEStarOrderStatusEnum.Invalid.value and \
                  instance.order_remarks in ['NOMINATION INVALID', 'PAYMENT NOT INITIATED FROM CLIENT END',
                                             'PAYMENT NOT RECEIVED TILL DATE', 'Balance Insufficient']))\
            and not Order.objects.filter(user=instance.user, sip=instance.sip, instrument=instance.instrument,
                                         status=OrderStatusEnum.Upcoming.value).exists():
        next_month = instance.order_date + relativedelta.relativedelta(months=1)
        logger.info(f"Creating upcoming order for {instance.instrument}")
        Order.objects.create(
            user=instance.user,
            sip=instance.sip,
            order_date=datetime.date(next_month.year, next_month.month, instance.sip.first_sip_date.day),
            bsestar_sip_reg_no=instance.sip.bsestar_xsip_reg_id,
            instrument=instance.instrument,
            buy_sell=BuySellEnum.Buy.value,
            amount=instance.sip.sip_amount,
            status=OrderStatusEnum.Upcoming.value
        )

