from celery import shared_task
from django.utils import timezone

from api.rest import (
    bsestar_registration,
    mandate_register_bsestar,
)
#from celery.utils.log import get_task_logger
#logger = get_task_logger(__name__)

import logging
logger = logging.getLogger(__name__)


@shared_task
def bsestar_mandate_registration(mandate_id):
    try:
        from .models import Mandate
        mandate_instance = Mandate.objects.get(id=mandate_id)
    except Mandate.DoesNotExist:
        error_text = f"Invalid mandate id: {mandate_id}"
        logger.error(error_text)
        return error_text
    status, message = bsestar_registration(mandate_instance.user)
    if not status:
        error_text = f"BSEStar user registration {message}"
        logger.error(error_text)
        mandate_instance.cancelled = True
        mandate_instance.cancelled_on = timezone.now()
        mandate_instance.cancelled_reason = error_text
    status, message = mandate_register_bsestar(mandate_instance.user, mandate_instance)
    if not status:
        error_text = f"BSEStar mandate registration {message}"
        logger.error(error_text)
        mandate_instance.cancelled = True
        mandate_instance.cancelled_on = timezone.now()
        mandate_instance.cancelled_reason = error_text
    else:
        logger.info("BSEStar mandate registered successfully !")
        mandate_instance.mandate_id_bsestar = str(message)
    mandate_instance.save()
    return message


@shared_task
def create_mandate_sip(mandate_id):
    from .models import Mandate, SIPMandate
    try:
        mandate_instance = Mandate.objects.get(id=mandate_id)
    except Mandate.DoesNotExist:
        return f"Invalid mandate ID: {mandate_id}"
    SIPMandate.objects.create_from_mandate(mandate_instance)
    mandate_instance.sip_created = True
    mandate_instance.save()
    return "SIP Mandate created successfully"

