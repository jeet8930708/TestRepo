from django.urls import path

from .views import (
    MandateCreateView,
    MandateStatusView,
    OrderListView,
    OrderDetailView,
    SIPStopView,
    OrderSellSIPCreateView,
    SIPRedeemDetailView,
    LumpsumRedeemDetailView,
    SIPStartDatesView,
    OrderBuyLumpsum,
    OrderBuyPGResponse,
    OrderSellLumpsumCreateView,
    MandateBSEStarURLView,
    OrderLumpsumPaymentStatusView,
    OrderSIPPaymentGatewayView,
    OrderSIPPGResponse,
    OrderSIPPaymentStatusView,
)

from .enums import OrderStatusEnum


urlpatterns = [
    path('mandate/get-sip-valid-start-dates/<int:recid>/', SIPStartDatesView.as_view(), name='sip-start-dates'),
    path('mandate/create/', MandateCreateView.as_view(), name='mandate-create'),
    path('mandate/get-bsestar-emandate-url/<int:pk>/', MandateBSEStarURLView.as_view(), name='mandate-generate-link'),
    path('mandate/auto-pay-setup-status/', MandateStatusView.as_view(), name='mandate-status'),
    path('transaction/pending/', OrderListView.as_view(), {"status": OrderStatusEnum.Pending.value},
         name='transaction-pending'),
    path('transaction/upcoming/', OrderListView.as_view(), {"status": OrderStatusEnum.Upcoming.value},
         name='transaction-upcoming'),
    path('transaction/completed/', OrderListView.as_view(), {"status": OrderStatusEnum.Completed.value},
         name='transaction-completed'),
    path('transaction/cancelled/', OrderListView.as_view(), {"status": OrderStatusEnum.Cancelled.value},
         name='transaction-cancelled'),
    path('detail/<int:pk>/', OrderDetailView.as_view(), name='order-detail'),
    path('sip/stop/', SIPStopView.as_view(), name='sip-stop'),
    path('redeem/detail/<int:pk>/', SIPRedeemDetailView.as_view(), name='sip-redeem-detail'),  # backward compatibility
    path('redeem/detail/sip/<int:pk>/', SIPRedeemDetailView.as_view(), name='sip-redeem-detail'),
    path('redeem/detail/lumpsum/<int:pk>/', LumpsumRedeemDetailView.as_view(), name='lumpsum-redeem-detail'),

    path('redeem-fund/', OrderSellSIPCreateView.as_view(), name='order-sell'),
    path('redeem-fund/sip/', OrderSellSIPCreateView.as_view(), name='order-sell-sip'),
    path('redeem-fund/lumpsum/', OrderSellLumpsumCreateView.as_view(), name='order-sell-lumpsum'),
    path('buy/sip/payment-gateway-request/', OrderSIPPaymentGatewayView.as_view(), name='order-sip-payment-gateway'),
    path('buy/sip/payment-gateway-response/', OrderSIPPGResponse.as_view(), name='sip-buy-pg-response'),
    path('buy/sip/payment-status/', OrderSIPPaymentStatusView.as_view(), name='sip-order-payment-status'),

    path('buy/lumpsum/payment-gateway-request/', OrderBuyLumpsum.as_view(), name='order-buy-lumpsum-pg-request'),
    path('buy/lumpsum/payment-gateway-response/', OrderBuyPGResponse.as_view(), name='lumpsum-buy-pg-response'),
    path('buy/lumpsum/payment-status/', OrderLumpsumPaymentStatusView.as_view(), name='lumpsum-order-payment-status'),
]
