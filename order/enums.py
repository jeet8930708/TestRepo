from core.enums import ChoiceEnum


class MandateFrequencyEnum(ChoiceEnum):
    Adhoc = "Adhoc"
    Monthly = "Monthly"
    Weekly = "Weekly"


class MandateCategoryEnum(ChoiceEnum):
    Mutual_Fund_Payment = "M001"
    Destination_Bank_Mandate = "D001"
    Others = "U099"


class MandateStatusEnum(ChoiceEnum):
    Partial = "partial"
    Success = "auth_success"
    Fail = "auth_fail"


class MandateInstrumentTypeEnum(ChoiceEnum):
    Debit = "debit"
    Credit = "credit"


class BuySellEnum(ChoiceEnum):
    Buy = 1
    Sell = 2


class BSEStarOrderStatusEnum(ChoiceEnum):
    Valid = "VALID"
    Invalid = "INVALID"


class OrderStatusEnum(ChoiceEnum):
    Pending = 1
    Upcoming = 2
    Completed = 3
    Cancelled = 4


class OrderLumpsumStatusEnum(ChoiceEnum):
    Pending = 1
    Success = 2
    Failed = 3


class PaymentModeEnum(ChoiceEnum):
    NetBanking = 1
    UPI = 2


class SIPMandateStatusEnum(ChoiceEnum):
    Pending = 1
    Success = 2
    Failed = 3
