# Generated by Django 4.0.2 on 2022-08-17 05:43

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0036_remove_user_bsestar_mandate_registered_and_more'),
        ('order', '0026_sip_bsestar_unique_ref_no_cancel'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='bsestar_unique_ref_no',
            field=models.CharField(blank=True, max_length=19, null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='cancelled',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='order',
            name='cancelled_on',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='cancelled_reason',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AddField(
            model_name='order',
            name='redeem_all',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='order',
            name='user_bank',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='sell_orders', to='core.userbankdetail'),
        ),
        migrations.AlterField(
            model_name='order',
            name='amount',
            field=models.FloatField(default=0.0, validators=[django.core.validators.MinValueValidator(0.0)]),
        ),
        migrations.AlterField(
            model_name='order',
            name='bsestar_sip_reg_no',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='order',
            name='nav',
            field=models.FloatField(default=0.0, validators=[django.core.validators.MinValueValidator(0.0)]),
        ),
        migrations.AlterField(
            model_name='order',
            name='units',
            field=models.FloatField(default=0.0, validators=[django.core.validators.MinValueValidator(0.0)]),
        ),
    ]
