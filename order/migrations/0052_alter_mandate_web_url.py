# Generated by Django 4.0.2 on 2022-10-16 12:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0051_remove_mandate_corporate_config_id_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mandate',
            name='web_url',
            field=models.CharField(max_length=300),
        ),
    ]
