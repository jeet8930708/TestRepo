# Generated by Django 4.0.2 on 2022-08-09 11:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0013_alter_sip_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='sip',
            name='bsestar_unique_ref_no',
            field=models.CharField(blank=True, max_length=19, null=True),
        ),
    ]
