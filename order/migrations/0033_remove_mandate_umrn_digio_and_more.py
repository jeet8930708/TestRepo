# Generated by Django 4.0.2 on 2022-08-25 12:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0032_mandate_umrn_digio'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mandate',
            name='umrn_digio',
        ),
        migrations.AddField(
            model_name='mandate',
            name='mandate_detail_digio',
            field=models.TextField(blank=True, null=True),
        ),
    ]
