from django.contrib import admin

# Register your models here.
from .models import Mandate, SIPMandate, SIP, Order, OrderLumpsum


class MandateAdmin(admin.ModelAdmin):
    list_display = ('user', 'bank_name', 'maximum_amount', 'frequency', 'is_recurring',
                    'first_collection_date', 'status', 'created_on', 'cancelled')
    list_filter = ('status', 'maximum_amount', 'cancelled')
    search_fields = ('user__first_name', 'user__phone', 'mandate_id_bsestar', 'bank_name',
                     'enach_id')
admin.site.register(Mandate, MandateAdmin)


class SIPMandateAdmin(admin.ModelAdmin):
    list_display = ('mandate', 'user', 'basket', 'risk_profile', 'inv_amount', 'target_amount', 'inv_interval',
                    'estimated_tenure', 'first_collection_date', 'payment_mode', 'created_on', 'status', 'cancelled')
    list_filter = ('inv_interval', 'risk_profile', 'payment_mode', 'status', 'basket', 'cancelled')
    search_fields = ('user__first_name', 'user__phone', 'mandate__mandate_id_bsestar')
admin.site.register(SIPMandate, SIPMandateAdmin)


class SIPAdmin(admin.ModelAdmin):
    list_display = ('bsestar_unique_ref_no', 'bsestar_xsip_reg_id', 'user', 'instrument', 'first_sip_date',
                    'sip_amount', 'inv_interval', 'created_on', 'order_created', 'cancelled')
    list_filter = ('inv_interval', 'order_created', 'cancelled')
    search_fields = ('user__first_name', 'user__phone', 'instrument__name', 'instrument__isin',
                     'bsestar_unique_ref_no', 'bsestar_xsip_reg_id')
admin.site.register(SIP, SIPAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ('order_date', 'user', 'bsestar_order_number', 'bsestar_order_datetime', 'instrument', 'buy_sell',
                    'amount', 'units', 'status', 'created_on')
    list_filter = ('buy_sell', 'status', 'bsestar_order_datetime', 'instrument')
    search_fields = ('bsestar_order_number', 'sip__instrument__isin', 'sip__instrument__name',
                    'user__phone', 'bsestar_sip_reg_no')
admin.site.register(Order, OrderAdmin)


class OrderLumpsumAdmin(admin.ModelAdmin):
    list_display = ('order_numbers', 'user', 'amount', 'payment_mode', 'status', 'created_on', 'updated_on', 'order_created')
    list_filter = ('status', 'payment_mode', 'order_created', 'created_on', 'updated_on')
    search_fields = ('order_numbers', 'user__phone', 'user__first_name', 'user__email')
admin.site.register(OrderLumpsum, OrderLumpsumAdmin)
