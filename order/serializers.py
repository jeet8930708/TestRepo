from django.utils import timezone

from rest_framework.serializers import (
    ModelSerializer,
    IntegerField,
    FloatField,
    ValidationError,
    CharField,
    DictField,
    ImageField,
    SerializerMethodField,
)

from recommendation.enums import InvestmentIntervalEnum
from core.models import UserBankDetail
from .models import Mandate, MandateFrequencyEnum, Order, BuySellEnum, SIP, OrderLumpsum, PaymentModeEnum


class MandateSerializer(ModelSerializer):

    bank = IntegerField(write_only=True)
    sip_amount = FloatField(write_only=True)

    class Meta:
        model = Mandate
        fields = ('id', 'user', 'recommendation', 'first_collection_date', 'bank', 'maximum_amount', 'sip_amount',
                  'frequency', 'is_recurring', 'bank_name', 'customer_name',
                  'web_url', 'status')
        read_only_fields = ('id', 'user', 'frequency', 'is_recurring', 'bank_name', 'customer_name',
                            'web_url', 'status')

    def validate_bank(self, value):
        request = self.context['request']
        try:
            rec = UserBankDetail.objects.get(id=value, user=request.user)
        except UserBankDetail.DoesNotExist:
            raise ValidationError(f"Invalid bank value: {value}")
        return rec

    def create(self, data):
        data.pop('sip_amount')
        user_bank = data.pop('bank')
        request = self.context['request']
        data['user'] = request.user
        data['bank_code'] = user_bank.bank.bank.code_pg
        data['bank_ifsc'] = user_bank.bank.ifsc
        data['bank_name'] = user_bank.bank.bank.name
        data['customer_account_number'] = user_bank.account_number
        data['customer_account_type'] = user_bank.account_type
        data['customer_name'] = user_bank.beneficiary_name
        inv_interval = data['recommendation'].inv_interval
        data['is_recurring'] = False if  inv_interval == InvestmentIntervalEnum.Lumpsum.value else True
        data['frequency'] = MandateFrequencyEnum.Monthly.value if inv_interval == InvestmentIntervalEnum.Monthly.value \
                                else MandateFrequencyEnum.Weekly.value if inv_interval == InvestmentIntervalEnum.Weekly.value \
                                else MandateFrequencyEnum.Adhoc.value
        return super().create(data)


class OrderListSerializer(ModelSerializer):

    fund_no = CharField(source="instrument.code")
    fund_name = CharField(source="instrument.name")
    fund_logo = ImageField(source="instrument.logo")
    buy_sell = CharField(source='get_buy_sell_display')
    bse_order_id = CharField(source='bsestar_order_number')
    sip_order_date = SerializerMethodField()
    nav_value = FloatField(source='nav')
    series_number_installment = CharField(source='sip_installment_number')

    class Meta:
        model = Order
        fields = ('id', 'fund_no', 'fund_name', 'fund_logo', 'order_date', 'order_type', 'amount', 'units', 'nav_value',
                  'buy_sell', 'bse_order_id', 'sip_order_date', 'series_number_installment')

    def get_sip_order_date(self, obj):
        return obj.sip.first_sip_date.strftime("%Y-%m-%d") if obj.sip else ""



class SIPRedeemSerializer(ModelSerializer):

    fund_no = CharField(source="instrument.code")
    fund_name = CharField(source="instrument.name")
    fund_logo = ImageField(source="instrument.logo")
    folio_number = CharField(source="user.bsestar_folio_no")

    class Meta:
        model = SIP
        fields = ('id', 'fund_no', 'fund_name', 'fund_logo', 'folio_number', 'summary')


class OrderSerializer(ModelSerializer):

    lumpsum_id = IntegerField(source='order_lumpsum_id')
    fund_no = CharField(source="instrument.code")
    fund_name = CharField(source="instrument.name")
    fund_logo = ImageField(source="instrument.logo")
    buy_sell = CharField(source='get_buy_sell_display')
    order_status = CharField(source='get_status_display')
    order_date = CharField(source='order_created_date')
    series_number_installment = CharField(source='sip_installment_number')
    bse_order_id = CharField(source='bsestar_order_number')

    class Meta:
        model = Order
        fields = ('id', 'sip_id', 'lumpsum_id', 'bse_order_id', 'fund_no', 'fund_name', 'fund_logo', 'risk_profile_display', 'buy_sell',
                  'series_number_installment', 'order_date', 'order_type', 'amount', 'units', 'nav', 'first_sip_date', 
                  'bank_account', 'order_status')


class OrderSellSIPCreateSerializer(ModelSerializer):

    class Meta:
        model = Order
        fields = ('id', 'sip', 'amount', 'redeem_all', 'redeem_2fa_url')
        read_only_fields = ('id', 'redeem_2fa_url')

    def create(self, data):
        request = self.context['request']
        data['user'] = request.user
        data['instrument'] = data['sip'].instrument
        data['order_date'] = timezone.now().date()
        data['buy_sell'] = BuySellEnum.Sell.value
        return super().create(data)


class OrderLumpsumCreateSerializer(ModelSerializer):

    bank = IntegerField(write_only=True, required=False)
    status = CharField(source='get_status_display', read_only=True)

    class Meta:
        model = OrderLumpsum
        fields = ('id', 'status', 'payment_status', 'payment_mode', 'bank', 'vpa_id',
                  'amount', 'recommendation', 'pg_response')
        read_only_fields = ('id', 'status', 'payment_status', 'pg_response',)

    # def validate_bank(self, value):
    #     request = self.context['request']
    #     try:
    #         rec = UserBankDetail.objects.get(id=value, user=request.user)
    #     except UserBankDetail.DoesNotExist:
    #         raise ValidationError(f"Invalid bank value: {value}")
    #     return rec

    def create(self, data):
        request = self.context['request']
        data['user'] = request.user
        user_bank_id = data.pop('bank')
        try:
            user_bank = UserBankDetail.objects.get(id=user_bank_id, user=request.user)
        except UserBankDetail.DoesNotExist:
            raise ValidationError(f"Invalid bank value: {user_bank_id}")

        bank = user_bank.bank.bank
        data['bank_account_number'] = user_bank.account_number
        data['bank_name'] = bank.name
        data['bank_code'] = bank.code_pg
        data['bank_ifsc'] = user_bank.bank.ifsc
        return super().create(data)


class LumpsumRedeemSerializer(ModelSerializer):

    fund_no = CharField(source="instrument.code")
    fund_name = CharField(source="instrument.name")
    fund_logo = ImageField(source="instrument.logo")
    summary = DictField(source='redeem_summary')

    class Meta:
        model = Order
        fields = ('id', 'fund_no', 'fund_name', 'fund_logo', 'summary')


class OrderSellLumpsumCreateSerializer(ModelSerializer):

    order_id = IntegerField(write_only=True)
    bank = IntegerField(write_only=True)

    class Meta:
        model = Order
        fields = ('id', 'order_id', 'bank', 'amount', 'redeem_all', 'redeem_2fa_url')
        read_only_fields = ('id', 'redeem_2fa_url')

    def validate_order_id(self, value):
        request = self.context['request']
        try:
            rec = Order.objects.get(id=value, user=request.user)
        except Order.DoesNotExist:
            raise ValidationError(f"Invalid order_id: {value}")
        return rec

    def validate_bank(self, value):
        request = self.context['request']
        try:
            rec = UserBankDetail.objects.get(id=value, user=request.user)
        except UserBankDetail.DoesNotExist:
            raise ValidationError(f"Invalid bank value: {value}")
        return rec

    def create(self, data):
        request = self.context['request']
        user_bank = data.pop('bank')
        order = data.pop('order_id')
        data['user'] = request.user
        data['order_lumpsum'] = order.order_lumpsum
        data['order_date'] = timezone.localdate()
        data['instrument'] = order.instrument
        data['buy_sell'] = BuySellEnum.Sell.value
        data['user_bank'] = user_bank
        return super().create(data)
