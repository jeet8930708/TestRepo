# fikaa/celery.py

import os
from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fikaa.settings")
app = Celery("fikaa")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
