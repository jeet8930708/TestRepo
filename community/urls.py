from django.urls import path

from .views import (
    MemberCreateView,
)

urlpatterns = [
    path('member/join/', MemberCreateView.as_view(), name='member-join'),
]
