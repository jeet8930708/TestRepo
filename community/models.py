from django.db import models

# Create your models here.
from django.db.models.signals import post_save
from django.dispatch import receiver
from utils.email_util import send_welcome_email


class Member(models.Model):
    email = models.EmailField(unique=True, error_messages={'unique': "The email is already registered to "
                                                                     "Fikaa community"})
    joined_on = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.email


@receiver(post_save, sender=Member)
def send_email(sender, instance, created, **kwargs):
    if created:
        send_welcome_email(instance.email)
