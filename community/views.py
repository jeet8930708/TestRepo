from django.shortcuts import render
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny

from utils.response import response_200
from .models import Member
from .serializers import MemberSerializer


class MemberCreateView(CreateAPIView):
    permission_classes = (AllowAny, )
    serializer_class = MemberSerializer
    queryset = Member.objects.all()

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Email registered to Fikaa community successfully", response.data)
