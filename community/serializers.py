from .models import Member
from rest_framework.serializers import ModelSerializer


class MemberSerializer(ModelSerializer):
    class Meta:
        model = Member
        fields = ['email']

