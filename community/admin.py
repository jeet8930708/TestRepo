from django.contrib import admin

# Register your models here.
from .models import Member


class MemberAdmin(admin.ModelAdmin):
    list_display = ('email', 'joined_on')
    list_filter = ('joined_on', 'is_active')
admin.site.register(Member, MemberAdmin)

