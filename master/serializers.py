from rest_framework.serializers import ModelSerializer, CharField, ImageField, ListSerializer
from .models import Category, BankBranch, Bank


# ========================================================================
class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name', 'is_active']
        read_only_fields = ('id',)

    def create(self, validated_data):
        validated_data['is_active'] = True
        return super().create(validated_data)


# ========================================================================
class BankListSerializer(ModelSerializer):

    logo = CharField(source='logo_url')

    class Meta:
        model = Bank
        fields = ('name', 'logo')


# ========================================================================
class BankSerializer(ModelSerializer):

    bank = CharField(source='bank.name')
    logo = CharField(source='bank.logo_url')

    class Meta:
        model = BankBranch
        fields = ('bank', 'logo', 'ifsc', 'branch', 'centre', 'district', 'state', 'city', 'address')
