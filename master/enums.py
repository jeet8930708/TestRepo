from core.enums import ChoiceEnum


class OccupationTypeEnum(ChoiceEnum):
    Business = "B"
    Service = "S"
    Others = "O"
    Not_Categorized = "X"
