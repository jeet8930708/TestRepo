from django.db import models
from .storage import BankMediaStorage
from .enums import OccupationTypeEnum
# Create your models here.


class Occupation(models.Model):
    name = models.CharField(max_length=100, unique=True)
    code = models.CharField(max_length=2, default="00")
    type = models.CharField(max_length=1, choices=OccupationTypeEnum.choices(), default=OccupationTypeEnum.Not_Categorized.value)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Income(models.Model):
    bucket = models.CharField(max_length=100, unique=True)
    code = models.CharField(max_length=2, default="00")
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.bucket


class SourceOfWealth(models.Model):
    name = models.CharField(max_length=100, unique=True)
    code = models.CharField(max_length=2, default="00")
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(max_length=50, unique=True)
    code = models.CharField(max_length=3, default="000")
    code_text = models.CharField(max_length=2, default="ZZ")
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'Countries'

    def __str__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=50)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, related_name='states')
    code = models.CharField(max_length=2, default="XX")
    code_cvlkra = models.CharField(max_length=3, default='000')
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ('name', 'country')

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(unique=True, max_length=50)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Bank(models.Model):
    name = models.CharField(max_length=100, unique=True)
    logo = models.ImageField(storage=BankMediaStorage, blank=True, null=True)
    code_pg = models.CharField(max_length=3, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)

    @property
    def logo_url(self):
        return self.logo.url if self.logo else "https://fikaa-assets.s3.ap-south-1.amazonaws.com/bank/default_icon.png"


class BankBranch(models.Model):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE, related_name='branches')
    ifsc = models.CharField(max_length=20, db_index=True)
    branch = models.CharField(max_length=200, db_index=True)
    centre = models.CharField(max_length=200, blank=True, null=True)
    district = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    contact = models.CharField(max_length=20, blank=True, null=True)
    imps = models.BooleanField(default=False)
    rtgs = models.BooleanField(default=False)
    city = models.CharField(max_length=100, db_index=True)
    neft = models.BooleanField(default=False)
    micr = models.CharField(max_length=20, blank=True, null=True)
    upi = models.BooleanField(default=False)
    swift = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.branch} - {self.city} - {self.state}"

    class Meta:
        ordering = ('state', 'city')
