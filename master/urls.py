from django.urls import path

from .views import (
    CountryListView,
    OccupationListView,
    IncomeListView,

    CategoryListCreateView,
    CategoryRetrieveUpdateView,

)


urlpatterns = [
    path('country/list/', CountryListView.as_view(), name='country-list'),
    path('occupation/list/', OccupationListView.as_view(), name='occupation-list'),
    path('income/list/', IncomeListView.as_view(), name='income-list'),

    path('category/create/', CategoryListCreateView.as_view(), name='category-create'),
    path('category/list/', CategoryListCreateView.as_view(), name='category-list'),
    path('category/retrieve/<int:pk>/', CategoryRetrieveUpdateView.as_view(), name='category-retrieve'),
    path('category/update/<int:pk>/', CategoryRetrieveUpdateView.as_view(), name='category-update'),
]
