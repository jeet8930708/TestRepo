# Generated by Django 4.0.2 on 2022-07-30 18:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('master', '0012_alter_occupation_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='country',
            name='code_text',
            field=models.CharField(default='ZZ', max_length=2),
        ),
    ]
