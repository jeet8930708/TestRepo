# Generated by Django 4.0.2 on 2022-04-28 06:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('master', '0003_alter_country_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='country',
            options={'ordering': ('name',), 'verbose_name_plural': 'Countries'},
        ),
    ]
