from django.contrib import admin

# Register your models here.

from .models import Income, Occupation, Country, Category, Bank, BankBranch, State, Country


class IncomeAdmin(admin.ModelAdmin):
    list_display = ['bucket', 'is_active']
    list_filter = ['is_active']
    search_fields = ['bucket']
admin.site.register(Income, IncomeAdmin)


class OccupationAdmin(admin.ModelAdmin):
    list_display = ['name', 'code', 'is_active']
    list_filter = ['is_active']
    search_fields = ['name']
admin.site.register(Occupation, OccupationAdmin)


class CountryAdmin(admin.ModelAdmin):
    list_display = ['name', 'code', 'is_active']
    list_filter = ['is_active']
    search_fields = ['name']
admin.site.register(Country, CountryAdmin)


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'is_active']
admin.site.register(Category, CategoryAdmin)


class BankAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active')
    list_filter = ('is_active',)
    search_fields = ('name', )
admin.site.register(Bank, BankAdmin)


class BankBranchAdmin(admin.ModelAdmin):
    list_display = ('ifsc', 'bank', 'branch', 'district', 'city', 'state')
    list_filter = ('bank',)
    search_fields = ('ifsc', 'bank__name', 'branch', 'district', 'city', 'state')
admin.site.register(BankBranch, BankBranchAdmin)


class StateAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'country', 'is_active')
    list_filter = ('is_active',)
    search_fields = ('name', )
admin.site.register(State, StateAdmin)
