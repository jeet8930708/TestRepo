from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.generics import RetrieveUpdateAPIView, ListCreateAPIView
from utils.response import response_200

from .models import Country, Occupation, Income, Category
from .serializers import CategorySerializer


class CountryListView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data = [{'id': c.id,
                 'name': c.name} for c in Country.objects.filter(is_active=True)]
        return response_200("Country list fetched successfully", data)


class OccupationListView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data = [{'id': o.id,
                 'name': o.name} for o in Occupation.objects.filter(is_active=True)]
        return response_200("Occupation list fetched successfully", data)


class IncomeListView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        data = [{'id': i.id,
                 'name': i.bucket} for i in Income.objects.filter(is_active=True)]
        return response_200("Income list fetched successfully", data)


# ========================================================================
class CategoryListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CategorySerializer
    queryset = Category.objects.all()

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("Category created successfully", response.data)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("Category list fetched successfully", response.data)


# ========================================================================
class CategoryRetrieveUpdateView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CategorySerializer
    queryset = Category.objects.all()

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("Category details retrieved successfully", response.data)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        return response_200("Category updated successfully", response.data)