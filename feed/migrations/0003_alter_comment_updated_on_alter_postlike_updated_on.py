# Generated by Django 4.0.2 on 2022-05-04 17:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feed', '0002_alter_post_updated_on'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='updated_on',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='postlike',
            name='updated_on',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
