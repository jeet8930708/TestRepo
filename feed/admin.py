from django.contrib import admin
from .models import (
    Post,
    Comment,
)


class PostAdmin(admin.ModelAdmin):
    list_display = ('user', 'category', 'get_text_summary', 'tag',
                    'likes_count', 'comments_count', 'shared_count', 'report_count',
                    'approved', 'created_on')
    list_filter = ('category', 'approved')
    search_fields = ('category__name', 'text')

    def get_text_summary(self, obj):
        return obj.text if len(obj.text) < 20 else f"{obj.text}..."
    get_text_summary.short_description = 'Summary'

admin.site.register(Post, PostAdmin)
