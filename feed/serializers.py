from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    CharField,
)

from .models import (
    Post,
    PostLike,
    PostReport,
    Comment,
    UserFollowing
)

from core.models import (
    UserProfessionalDetail
)


class UserFollowingSerializer(ModelSerializer):
    is_following = SerializerMethodField()
    
    def get_is_following(self, object):
        followings = UserFollowing.objects.filter(user=self.context["request"].user)
        return False

    class Meta:
        model = UserFollowing
        fields = ['id', 'user', 'following', 'is_following']
        read_only_fields = ['id', 'user']

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return super().create(validated_data)


# ========================================================================
class PostSerializer(ModelSerializer):

    category_name = CharField(source='category.name', read_only=True)
    user_name = CharField(source='user.get_full_name', read_only=True)
    user_profile_pic = CharField(source='user.image', read_only=True)
    is_liked = SerializerMethodField()
    # user_occupation = SerializerMethodField()

    # def get_user_occupation(self, object):
    #     request_user = self.context['request'].user
    #     try:
    #         occupation = UserProfessionalDetail.objects.get(user=request_user.id)
    #     except:
    #         occupation = "No Occupation Defined"
    #     return occupation

    class Meta:
        model = Post
        fields = ['id', 'user', 'user_name', 'user_profile_pic', 'weight', 'text', 'content', 'category', 'category_name', 'tag',
                  'is_liked', 'likes_count', 'comments_count', 'shared_count', 'report_count',
                  'approved', 'created_on', 'updated_on']
        read_only_fields = ('id', 'user', 'likes_count', 'comments_count', 'shared_count', 'report_count',
                            'approved', 'weight')

    def get_is_liked(self, obj):
        request = self.context['request']
        return obj.is_liked(request.user)

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return super().create(validated_data)


# ========================================================================
class PostLikeSerializer(ModelSerializer):
    class Meta:
        model = PostLike
        fields = ['id', 'post']
        read_only_fields = ('id',)

    def create(self, validated_data):
        request = self.context['request']
        validated_data['liked_by'] = request.user
        return super().create(validated_data)

# ========================================================================


class PostReportSerializer(ModelSerializer):
    class Meta:
        model = PostReport
        fields = ['id', 'post', 'reported_by', 'report_reason']
        read_only_fields = ('id', 'reported_by')

    def create(self, validated_data):
        request = self.context['request']
        validated_data['reported_by'] = request.user
        return super().create(validated_data)

# ========================================================================


class CommentSerializer(ModelSerializer):
    queryset = Comment.objects.all()
    user_name = CharField(source='user.get_full_name', read_only=True)
    user_profile_pic = CharField(source='user.image', read_only=True)

    class Meta:
        model = Comment
        fields = ['id', 'post', 'content', 'user_name', 'user_profile_pic', 'parent_comment', 'created_on', 'updated_on']
        read_only_fields = ('id',)

    def create(self, validated_data):
        request = self.context['request']
        validated_data['user'] = request.user
        return super().create(validated_data)
