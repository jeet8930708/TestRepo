from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, post_delete, pre_delete
from django.dispatch import receiver

from master.models import Category

User = get_user_model()


class UserFollowing(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="followers")
    following = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="following")
    unfollow = models.BooleanField(default=False)
    unfollowed_on = models.DateTimeField(blank=True, null=True)
    last_modified_on = models.DateTimeField(auto_now=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('user', 'following')


class Post(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="posts")
    text = models.TextField(null=False, blank=False)
    likes_count = models.IntegerField(default=0)
    comments_count = models.IntegerField(default=0)
    shared_count = models.IntegerField(default=0)
    report_count = models.IntegerField(default=0)
    weight = models.IntegerField(default=0, blank=False, null=False)
    content = models.JSONField(blank=True, null=True)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="posts")
    tag = models.TextField()
    approved = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    approved_by = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="posts_approved", null=True, blank=True)
    approved_on = models.DateTimeField(
        auto_now_add=False, blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created_on',)

    def is_liked(self, user):
        return self.likes.filter(liked_by=user).exists()


class PostLike(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='likes')
    liked_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='liked_posts')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('post', 'liked_by')


class PostReport(models.Model):
    SPM = 'Spam or misleading'
    ABC = 'Abusive Content'
    VC = 'Violent Content'
    OTH = 'Other'
    SPAM_TYPES = [
        (SPM, 'Spam or misleading'),
        (ABC, 'Abusive Content'),
        (VC, 'Violent Content'),
        (OTH, 'Other')
    ]
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    reported_by = models.ForeignKey(User, on_delete=models.CASCADE)
    report_reason = models.CharField(
        null=False, blank=False, choices=SPAM_TYPES, max_length=20)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('post', 'reported_by')


@receiver(post_save, sender=PostLike)
def update_post_likes_counts(sender, instance, created, *args, **kwargs):
    instance.post.likes_count = instance.post.likes_count + 1
    instance.post.weight = instance.post.weight + (instance.post.likes_count + (
        instance.post.comments_count * 2) + (instance.post.comments_count * 5))
    instance.post.save()


@receiver(post_delete, sender=PostLike)
def update_post_likes_after_delete_counts(sender, instance, *args, **kwargs):
    # post = 
    instance.post.likes_count = instance.post.likes_count - 1
    instance.post.weight = instance.post.weight + (instance.post.likes_count + (
        instance.post.comments_count * 2) + (instance.post.comments_count * 5))
    instance.post.save()


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    parent_comment = models.ForeignKey(
        "self", on_delete=models.CASCADE, null=True)
    is_active = models.BooleanField(default=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


@receiver(post_save, sender=Comment)
def update_post_comments_count(sender, instance, created, *args, **kwargs):
    instance.post.comments_count = instance.post.comments_count + 1
    instance.post.weight = instance.post.weight + (instance.post.likes_count + (
        instance.post.comments_count * 2) + (instance.post.comments_count * 5))
    instance.post.save()
