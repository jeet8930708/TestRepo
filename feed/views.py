from django.shortcuts import get_object_or_404
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView, GenericAPIView, ListAPIView, CreateAPIView, RetrieveUpdateDestroyAPIView, DestroyAPIView, UpdateAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import filters, status
from datetime import date
from rest_framework.response import Response
from utils.response import response_200, response_400_bad_request
from rest_framework.request import Request
from .models import Post, PostLike, Comment, PostReport, UserFollowing
from .serializers import (
    PostSerializer,
    PostLikeSerializer,
    PostReportSerializer,
    CommentSerializer,
    UserFollowingSerializer,
)

import os

from rest_framework.views import APIView
from django.http import JsonResponse
from django.utils import timezone
import datetime
from feed.storage import MediaStorage


class UserFollowingListAPIView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserFollowingSerializer
    queryset = UserFollowing.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        print(self.request.GET.get('type'))
        if self.request.GET.get('type') == "followers":
            queryset = queryset.filter(following=self.request.user)
        else:
            queryset = queryset.filter(user=self.request.user)
        return queryset

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("List", response.data)


class UserFollowingListCreateView(CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserFollowingSerializer
    queryset = UserFollowing.objects.all()

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return response_200("User Followed", response.data)


# ========================================================================
class PostListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    search_fields = ['text', 'tag', 'user__username', 'user__first_name', 'user__last_name']
    filter_backends = (filters.SearchFilter,)
    serializer_class = PostSerializer
    queryset = Post.objects.filter(approved=True).order_by('-weight')

    def get_queryset(self):
        queryset = super().get_queryset()
        reported_posts = PostReport.objects.filter(reported_by=self.request.user)
        ids = []
        if reported_posts:
            for x in reported_posts:
                ids.append(x.post_id)
        queryset = queryset.filter(category=self.kwargs['category_id'], approved=True).exclude(id__in=ids)
        return queryset

    def create(self, request, *args, **kwargs):
        if request.user.kyc_submitted:
            response = super().create(request, *args, **kwargs)
            return response_200("Post created successfully", response.data)
        else:
            return response_400_bad_request("User KYC not verified")

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("Post list fetched successfully", response.data)

class PostDeleteByIdView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def update(self, request:Request, *args, **kwargs):
        response = self.partial_update(request, *args, **kwargs)
        return response_200("Comment updated successfully", response.data)

    
class CommentUpdateByIdView(UpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()

    def update(self, request:Request, *args, **kwargs):
        response = self.partial_update(request, *args, **kwargs)
        return response_200("Comment updated successfully", response.data)
    
    def update(self, request:Request , *args, **kwargs):
        kwargs['partial'] = True
        comment_data = get_object_or_404(Comment, user = request.user, id= kwargs["pk"])
        user_data = request.data
        serializer_response = self.serializer_class(instance=comment_data, data = user_data, partial = True)
        if (serializer_response.is_valid()):
            serializer_response.save()          
            return response_200("Comment updated successfully", serializer_response.data)
        return Response(data = serializer_response.error_messages, status= status.HTTP_400_BAD_REQUEST)

        

        
# ========================================================================
class PostListView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("Post list fetched successfully", response.data)


# ========================================================================
class PostLatestListView(PostListView):

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(approved=True).order_by('-created_on')


# ========================================================================
class PostTrendingListView(PostListView):

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(approved=True).order_by('-likes_count')


# ========================================================================
class MyPostListView(PostListView):

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(user=self.request.user)


# ========================================================================
class PostRetrieveUpdateView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        response.data["is_liked"] = False
        post = response.data
        liked = PostLike.objects.filter(post=post["id"], liked_by=request.user).first()
        if liked is not None:
            response.data["is_liked"] = True
        return response_200("Post details retrieved successfully", response.data)

    def update(self, request, *args, **kwargs):
        if request.user.kyc_submitted:
            response = super().update(request, *args, **kwargs)
            return response_200("Post updated successfully", response.data)
        else:
            return response_400_bad_request("User KYC not verified")

# ========================================================================


class PostLikeListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PostLikeSerializer
    queryset = PostLike.objects.all()

    def create(self, request, *args, **kwargs):
        liked = PostLike.objects.filter(post=request.data["post"], liked_by=request.user.id).first()
        if liked:
            print(liked)
        response = super().create(request, *args, **kwargs)
        return response_200("Post Liked successfully", response.data)

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("Post likes fetched successfully", response.data)


# ========================================================================

class PostReportListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PostReportSerializer
    queryset = PostReport.objects.all()

    def create(self, request, *args, **kwargs):
        if request.user.kyc_submitted:
            response = super().create(request, *args, **kwargs)
            return response_200("Post Reported successfully", response.data)
        else:
            return response_400_bad_request("User KYC not verified")

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("Post likes fetched successfully", response.data)


# ========================================================================
class PostLikeRetrieveUpdateView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PostLikeSerializer
    queryset = PostLike.objects.all()

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("Post like retrieved successfully", response.data)

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        return response_200("Post like updated successfully", response.data)

    def delete(self, request, *args, **kwargs):
        liked = PostLike.objects.filter(liked_by=request.user,post=kwargs["pk"]).delete()
        return response_200("Post unliked updated successfully")

# ========================================================================


class CommentListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(post=self.kwargs["post_id"])
        if "comment_id" in self.kwargs:
            print(queryset)
            return queryset.filter(parent_comment=self.kwargs["comment_id"])
        else:
            return queryset.filter(parent_comment__isnull=True)
        return queryset

    def create(self, request, *args, **kwargs):
        if request.user.kyc_submitted:
            response = super().create(request, *args, **kwargs)
            return response_200("Commnented successfully", response.data)
        else:
            return response_400_bad_request("User KYC not verified")

    def list(self, request, *args, **kwargs):
        response = super().list(request, *args, **kwargs)
        return response_200("Comments fetched successfully", response.data)


# ========================================================================
class CommentRetrieveUpdateView(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = CommentSerializer
    # queryset = Comment.objects.all()

    def retrieve(self, request, *args, **kwargs):
        response = super().retrieve(request, *args, **kwargs)
        return response_200("Comment Details fetched successfully", response.data)

    def update(self, request, *args, **kwargs):
        if request.user.kyc_submitted:
            response = super().update(request, *args, **kwargs)
            return response_200("Comment updated successfully", response.data)
        else:
            return response_400_bad_request("User KYC not verified")


# ========================================================================


class ApprovePostView(APIView):
    serializer = PostSerializer

    def post(self, request, *args, **kwargs):
        request_data = request.data
        post = Post.objects.filter(id=request_data["post"])
        current_datetime = datetime.datetime.now()
        post = Post.objects.filter(pk=request_data["post"]).update(
            approved=request_data["approved"], approved_by=request.user, approved_on=current_datetime)
        return response_200("Post appoved successfully")


# ========================================================================


class PostShareView(APIView):
    serializer = PostSerializer

    def post(self, request, *args, **kwargs):
        request_data = request.data
        post = Post.objects.filter(id=request_data["post"]).first()
        post.shared_count = post.shared_count + 1
        post.weight = post.weight + \
            (post.likes_count + (post.comments_count * 2) + (post.comments_count * 5))
        post.save()
        return response_200("Post shared successfully")


# ========================================================================

class FileUploadView(APIView):

    def post(self, requests, **kwargs):
        file_obj = requests.FILES.get('file', '')
        file_type = kwargs["type"]
        # do your validation here e.g. file size/type check

        # organize a path for the file in bucket
        file_directory_within_bucket = 'user_upload_files/{file_type}/{username}'.format(
            username=requests.user, file_type=file_type)

        # synthesize a full file path; note that we included the filename
        file_path_within_bucket = os.path.join(
            file_directory_within_bucket,
            file_obj.name
        )

        media_storage = MediaStorage()

        # if not media_storage.exists(file_path_within_bucket): # avoid overwriting existing file
        media_storage.save(file_path_within_bucket, file_obj)
        file_url = media_storage.url(file_path_within_bucket)

        return JsonResponse({
            'message': 'OK',
            'fileUrl': file_url,
        })
        # else:
        #     return JsonResponse({
        #         'message': 'Error: file {filename} already exists at {file_directory} in bucket {bucket_name}'.format(
        #             filename=file_obj.name,
        #             file_directory=file_directory_within_bucket,
        #             bucket_name=media_storage.bucket_name
        #         ),
        #     }, status=400)
