from django.urls import path

from .views import (
    PostListCreateView,
    PostRetrieveUpdateView,
    PostLikeListCreateView,
    PostLikeRetrieveUpdateView,
    PostReportListCreateView,
    CommentListCreateView,
    FileUploadView,
    UserFollowingListCreateView,
    UserFollowingListAPIView,
    ApprovePostView,
    PostShareView,
    PostLatestListView,
    PostTrendingListView,
    MyPostListView,
    PostDeleteByIdView,
    CommentUpdateByIdView,
)

urlpatterns = [
    path('my-posts/', MyPostListView.as_view(), name='my-posts'),
    path('post/create/', PostListCreateView.as_view(), name='post-create'),
    path('post/list/<int:category_id>/', PostListCreateView.as_view(), name='post-list'),
    path('post/list/latest/', PostLatestListView.as_view(), name='post-list-latest'),
    path('post/list/trending/', PostTrendingListView.as_view(), name='post-list-trending'),
    path('post/retrieve/<int:pk>/', PostRetrieveUpdateView.as_view(), name='post-retrieve'),
    path('post/update/<int:pk>/', PostRetrieveUpdateView.as_view(), name='post-update'),
    path('like/post/', PostLikeListCreateView.as_view(), name='post-like'),
    path('like/post/<int:pk>/', PostLikeRetrieveUpdateView.as_view(), name='post-like-update'),

    path('post/comment/create/', CommentListCreateView.as_view(), name='post-comment'),
    path('post/<int:post_id>/comment/list/', CommentListCreateView.as_view(), name='post-get-comment'),
    path('post/<int:post_id>/comment/<int:comment_id>/', CommentListCreateView.as_view(),
         name='post-get-comment-details'),

    path('upload/<str:type>/', FileUploadView.as_view(), name="upload-files"),
    
    path('post/report/', PostReportListCreateView.as_view(), name='post-report'),
    
    path('post/approve/', ApprovePostView.as_view(), name='post-approve'),
    
    path('post/share/', PostShareView.as_view(), name='post-share'),
    
    path('users/follow/', UserFollowingListCreateView.as_view(), name='follow-create'),
    path('users/follow/<int:user_id>/', UserFollowingListAPIView.as_view(), name='follow-create'),


    path('post/delete/<int:pk>/', PostDeleteByIdView.as_view(), name='post-delete'),
    path('post/comment/delete/<int:pk>/', CommentUpdateByIdView.as_view(), name='post-delete'),

    # path('post/list/<int:category_id>', PostListCreateView.as_view(), name='post-list'),
    # path('post/retrieve/<int:pk>/', PostRetrieveUpdateView.as_view(), name='post-retrieve'),
    # path('post/update/<int:pk>/', PostRetrieveUpdateView.as_view(), name='post-update'),
    
]
